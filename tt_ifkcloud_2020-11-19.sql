# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.36-MariaDB)
# Database: tt_ifkcloud
# Generation Time: 2020-11-19 15:22:02 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table _postcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postcategories`;

CREATE TABLE `_postcategories` (
  `PostCategoryID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryName` varchar(50) NOT NULL,
  `PostCategoryLabel` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`PostCategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_postcategories` WRITE;
/*!40000 ALTER TABLE `_postcategories` DISABLE KEYS */;

INSERT INTO `_postcategories` (`PostCategoryID`, `PostCategoryName`, `PostCategoryLabel`)
VALUES
	(1,'Inspirasi','#f56954'),
	(5,'Lainnya','#3c8dbc');

/*!40000 ALTER TABLE `_postcategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _postimages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postimages`;

CREATE TABLE `_postimages` (
  `PostImageID` bigint(10) NOT NULL AUTO_INCREMENT,
  `PostID` bigint(10) NOT NULL,
  `FileName` varchar(250) NOT NULL,
  `Description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`PostImageID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table _posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_posts`;

CREATE TABLE `_posts` (
  `PostID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryID` int(10) NOT NULL,
  `PostDate` date NOT NULL,
  `PostTitle` varchar(200) NOT NULL,
  `PostSlug` varchar(200) NOT NULL,
  `PostContent` longtext NOT NULL,
  `PostExpiredDate` date NOT NULL,
  `TotalView` int(11) NOT NULL DEFAULT '0',
  `LastViewDate` datetime DEFAULT NULL,
  `IsSuspend` tinyint(1) NOT NULL DEFAULT '1',
  `FileName` varchar(250) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) NOT NULL,
  `UpdatedOn` datetime NOT NULL,
  PRIMARY KEY (`PostID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table _roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_roles`;

CREATE TABLE `_roles` (
  `RoleID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(50) NOT NULL,
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_roles` WRITE;
/*!40000 ALTER TABLE `_roles` DISABLE KEYS */;

INSERT INTO `_roles` (`RoleID`, `RoleName`)
VALUES
	(1,'Administrator'),
	(2,'Operator Farmasi'),
	(3,'Operator Puskesmas');

/*!40000 ALTER TABLE `_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_settings`;

CREATE TABLE `_settings` (
  `SettingID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`SettingID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_settings` WRITE;
/*!40000 ALTER TABLE `_settings` DISABLE KEYS */;

INSERT INTO `_settings` (`SettingID`, `SettingLabel`, `SettingName`, `SettingValue`)
VALUES
	(1,'SETTING_WEB_NAME','SETTING_WEB_NAME','Farmasi Cloud'),
	(2,'SETTING_WEB_DESC','SETTING_WEB_DESC','Sistem Informasi Pengelolaan Data Farmasi'),
	(3,'SETTING_WEB_DISQUS_URL','SETTING_WEB_DISQUS_URL','https://general-9.disqus.com/embed.js'),
	(4,'SETTING_ORG_NAME','SETTING_ORG_NAME','-'),
	(5,'SETTING_ORG_ADDRESS','SETTING_ORG_ADDRESS','-'),
	(6,'SETTING_ORG_LAT','SETTING_ORG_LAT',''),
	(7,'SETTING_ORG_LONG','SETTING_ORG_LONG',''),
	(8,'SETTING_ORG_PHONE','SETTING_ORG_PHONE','-'),
	(9,'SETTING_ORG_FAX','SETTING_ORG_FAX','-'),
	(10,'SETTING_ORG_MAIL','SETTING_ORG_MAIL','-'),
	(11,'SETTING_WEB_API_FOOTERLINK','SETTING_WEB_API_FOOTERLINK','-'),
	(12,'SETTING_WEB_LOGO','SETTING_WEB_LOGO','logo.png'),
	(13,'SETTING_WEB_SKIN_CLASS','SETTING_WEB_SKIN_CLASS','skin-green-light'),
	(14,'SETTING_WEB_PRELOADER','SETTING_WEB_PRELOADER','loader.gif'),
	(15,'SETTING_WEB_VERSION','SETTING_WEB_VERSION','1.0'),
	(16,'SETTING_ORG_LINKFACEBOOK','SETTING_ORG_LINKFACEBOOK','https://www.facebook.com/Mentor-Huis-105230401189706'),
	(17,'SETTING_ORG_LINKTWITTER','SETTING_ORG_LINKTWITTER','https://twitter.com/MentorhuisID'),
	(18,'SETTING_ORG_LINKINSTAGRAM','SETTING_ORG_LINKINSTAGRAM','');

/*!40000 ALTER TABLE `_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _userinformation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_userinformation`;

CREATE TABLE `_userinformation` (
  `UserName` varchar(50) NOT NULL,
  `IdUnit` bigint(10) DEFAULT NULL,
  `Email` varchar(50) NOT NULL DEFAULT '',
  `NM_FullName` varchar(50) DEFAULT NULL,
  `NM_ProfileImage` varchar(250) DEFAULT NULL,
  `DATE_Registered` datetime DEFAULT NULL,
  `IS_EmailVerified` tinyint(1) DEFAULT NULL,
  `IS_LoginViaGoogle` tinyint(1) DEFAULT NULL,
  `IS_LoginViaFacebook` tinyint(1) DEFAULT NULL,
  `NM_GoogleOAuthID` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`UserName`),
  CONSTRAINT `FK_Userinformation_User` FOREIGN KEY (`UserName`) REFERENCES `_users` (`UserName`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_userinformation` WRITE;
/*!40000 ALTER TABLE `_userinformation` DISABLE KEYS */;

INSERT INTO `_userinformation` (`UserName`, `IdUnit`, `Email`, `NM_FullName`, `NM_ProfileImage`, `DATE_Registered`, `IS_EmailVerified`, `IS_LoginViaGoogle`, `IS_LoginViaFacebook`, `NM_GoogleOAuthID`)
VALUES
	('admin',NULL,'yoelrolas@gmail.com','Partopi Tao',NULL,'2018-08-17 00:00:00',NULL,NULL,NULL,NULL),
	('opt.farmasi',NULL,'opt.farmasi','Galih',NULL,'2020-10-04 00:00:00',NULL,NULL,NULL,NULL),
	('opt.pabatu',1,'opt.pabatu','MANG OLEH',NULL,'2020-11-19 00:00:00',NULL,NULL,NULL,NULL),
	('opt.satria',4,'opt.satria','You Know Who',NULL,'2020-10-04 00:00:00',NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `_userinformation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_users`;

CREATE TABLE `_users` (
  `UserName` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `RoleID` int(10) unsigned NOT NULL,
  `IsSuspend` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `LastLogin` datetime DEFAULT NULL,
  `LastLoginIP` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_users` WRITE;
/*!40000 ALTER TABLE `_users` DISABLE KEYS */;

INSERT INTO `_users` (`UserName`, `Password`, `RoleID`, `IsSuspend`, `LastLogin`, `LastLoginIP`)
VALUES
	('admin','3798e989b41b858040b8b69aa6f2ce90',1,0,'2020-11-19 21:25:04','::1'),
	('opt.farmasi','e10adc3949ba59abbe56e057f20f883e',2,0,'2020-10-04 02:44:05','::1'),
	('opt.pabatu','e10adc3949ba59abbe56e057f20f883e',3,0,'2020-11-19 22:12:43','::1'),
	('opt.satria','e10adc3949ba59abbe56e057f20f883e',3,0,'2020-11-19 21:58:26','::1');

/*!40000 ALTER TABLE `_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mkategori
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mkategori`;

CREATE TABLE `mkategori` (
  `IdKategori` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `NmKategori` varchar(200) NOT NULL DEFAULT '',
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL,
  `DeletedOn` datetime DEFAULT NULL,
  `DeletedBy` varchar(50) DEFAULT '',
  PRIMARY KEY (`IdKategori`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mkategori` WRITE;
/*!40000 ALTER TABLE `mkategori` DISABLE KEYS */;

INSERT INTO `mkategori` (`IdKategori`, `NmKategori`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`, `IsDeleted`, `DeletedOn`, `DeletedBy`)
VALUES
	(1,'OBAT GENERIK','admin','2020-10-04 01:05:18',NULL,NULL,1,'2020-10-04 01:27:58','admin'),
	(2,'OBAT NON GENERIK','admin','2020-10-04 01:05:27','admin','2020-10-04 01:05:35',0,NULL,''),
	(4,'IMUNISASI','admin','2020-10-04 01:26:59',NULL,NULL,1,'2020-10-04 01:27:58','admin'),
	(5,'OBAT GENERIK','admin','2020-10-04 01:28:07',NULL,NULL,0,NULL,''),
	(6,'IMUNISASI','admin','2020-10-04 01:28:12',NULL,NULL,0,NULL,'');

/*!40000 ALTER TABLE `mkategori` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mpabrik
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mpabrik`;

CREATE TABLE `mpabrik` (
  `IdPabrik` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `NmPabrik` varchar(200) NOT NULL DEFAULT '',
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(11) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL,
  `DeletedBy` varchar(50) DEFAULT NULL,
  `DeletedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`IdPabrik`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mpabrik` WRITE;
/*!40000 ALTER TABLE `mpabrik` DISABLE KEYS */;

INSERT INTO `mpabrik` (`IdPabrik`, `NmPabrik`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`, `IsDeleted`, `DeletedBy`, `DeletedOn`)
VALUES
	(1,'PT. NOVAPHARIN','admin','2020-10-04 01:59:24',NULL,NULL,0,NULL,NULL),
	(2,'PT. HEXPHARM JAYA','admin','2020-10-04 01:59:36',NULL,NULL,0,NULL,NULL),
	(3,'PT. MERSIFARMA TM','admin','2020-10-04 01:59:52',NULL,NULL,0,NULL,NULL),
	(5,'PT. TEMPO','admin','2020-10-04 02:00:44',NULL,NULL,0,NULL,NULL);

/*!40000 ALTER TABLE `mpabrik` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mpuskesmas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mpuskesmas`;

CREATE TABLE `mpuskesmas` (
  `IdPuskesmas` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `NmPuskesmas` varchar(50) NOT NULL DEFAULT '',
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL,
  `DeletedBy` varchar(50) DEFAULT NULL,
  `DeletedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`IdPuskesmas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mpuskesmas` WRITE;
/*!40000 ALTER TABLE `mpuskesmas` DISABLE KEYS */;

INSERT INTO `mpuskesmas` (`IdPuskesmas`, `NmPuskesmas`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`, `IsDeleted`, `DeletedBy`, `DeletedOn`)
VALUES
	(1,'PABATU','admin','2020-10-04 01:35:40',NULL,NULL,0,NULL,NULL),
	(2,'BROHOL','admin','2020-10-04 01:37:23',NULL,NULL,0,NULL,NULL),
	(3,'SRI PADANG','admin','2020-10-04 01:37:31',NULL,NULL,0,NULL,NULL),
	(4,'SATRIA','admin','2020-10-04 01:37:39',NULL,NULL,0,NULL,NULL),
	(5,'PASAR GAMBIR','admin','2020-10-04 01:37:46',NULL,NULL,0,NULL,NULL),
	(6,'RAMBUNG','admin','2020-10-04 01:37:53',NULL,NULL,0,NULL,NULL),
	(8,'TJ. MARULAK','admin','2020-10-04 01:38:17',NULL,NULL,0,NULL,NULL),
	(9,'TLK. KARANG','admin','2020-10-04 01:38:28',NULL,NULL,0,NULL,NULL),
	(10,'RT. LABAN','admin','2020-10-04 01:39:02',NULL,NULL,0,NULL,NULL);

/*!40000 ALTER TABLE `mpuskesmas` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mstock
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mstock`;

CREATE TABLE `mstock` (
  `IdStock` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdKategori` bigint(10) unsigned NOT NULL,
  `KdStock` varchar(200) DEFAULT NULL,
  `NmStock` varchar(200) DEFAULT NULL,
  `NmSatuan` varchar(200) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL,
  `DeletedBy` varchar(50) DEFAULT NULL,
  `DeletedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`IdStock`),
  KEY `fk_stock_kategori` (`IdKategori`),
  CONSTRAINT `fk_stock_kategori` FOREIGN KEY (`IdKategori`) REFERENCES `mkategori` (`IdKategori`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mstock` WRITE;
/*!40000 ALTER TABLE `mstock` DISABLE KEYS */;

INSERT INTO `mstock` (`IdStock`, `IdKategori`, `KdStock`, `NmStock`, `NmSatuan`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`, `IsDeleted`, `DeletedBy`, `DeletedOn`)
VALUES
	(1,5,'AMB@30MG','AMBROXOL 30mg','TABLET','admin','2020-10-04 02:30:24','admin','2020-10-04 02:31:14',1,'admin','2020-10-04 02:31:21'),
	(2,5,'AMBS','AMBROXOL SIRUP','BOTOL','admin','2020-10-04 02:31:07',NULL,NULL,1,'admin','2020-10-04 02:31:21'),
	(3,5,'AMB@30MG','AMBROXOL 30mg','TABLET','admin','2020-10-04 02:31:38',NULL,NULL,0,NULL,NULL),
	(4,5,'AMX@500MG','AMOKSILIN 500mg','KAPLET','admin','2020-10-04 02:36:22','admin','2020-10-04 02:37:08',0,NULL,NULL),
	(5,5,'AMX@250MG','AMOKSILIN 250mg','KAPLET','admin','2020-10-04 02:37:01',NULL,NULL,0,NULL,NULL);

/*!40000 ALTER TABLE `mstock` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table msupplier
# ------------------------------------------------------------

DROP TABLE IF EXISTS `msupplier`;

CREATE TABLE `msupplier` (
  `IdSupplier` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `NmSupplier` varchar(200) NOT NULL DEFAULT '',
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL,
  `DeletedBy` varchar(50) DEFAULT NULL,
  `DeletedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`IdSupplier`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `msupplier` WRITE;
/*!40000 ALTER TABLE `msupplier` DISABLE KEYS */;

INSERT INTO `msupplier` (`IdSupplier`, `NmSupplier`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`, `IsDeleted`, `DeletedBy`, `DeletedOn`)
VALUES
	(1,'Dinas Kesehatan Prov. Sumatera Utara','admin','2020-10-04 02:04:22',NULL,NULL,0,NULL,NULL),
	(2,'KFTD Pematang Siantar','admin','2020-10-04 02:04:37',NULL,NULL,0,NULL,NULL),
	(3,'PT. Edysa Sukses Bersama','admin','2020-10-04 02:05:02',NULL,NULL,0,NULL,NULL),
	(4,'PT. Merapi Utama Pharma','admin','2020-10-04 02:05:17',NULL,NULL,0,NULL,NULL),
	(5,'PT. Enseval Putera Megatrading, Tbk','admin','2020-10-04 02:05:57',NULL,NULL,0,NULL,NULL);

/*!40000 ALTER TABLE `msupplier` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table trequest
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trequest`;

CREATE TABLE `trequest` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdPuskesmas` bigint(10) NOT NULL,
  `DateRequest` date NOT NULL,
  `NmRequest` varchar(200) DEFAULT NULL,
  `NmRemarks` varchar(200) DEFAULT NULL,
  `IsProcessed` tinyint(1) NOT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table trequest_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trequest_items`;

CREATE TABLE `trequest_items` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdRequest` bigint(10) unsigned NOT NULL,
  `IdStock` bigint(10) unsigned NOT NULL,
  `JlhRequest` double NOT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `FK_Item_Request` (`IdRequest`),
  KEY `FK_Item_Stock` (`IdStock`),
  CONSTRAINT `FK_Item_Request` FOREIGN KEY (`IdRequest`) REFERENCES `trequest` (`Uniq`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Item_Stock` FOREIGN KEY (`IdStock`) REFERENCES `mstock` (`IdStock`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tstockdistribution
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tstockdistribution`;

CREATE TABLE `tstockdistribution` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdPuskesmas` bigint(10) unsigned NOT NULL,
  `NmReferensi` varchar(200) DEFAULT NULL,
  `DateDistribution` date NOT NULL,
  `NmRemarks` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `fk_dist_puskesmas` (`IdPuskesmas`),
  CONSTRAINT `fk_dist_puskesmas` FOREIGN KEY (`IdPuskesmas`) REFERENCES `mpuskesmas` (`IdPuskesmas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tstockdistribution_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tstockdistribution_items`;

CREATE TABLE `tstockdistribution_items` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdDistribution` bigint(10) unsigned NOT NULL,
  `IdReceipt` bigint(10) unsigned NOT NULL,
  `IdRequestItem` bigint(10) unsigned DEFAULT NULL,
  `Jumlah` double NOT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `fk_item_dist` (`IdDistribution`),
  KEY `fk_item_receipt` (`IdReceipt`),
  CONSTRAINT `fk_item_dist` FOREIGN KEY (`IdDistribution`) REFERENCES `tstockdistribution` (`Uniq`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_item_receipt` FOREIGN KEY (`IdReceipt`) REFERENCES `tstockreceipt` (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tstockissue
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tstockissue`;

CREATE TABLE `tstockissue` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdItem` bigint(10) unsigned NOT NULL,
  `DateIssue` date NOT NULL,
  `Jumlah` double NOT NULL,
  `NmRemarks` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `fk_issue_item` (`IdItem`),
  CONSTRAINT `fk_issue_item` FOREIGN KEY (`IdItem`) REFERENCES `tstockdistribution_items` (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tstockreceipt
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tstockreceipt`;

CREATE TABLE `tstockreceipt` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdStock` bigint(10) unsigned NOT NULL,
  `IdPabrik` bigint(10) unsigned NOT NULL,
  `IdSupplier` bigint(10) unsigned NOT NULL,
  `DateReceipt` date NOT NULL,
  `NmBatch` varchar(200) NOT NULL DEFAULT '',
  `NmSumber` varchar(200) NOT NULL,
  `Harga` double NOT NULL,
  `Jumlah` double NOT NULL,
  `Tahun` int(11) NOT NULL,
  `DateExpired` date NOT NULL,
  `NmRemarks` text,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `fk_receipt_stock` (`IdStock`),
  KEY `fk_receipt_pabrik` (`IdPabrik`),
  KEY `fk_receipt_supplier` (`IdSupplier`),
  CONSTRAINT `fk_receipt_pabrik` FOREIGN KEY (`IdPabrik`) REFERENCES `mpabrik` (`IdPabrik`),
  CONSTRAINT `fk_receipt_stock` FOREIGN KEY (`IdStock`) REFERENCES `mstock` (`IdStock`),
  CONSTRAINT `fk_receipt_supplier` FOREIGN KEY (`IdSupplier`) REFERENCES `msupplier` (`IdSupplier`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
