<?php

class Muser extends CI_Model {
    private $table = TBL__USERS;

    function custom_rule($str)
    {
        if ( !preg_match('/^[a-z.,\-]+$/i',"dev.dev") )
        {
            return false;
        }
        else return false;
    }

    function rules($newdata=true, $role) {
        $rules = array();

        if($newdata) {
            $arr = array(
                array(
                    'field' => COL_USERNAME,
                    'label' => COL_USERNAME,
                    'rules' => 'required|min_length[5]|regex_match[/^[a-z0-9.\-]+$/i]|is_unique[_users.UserName]',
                    'errors' => array('is_unique' => 'Username already registered on system')
                ),
                array(
                    'field' => COL_PASSWORD,
                    'label' => COL_PASSWORD,
                    'rules' => 'required|min_length[5]'
                ),
                array(
                    'field' => 'RepeatPassword',
                    'label' => 'Repeat Password',
                    'rules' => 'required|matches[Password]'
                ),
                array(
                    'field' => COL_EMAIL,
                    'label' => COL_EMAIL,
                    'rules' => 'required|valid_email|is_unique[_userinformation.Email]',
                    'errors' => array('is_unique' => 'Email already registered on system')
                )
            );
            $rules = array_merge($arr, $rules);
        }
        return $rules;
    }

    function register($userdata, $userinfo) {
        $retval = true;

        $this->db->trans_begin();
        if(!$this->db->insert(TBL__USERS, $userdata)) {
            $this->db->trans_rollback();
            $retval = false;
        }
        if(!$this->db->insert(TBL__USERINFORMATION, $userinfo)) {
            $this->db->trans_rollback();
            $retval = false;
        }
        /*if($userdata[COL_ROLEID] && $userdata[COL_ROLEID] == ROLECOMPANY) {
            if(!$this->db->insert(TBL_COMPANIES, $companydata)) {
                $this->db->trans_rollback();
                $retval = false;
            }
        }*/
        $this->db->trans_commit();

        return $retval;
    }

    function authenticate($username, $password) {
        $this->db->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__USERS.".".COL_USERNAME,"inner");
        $this->db->where("(".TBL__USERS.".".COL_USERNAME." = '".$username."' OR ".TBL__USERINFORMATION.".".COL_EMAIL." = '".$username."') AND ".TBL__USERS.".".COL_PASSWORD." = '".md5($password)."'");

        $res = $this->db->get($this->table)->row_array();
        //$query = "SELECT * FROM ".TBL__USERS." where (".COL_USERNAME." = '".$username."' OR ".COL_EMAIL." = '".$username."') AND ".COL_PASSWORD." = '".md5($password)."'";
        //$cek = $this->db->query($query)->num_rows();
        if($res){
            return TRUE;
        }else{
            return FALSE;
        }
    }
    function IsSuspend($username){
        $this->db->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__USERS.".".COL_USERNAME,"inner");
        $this->db->where("(".TBL__USERS.".".COL_USERNAME." = '".$username."' OR ".TBL__USERINFORMATION.".".COL_EMAIL." = '".$username."')");

        $user = $this->db->get($this->table)->row_array();
        if($user && !$user[COL_ISSUSPEND]) return FALSE;
        else return TRUE;
    }
    function getdetails($username){
        $this->db->select('_userinformation.*, _users.*, COALESCE(_roles.RoleName, mkategori.NmKategori) as RoleName');
        $this->db->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__USERS.".".COL_USERNAME,"inner");
        $this->db->join(TBL__ROLES,TBL__ROLES.'.'.COL_ROLEID." = ".TBL__USERS.".".COL_ROLEID,"left");
        $this->db->join(TBL_MKATEGORI,TBL_MKATEGORI.'.'.COL_IDKATEGORI." = ".TBL__USERS.".".COL_ROLEID,"left");
        //$this->db->join(TBL_COMPANIES,TBL_COMPANIES.'.'.COL_COMPANYID." = ".TBL__USERINFORMATION.".".COL_COMPANYID,"left");
        //$this->db->join(TBL_RELIGIONS,TBL_RELIGIONS.'.'.COL_RELIGIONID." = ".TBL__USERINFORMATION.".".COL_RELIGIONID,"left");
        //$this->db->join(TBL_EDUCATIONTYPES,TBL_EDUCATIONTYPES.'.'.COL_EDUCATIONTYPEID." = ".TBL__USERINFORMATION.".".COL_EDUCATIONID,"left");
        //$this->db->join(TBL_EMPLOYEES,TBL_EMPLOYEES.'.'.COL_EMPLOYEEID." = ".TBL__USERINFORMATION.".".COL_EMPLOYEEID,"left");
        //$this->db->join(TBL_INDUSTRYTYPES,TBL_INDUSTRYTYPES.'.'.COL_INDUSTRYTYPEID." = ".TBL_COMPANIES.".".COL_INDUSTRYTYPEID,"left");
        //$this->db->where(TBL__USERS.".".COL_USERNAME,$username);
        $this->db->where("(".TBL__USERS.".".COL_USERNAME." = '".$username."' OR ".TBL__USERINFORMATION.".".COL_EMAIL." = '".$username."')");

        return $this->db->get($this->table)->row_array();
    }

    function delete($datum) {
        //$this->load->model('mvacancy');
        $this->db->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__USERS.".".COL_USERNAME,"inner");
        $this->db->where(TBL__USERS.".".COL_USERNAME, $datum);
        $ruser = $this->db->get(TBL__USERS)->row_array();
        $this->db->trans_begin();
        if($ruser) {
            if($ruser[COL_ROLEID] == ROLEADMIN) {
                $this->db->trans_commit();
                return true;
            }

            if($ruser[COL_ROLEID] == ROLECOMPANY && $ruser[COL_COMPANYID]) {
                // Delete Company
                /*if(!$this->db->delete(TBL_COMPANIES, array(COL_COMPANYID => $ruser[COL_COMPANYID]))) {
                    $this->db->trans_rollback();
                    return false;
                }

                // Delete vacancies
                $rvacancy = $this->db->where(COL_COMPANYID, $ruser[COL_COMPANYID])->get(TBL_VACANCIES)->result_array();
                foreach($rvacancy as $vac) {
                    if(!$this->mvacancy->delete($vac[COL_VACANCYID])) {
                        $this->db->trans_rollback();
                        return false;
                    }
                }*/
            }

            // Delete Vacancy Applies
            /*if(!$this->db->delete(TBL_VACANCYAPPLIES, array(COL_USERNAME => $datum))) {
                $this->db->trans_rollback();
                return false;
            }*/

            if(!$this->db->delete(TBL__USERINFORMATION, array(COL_USERNAME => $datum))) {
                $this->db->trans_rollback();
                return false;
            }
        }

        if(!$this->db->delete(TBL__USERS, array(COL_USERNAME => $datum))) {
            $this->db->trans_rollback();
            return false;
        }

        $this->db->trans_commit();
        return true;
    }

    function IsProfileComplete() {
        $user = GetLoggedUser();
        if($user[COL_ROLEID] == ROLEUSER) {
            if(empty($user[COL_NAME]) || empty($user[COL_IDENTITYNO]) || empty($user[COL_BIRTHDATE]) ||
                empty($user[COL_RELIGIONID]) || empty($user[COL_GENDER]) || empty($user[COL_ADDRESS]) || empty($user[COL_PHONENUMBER]) ||
                empty($user[COL_EDUCATIONID]) || empty($user[COL_UNIVERSITYNAME]) || empty($user[COL_FACULTYNAME]) || empty($user[COL_MAJORNAME]) ||
                empty($user[COL_EXPECTEDSALARY]) || empty($user[COL_CVFILENAME]) || empty($user[COL_IMAGEFILENAME])) {
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    function IsVacancyFitToUser($vacancy) {
        $user = GetLoggedUser();
        $retval = true;
        /*if(!empty($user)) {
            $pref = $this->db->where(COL_USERNAME, $user[COL_USERNAME])->get(TBL__USERSPREFERENCES)->result_array();
            foreach($pref as $p) {
                if($pref[COL_PREFERENCETYPEID] == PREFERENCETYPE_INDUSTRYTYPE && $vacancy[COL_INDUSTRYTYPEID] != $pref[COL_PREFERENCEVALUE]) {
                    if($retval == true)
                }
            }
        } else {
            return false;
        }*/

        return $retval;
    }
}
