<?php
class Issue extends MY_Controller {
  function __construct() {
    parent::__construct();

    if(!IsLogin()) {
      redirect('site/home/login');
    }

    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEPUSKESMAS) {
      redirect('admin/dashboard');
    }
  }

  public function index() {
    $data['title'] = "Pemakaian";
    $this->template->load('main', 'admin/issue/index', $data);
  }

  public function index_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $IdPuskesmas = !empty($_POST['idPuskesmas'])?$_POST['idPuskesmas']:null;
    $dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    $dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');

    $ruser = GetLoggedUser();
    $orderdef = array(COL_DATEISSUE=>'desc');
    $orderables = array(null,COL_DATEISSUE,COL_NMPUSKESMAS,COL_NMSTOCK,null,null,COL_CREATEDON);
    $cols = array(COL_DATEISSUE,COL_NMPUSKESMAS,COL_NMSTOCK,COL_CREATEDBY);

    $queryAll = $this->db
    ->where('pus.KdTipe != ', 2)
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TSTOCKISSUE.".".COL_CREATEDBY,"left")
    ->join(TBL_TSTOCKDISTRIBUTION_ITEMS.' i','i.'.COL_UNIQ." = ".TBL_TSTOCKISSUE.".".COL_IDITEM,"left")
    ->join(TBL_TSTOCKDISTRIBUTION.' dist','dist.'.COL_UNIQ." = i.".COL_IDDISTRIBUTION,"left")
    ->join(TBL_TSTOCKRECEIPT.' r','r.'.COL_UNIQ." = i.".COL_IDRECEIPT,"left")
    ->join(TBL_MPUSKESMAS.' pus','pus.'.COL_IDPUSKESMAS." = dist.".COL_IDPUSKESMAS,"left")
    ->group_by(array(TBL_TSTOCKISSUE.'.'.COL_DATEISSUE,"pus.".COL_NMPUSKESMAS))
    ->get(TBL_TSTOCKISSUE);

    $i = 0;
    foreach($cols as $item){
      if($item == COL_NMPUSKESMAS) $item = 'pus.'.COL_NMPUSKESMAS;
      if($item == COL_CREATEDBY) $item = TBL_TSTOCKISSUE.'.'.COL_CREATEDBY;
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($dateFrom)) {
      $this->db->where(TBL_TSTOCKISSUE.'.'.COL_DATEISSUE.' >= ', $dateFrom);
    }
    if(!empty($dateTo)) {
      $this->db->where(TBL_TSTOCKISSUE.'.'.COL_DATEISSUE.' <= ', $dateTo);
    }
    if(!empty($IdPuskesmas)) {
      $this->db->where('dist.'.COL_IDPUSKESMAS, $IdPuskesmas);
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
        $order = $orderdef;
        $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('tstockissue.*, st.NmStock, st.NmSatuan, r.NmBatch, pus.NmPuskesmas, uc.Nm_FullName as Nm_CreatedBy, count(tstockissue.Uniq) as CountItem, max(tstockissue.CreatedOn) as LastCreatedOn, dist.IdPuskesmas')
    ->where('pus.KdTipe != ', 2)
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TSTOCKISSUE.".".COL_CREATEDBY,"left")
    ->join(TBL_TSTOCKDISTRIBUTION_ITEMS.' i','i.'.COL_UNIQ." = ".TBL_TSTOCKISSUE.".".COL_IDITEM,"left")
    ->join(TBL_TSTOCKDISTRIBUTION.' dist','dist.'.COL_UNIQ." = i.".COL_IDDISTRIBUTION,"left")
    ->join(TBL_TSTOCKRECEIPT.' r','r.'.COL_UNIQ." = i.".COL_IDRECEIPT,"left")
    ->join(TBL_MSTOCK.' st','st.'.COL_IDSTOCK." = r.".COL_IDSTOCK,"left")
    ->join(TBL_MPUSKESMAS.' pus','pus.'.COL_IDPUSKESMAS." = dist.".COL_IDPUSKESMAS,"left")
    ->order_by(TBL_TSTOCKISSUE.".".COL_CREATEDON, 'desc')
    ->group_by(array(TBL_TSTOCKISSUE.'.'.COL_DATEISSUE,"pus.".COL_NMPUSKESMAS))
    ->get_compiled_select(TBL_TSTOCKISSUE, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $data[] = array(
        '<a href="'.site_url('admin/issue/delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action"><i class="fas fa-trash"></i>&nbsp;HAPUS</a>&nbsp;'.
        '<a href="'.site_url('admin/issue/view-item/'.$r[COL_DATEISSUE].'/'.$r[COL_IDPUSKESMAS]).'" class="btn btn-xs btn-outline-info btn-view"><i class="fas fa-info-circle"></i>&nbsp;INFO</a>',
        date('Y-m-d', strtotime($r[COL_DATEISSUE])),
        $r[COL_NMPUSKESMAS],
        number_format($r['CountItem']),
        /*$r[COL_NMSTOCK],
        $r[COL_NMBATCH],
        number_format($r[COL_JUMLAH]),
        $r[COL_NMSATUAN],
        $r[COL_CREATEDBY],
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))*/
        date('Y-m-d H:i', strtotime($r['LastCreatedOn']))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $arrDet = array();
      $det = $this->input->post("IssueItems");
      $this->db->trans_begin();
      try {
        if(!empty($det)) {
          $det = json_decode(urldecode($det));
          foreach($det as $s) {
            /* CHECK STOCK */
            $qCheck = @"
            select
            i.Jumlah - coalesce((select sum(is_.Jumlah) from tstockissue is_ where is_.IdItem = i.Uniq),0) as JlhSisa
            from tstockdistribution_items i
            inner join tstockdistribution dist on dist.Uniq = i.IdDistribution
            where
              i.Uniq = ?
              and dist.DateDistribution <= ?
            ";
            $rcheck = $this->db->query($qCheck, array($s->IdItem, $this->input->post(COL_DATEISSUE)))->row_array();
            if(empty($rcheck)) {
              throw new Exception('No. BATCH tidak valid. Silakan periksa kembali.');
            }
            if($rcheck['JlhSisa'] < $s->Jumlah) {
              throw new Exception('Jumlah stok pada item BATCH No. <b>'.$rcheck[COL_NMBATCH].'</b> tidak mencukupi.<br />SISA STOK: <b>'.number_format($rcheck['JlhSisa']).'</b>');
            }
            /* CHECK STOCK */

            $arrDet[] = array(
              COL_IDITEM=>$s->IdItem,
              COL_DATEISSUE=>$this->input->post(COL_DATEISSUE),
              COL_JUMLAH=>toNum($s->Jumlah),
              COL_NMREMARKS=>$this->input->post(COL_NMREMARKS),

              COL_CREATEDBY => $ruser[COL_USERNAME],
              COL_CREATEDON => date('Y-m-d H:i:s')
            );
          }
        }

        if(!empty($arrDet)) {
          $res = $this->db->insert_batch(TBL_TSTOCKISSUE, $arrDet);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        } else {
          throw new Exception('Item tidak boleh kosong.');
        }

        $this->db->trans_commit();
        ShowJsonSuccess('INPUT DATA BERHASIL');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      /*ShowJsonError('Parameter tidak valid.');
      return;*/
      $data['IssueItems'] = json_encode(array());
      $this->load->view('admin/issue/form', $data);
    }
  }

  public function view($id) {
    $rdata = $this->db
    ->select('tstockissue.*, st.NmStock, st.NmSatuan, r.NmBatch, pus.NmPuskesmas, uc.Nm_FullName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TSTOCKISSUE.".".COL_CREATEDBY,"left")
    ->join(TBL_TSTOCKDISTRIBUTION_ITEMS.' i','i.'.COL_UNIQ." = ".TBL_TSTOCKISSUE.".".COL_IDITEM,"left")
    ->join(TBL_TSTOCKDISTRIBUTION.' dist','dist.'.COL_UNIQ." = i.".COL_IDDISTRIBUTION,"left")
    ->join(TBL_TSTOCKRECEIPT.' r','r.'.COL_UNIQ." = i.".COL_IDRECEIPT,"left")
    ->join(TBL_MSTOCK.' st','st.'.COL_IDSTOCK." = r.".COL_IDSTOCK,"left")
    ->join(TBL_MPUSKESMAS.' pus','pus.'.COL_IDPUSKESMAS." = dist.".COL_IDPUSKESMAS,"left")
    ->where(TBL_TSTOCKISSUE.'.'.COL_UNIQ, $id)
    ->get(TBL_TSTOCKISSUE)
    ->row_array();
    if(empty($rdata)) {
      show_error('Data tidak valid atau sudah dihapus');
      return;
    }

    $html = @
    "
    <div class='modal-header'>
    <h5 class='modal-title'>DETAIL PEMAKAIAN</h5>
    </div>
    <div class='modal-body p-0'>
    <table class='table table-striped mb-0'>
    <tr>
      <td>Tanggal</td><td width='10px'>:</td>
      <td>".$rdata[COL_DATEISSUE]."</td>
    </tr>
    <tr>
      <td>Puskesmas / Lokasi</td><td width='10px'>:</td>
      <td>".$rdata[COL_NMPUSKESMAS]."</td>
    </tr>
    <tr>
      <td>Obat</td><td width='10px'>:</td>
      <td>".$rdata[COL_NMSTOCK]."</td>
    </tr>
    <tr>
      <td>Batch</td><td width='10px'>:</td>
      <td>".$rdata[COL_NMBATCH]."</td>
    </tr>
    <tr>
      <td>Jumlah</td><td width='10px'>:</td>
      <td>".number_format($rdata[COL_JUMLAH]).' '.$rdata[COL_NMSATUAN]."</td>
    </tr>
    <tr>
      <td>Catatan</td><td width='10px'>:</td>
      <td>".$rdata[COL_NMREMARKS]."</td>
    </tr>
    </table>
    <p class=\"text-muted text-sm font-italic label-info mb-0 p-3\">
      Diinput oleh: <b>".$rdata['Nm_CreatedBy']."</b><br />
      Diinput pada: <b>".date('Y-m-d H:i:s', strtotime($rdata[COL_CREATEDON]))."</b>
    </p>
    </div>";
    echo $html;
  }

  public function view_item($tgl, $idPuskesmas) {
    $rdata = $this->db
    ->select('tstockissue.*, st.NmStock, st.NmSatuan, r.NmBatch, pus.NmPuskesmas, uc.Nm_FullName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TSTOCKISSUE.".".COL_CREATEDBY,"left")
    ->join(TBL_TSTOCKDISTRIBUTION_ITEMS.' i','i.'.COL_UNIQ." = ".TBL_TSTOCKISSUE.".".COL_IDITEM,"left")
    ->join(TBL_TSTOCKDISTRIBUTION.' dist','dist.'.COL_UNIQ." = i.".COL_IDDISTRIBUTION,"left")
    ->join(TBL_TSTOCKRECEIPT.' r','r.'.COL_UNIQ." = i.".COL_IDRECEIPT,"left")
    ->join(TBL_MSTOCK.' st','st.'.COL_IDSTOCK." = r.".COL_IDSTOCK,"left")
    ->join(TBL_MPUSKESMAS.' pus','pus.'.COL_IDPUSKESMAS." = dist.".COL_IDPUSKESMAS,"left")
    ->where(TBL_TSTOCKISSUE.'.'.COL_DATEISSUE, $tgl)
    ->where('dist.'.COL_IDPUSKESMAS, $idPuskesmas)
    ->order_by(TBL_TSTOCKISSUE.'.'.COL_CREATEDON, 'desc')
    ->get(TBL_TSTOCKISSUE)
    ->result_array();
    if(empty($rdata)) {
      show_error('Data tidak valid atau sudah dihapus');
      return;
    }
    $this->load->view('admin/issue/form_', array('rdata'=>$rdata));
  }

  public function delete($id) {
    $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_TSTOCKISSUE);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      return;
    }

    ShowJsonSuccess('Data berhasil dihapus.');
    return;
  }
}
?>
