<?php
class User extends MY_Controller {
  function __construct() {
    parent::__construct();

    if(!IsLogin()) {
      redirect('site/home/login');
    }

    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN) {
      redirect('admin/dashboard');
    }
  }

  /*public function mentor() {
    $data['title'] = "Pengguna";
    $this->db->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__USERS.".".COL_USERNAME,"inner");
    $this->db->join(TBL__ROLES,TBL__ROLES.'.'.COL_ROLEID." = ".TBL__USERS.".".COL_ROLEID,"inner");
    $this->db->where(TBL__USERS.".".COL_ROLEID, ROLEPSIKOLOG);
    $this->db->order_by(TBL__USERS.".".COL_USERNAME, 'asc');
    $data['res'] = $this->db->get(TBL__USERS)->result_array();
    $data['role'] = ROLEPSIKOLOG;
    //$this->load->view('user/index', $data);
    $this->template->load('main', 'admin/user/index', $data);
  }*/

  public function index() {
    $data['title'] = "Pengguna";
    $this->db->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__USERS.".".COL_USERNAME,"inner");
    $this->db->join(TBL__ROLES,TBL__ROLES.'.'.COL_ROLEID." = ".TBL__USERS.".".COL_ROLEID,"left");
    $this->db->join(TBL_MPUSKESMAS,TBL_MPUSKESMAS.'.'.COL_IDPUSKESMAS." = ".TBL__USERINFORMATION.".".COL_IDUNIT,"left");
    $this->db->where(TBL__USERS.".".COL_ROLEID." != ", ROLEADMIN);
    $this->db->order_by(TBL__USERS.".".COL_USERNAME, 'asc');
    $data['res'] = $this->db->get(TBL__USERS)->result_array();
    $data['role'] = '';
    //$this->load->view('user/index', $data);
    $this->template->load('main', 'admin/user/index', $data);
  }

  public function add($role='') {
    $data['title'] = "Form Pengguna";
    $data['role'] = $role;

    if(!empty($_POST)) {
      $this->form_validation->set_rules(array(
        /*array(
          'field' => COL_EMAIL,
          'label' => COL_EMAIL,
          'rules' => 'required|valid_email|is_unique[_userinformation.Email]',
          'errors' => array('is_unique' => 'Email sudah digunakan.')
        ),*/
        array(
          'field' => COL_PASSWORD,
          'label' => COL_PASSWORD,
          'rules' => 'required|min_length[5]',
          'errors' => array('min_length' => 'Password minimal terdiri dari 5 karakter.')
        ),
        array(
          'field' => 'ConfirmPassword',
          'label' => 'Repeat ConfirmPassword',
          'rules' => 'required|matches[Password]',
          'errors' => array('matches' => 'Kolom Ulangi Password wajib sama dengan Password.')
        )
      ));

      if(!$this->form_validation->run()) {
        $err = validation_errors();
        ShowJsonError($err);
        return false;
      }

      if(empty($role)) {
        $role = $this->input->post(COL_ROLEID);
      }
      $email = $this->input->post(COL_EMAIL);
      $verify = $this->input->post("Verify");

      $userdata = array(
        COL_USERNAME => $email,
        COL_PASSWORD => md5($this->input->post(COL_PASSWORD)),
        COL_ROLEID => $role
      );
      $userinfo = array(
        COL_USERNAME => $email,
        COL_EMAIL => $email,
        COL_IDUNIT => $this->input->post(COL_IDUNIT),
        COL_NM_FULLNAME => $this->input->post(COL_NM_FULLNAME),
        COL_DATE_REGISTERED => date('Y-m-d')
      );

      $res = true;
      $this->db->trans_begin();
      try {

        $res = $this->db->insert(TBL__USERS, $userdata);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $res = $this->db->insert(TBL__USERINFORMATION, $userinfo);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil', array('redirect'=>site_url('admin/user/index')));
        return;
      } catch (Exception $e) {
        $this->db->trans_rollback();
        ShowJsonError($e->getMessage());
        return;
      }
    }
    $this->template->load('main', 'admin/user/form', $data);
  }

  public function edit($id) {
    $id = GetDecryption($id);
    $rdata = $this->db
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL__USERS.".".COL_USERNAME,"inner")
    ->where(TBL__USERS.".".COL_USERNAME, $id)
    ->get(TBL__USERS)->row_array();

    if(empty($rdata)) {
      show_error('Data tidak ditemukan.');
      return;
    }

    $data['title'] = "Form Pengguna";
    $data['edit'] = true;
    $data['role'] = $rdata[COL_ROLEID];
    $data['data'] = $rdata;

    if(!empty($_POST)) {
      $verify = $this->input->post("Verify");
      $userinfo = array(
        COL_IDUNIT => $this->input->post(COL_IDUNIT),
        COL_NM_FULLNAME => $this->input->post(COL_NM_FULLNAME),
      );

      $res = true;
      $this->db->trans_begin();
      try {

        $res = $this->db->where(COL_USERNAME, $id)->update(TBL__USERINFORMATION, $userinfo);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $res = $this->db->where(COL_USERNAME, $id)->update(TBL__USERS, array(COL_ROLEID=>$this->input->post(COL_ROLEID)));
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil', array('redirect'=>site_url('admin/user/index')));
        return;
      } catch (Exception $e) {
        $this->db->trans_rollback();
        ShowJsonError($e->getMessage());
        return;
      }
    }
    $this->template->load('main', 'admin/user/form', $data);
  }

  public function delete() {
    $data = $this->input->post('cekbox');
    $deleted = 0;
    foreach ($data as $datum) {
      $res = $this->db->where(COL_USERNAME, $datum)->delete(TBL__USERS);
      if($res) {
        $deleted++;
      }
    }
    if($deleted){
        ShowJsonSuccess($deleted." data dihapus");
    }else{
        ShowJsonError("Tidak ada data dihapus");
    }
  }

  public function activate($opt = 0) {
    $data = $this->input->post('cekbox');
    $deleted = 0;
    foreach ($data as $datum) {
      if($opt == 0 || $opt == 1) {
        if($this->db->where(COL_USERNAME, $datum)->update(TBL__USERS, array(COL_ISSUSPEND=>$opt))) {
          $deleted++;
        }
        if($opt == 0) {
          $this->db->where(COL_USERNAME, $datum)->update(TBL__USERINFORMATION, array(COL_DATE_REGISTERED=>date('Y-m-d H:i:s')));
        }
      }
      else {
        if($this->db->where(COL_USERNAME, $datum)->update(TBL__USERS, array(COL_PASSWORD=>MD5('123456')))) {
          $deleted++;
        }
      }
    }
    if($deleted){
      ShowJsonSuccess($deleted." data diubah");
    }else{
      ShowJsonError("Tidak ada data yang diubah");
    }
  }
}
?>
