<?php
class Receipt extends MY_Controller {
  function __construct() {
    parent::__construct();

    if(!IsLogin()) {
      redirect('site/home/login');
    }

    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEOPERATOR) {
      redirect('admin/dashboard');
    }
  }

  public function index() {
    $data['title'] = "Penerimaan";
    $this->template->load('main', 'admin/receipt/index', $data);
  }

  public function index_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $IdStock = !empty($_POST['idStock'])?$_POST['idStock']:null;
    $dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    $dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');

    $ruser = GetLoggedUser();
    $orderdef = array(COL_DATERECEIPT=>'desc');
    $orderables = array(null,COL_DATERECEIPT,COL_NMSTOCK,COL_NMBATCH,null,null,COL_CREATEDON);
    $cols = array(COL_DATERECEIPT,COL_NMSTOCK,COL_NMBATCH,COL_NMSATUAN,COL_CREATEDBY);

    $queryAll = $this->db
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TSTOCKRECEIPT.".".COL_CREATEDBY,"left")
    ->join(TBL_MSTOCK.' st','st.'.COL_IDSTOCK." = ".TBL_TSTOCKRECEIPT.".".COL_IDSTOCK,"left")
    ->get(TBL_TSTOCKRECEIPT);

    $i = 0;
    foreach($cols as $item){
      if($item == COL_NMSTOCK) $item = 'st.'.COL_NMSTOCK;
      if($item == COL_NMSATUAN) $item = 'st.'.COL_NMSATUAN;
      if($item == COL_CREATEDBY) $item = TBL_TSTOCKRECEIPT.'.'.COL_CREATEDBY;
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($dateFrom)) {
      $this->db->where(TBL_TSTOCKRECEIPT.'.'.COL_DATERECEIPT.' >= ', $dateFrom);
    }
    if(!empty($dateTo)) {
      $this->db->where(TBL_TSTOCKRECEIPT.'.'.COL_DATERECEIPT.' <= ', $dateTo);
    }
    if(!empty($IdStock)) {
      $this->db->where(TBL_TSTOCKRECEIPT.'.'.COL_IDSTOCK, $IdStock);
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
        $order = $orderdef;
        $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('tstockreceipt.*, st.NmStock, st.NmSatuan, uc.Nm_FullName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TSTOCKRECEIPT.".".COL_CREATEDBY,"left")
    ->join(TBL_MSTOCK.' st','st.'.COL_IDSTOCK." = ".TBL_TSTOCKRECEIPT.".".COL_IDSTOCK,"left")
    ->order_by(TBL_TSTOCKRECEIPT.".".COL_CREATEDON, 'desc')
    ->get_compiled_select(TBL_TSTOCKRECEIPT, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $barcode = urlencode($r[COL_NMBATCH]);
      $barcodeUrl = "https://bwipjs-api.metafloor.com/?bcid=code128&text=$barcode&alttext=$barcode&timestamp=".date('YmdHis');
      $data[] = array(
        '<a href="'.site_url('admin/receipt/delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action"><i class="fas fa-trash"></i>&nbsp;HAPUS</a>&nbsp;'.
        '<a href="'.site_url('admin/receipt/view/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-info btn-view"><i class="fas fa-info-circle"></i>&nbsp;INFO</a>&nbsp;'.
        '<btn class="btn btn-xs btn-outline-success btn-barcode" data-url="'.$barcodeUrl.'" data-nmstock="'.$r[COL_NMSTOCK].'" data-expdate="'.date('d-m-Y', strtotime($r[COL_DATEEXPIRED])).'"><i class="fas fa-barcode"></i>&nbsp;BARCODE</btn>',
        date('Y-m-d', strtotime($r[COL_DATERECEIPT])),
        $r[COL_NMSTOCK],
        $r[COL_NMBATCH],
        number_format($r[COL_JUMLAH]),
        $r[COL_NMSATUAN],
        $r[COL_CREATEDBY],
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_IDSTOCK => $this->input->post(COL_IDSTOCK),
        COL_IDPABRIK => $this->input->post(COL_IDPABRIK),
        COL_IDSUPPLIER => $this->input->post(COL_IDSUPPLIER),
        COL_DATERECEIPT => $this->input->post(COL_DATERECEIPT),
        COL_NMBATCH => $this->input->post(COL_NMBATCH),
        COL_NMSUMBER => $this->input->post(COL_NMSUMBER),
        COL_HARGA => toNum($this->input->post(COL_HARGA)),
        COL_JUMLAH => toNum($this->input->post(COL_JUMLAH)),
        COL_TAHUN => $this->input->post(COL_TAHUN),
        COL_DATEEXPIRED => $this->input->post(COL_DATEEXPIRED),
        COL_NMREMARKS => $this->input->post(COL_NMREMARKS),

        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        /* check batch */
        $rChkBatch = $this->db
        ->where(COL_NMBATCH, $data[COL_NMBATCH])
        ->get(TBL_TSTOCKRECEIPT)
        ->row_array();
        if(!empty($rChkBatch)) {
          throw new Exception('Maaf, BATCH dengan kode '.$data[COL_NMBATCH].' sudah terdaftar di sistem. Silakan periksa kembali.');
        }
        /* check batch */

        $res = $this->db->insert(TBL_TSTOCKRECEIPT, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('INPUT DATA BERHASIL');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      /*ShowJsonError('Parameter tidak valid.');
      return;*/
      $this->load->view('admin/receipt/form');
    }
  }

  public function view($id) {
    $rdata = $this->db
    ->select('tstockreceipt.*, uc.Nm_FullName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TSTOCKRECEIPT.".".COL_CREATEDBY,"left")
    ->where(COL_UNIQ, $id)
    ->get(TBL_TSTOCKRECEIPT)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak ditemukan';
      return;
    }
    $this->load->view('admin/receipt/form', array('data'=>$rdata,'disabled'=>true));
  }

  public function delete($id) {
    $rdist = $this->db
    ->where(COL_IDRECEIPT, $id)
    ->get(TBL_TSTOCKDISTRIBUTION_ITEMS)
    ->row_array();
    if(!empty($rdist)) {
      ShowJsonError('Data tidak dapat dihapus karena sudah memiliki relasi dengan data DISTRIBUSI.');
      return;
    }

    $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_TSTOCKRECEIPT);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      return;
    }

    ShowJsonSuccess('Data berhasil dihapus.');
    return;
  }
}
