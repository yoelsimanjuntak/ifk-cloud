<?php
class Master extends MY_Controller {
  function __construct() {
    parent::__construct();

    if(!IsLogin()) {
      redirect('site/home/login');
    }

    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN) {
      redirect('admin/dashboard');
    }
  }

  public function kategori_index() {
    $data['title'] = "Kategori";
    $this->db->select('*,uc.Nm_FullName as Nm_CreatedBy, uu.Nm_FullName as Nm_UpdatedBy');
    $this->db->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_MKATEGORI.".".COL_CREATEDBY,"left");
    $this->db->join(TBL__USERINFORMATION.' uu','uu.'.COL_USERNAME." = ".TBL_MKATEGORI.".".COL_UPDATEDBY,"left");
    $this->db->where(COL_ISDELETED." != ", 1);
    $this->db->order_by(COL_SEQ, 'asc');
    $this->db->order_by(COL_NMKATEGORI, 'asc');
    $data['res'] = $this->db->get(TBL_MKATEGORI)->result_array();
    //$this->load->view('user/index', $data);
    $this->template->load('main', 'admin/master/index-kategori', $data);
  }

  public function kategori_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_NMKATEGORI => $this->input->post(COL_NMKATEGORI),
        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_MKATEGORI, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function kategori_edit($id) {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_NMKATEGORI => $this->input->post(COL_NMKATEGORI),
        COL_UPDATEDBY => $ruser[COL_USERNAME],
        COL_UPDATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_IDKATEGORI, $id)->update(TBL_MKATEGORI, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function kategori_delete() {
    $ruser = GetLoggedUser();
    $data = $this->input->post('cekbox');
    $deleted = 0;
    $this->db->trans_begin();
    try {
      foreach ($data as $datum) {
        $res = $this->db
        ->where(COL_IDKATEGORI, $datum)
        ->update(TBL_MKATEGORI, array(
          COL_ISDELETED=>1,
          COL_DELETEDBY=>$ruser[COL_USERNAME],
          COL_DELETEDON=>date('Y-m-d H:i:s')
        ));
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }
      }
      $this->db->trans_commit();
      ShowJsonSuccess('OK');
      return;
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($e->getMessage());
      return;
    }
    if($deleted){
        ShowJsonSuccess($deleted." data dihapus");
    }else{
        ShowJsonError("Tidak ada dihapus");
    }
  }

  public function puskesmas_index() {
    $data['title'] = "Puskesmas";
    $this->db->select('*,uc.Nm_FullName as Nm_CreatedBy, uu.Nm_FullName as Nm_UpdatedBy');
    $this->db->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_MPUSKESMAS.".".COL_CREATEDBY,"left");
    $this->db->join(TBL__USERINFORMATION.' uu','uu.'.COL_USERNAME." = ".TBL_MPUSKESMAS.".".COL_UPDATEDBY,"left");
    $this->db->where(COL_ISDELETED." != ", 1);
    $this->db->where(COL_KDTIPE, 1);
    $this->db->order_by(COL_NMPUSKESMAS, 'asc');
    $data['res'] = $this->db->get(TBL_MPUSKESMAS)->result_array();
    //$this->load->view('user/index', $data);
    $this->template->load('main', 'admin/master/index-puskesmas', $data);
  }

  public function puskesmas_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_NMPUSKESMAS => $this->input->post(COL_NMPUSKESMAS),
        COL_NMKAPUSKESMAS => $this->input->post(COL_NMKAPUSKESMAS),
        COL_NMKAPUSKESMASNIP => $this->input->post(COL_NMKAPUSKESMASNIP),
        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_MPUSKESMAS, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function puskesmas_edit($id) {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_NMPUSKESMAS => $this->input->post(COL_NMPUSKESMAS),
        COL_NMKAPUSKESMAS => $this->input->post(COL_NMKAPUSKESMAS),
        COL_NMKAPUSKESMASNIP => $this->input->post(COL_NMKAPUSKESMASNIP),
        COL_UPDATEDBY => $ruser[COL_USERNAME],
        COL_UPDATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_IDPUSKESMAS, $id)->update(TBL_MPUSKESMAS, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function puskesmas_delete() {
    $ruser = GetLoggedUser();
    $data = $this->input->post('cekbox');
    $deleted = 0;
    $this->db->trans_begin();
    try {
      foreach ($data as $datum) {
        $res = $this->db
        ->where(COL_IDPUSKESMAS, $datum)
        ->update(TBL_MPUSKESMAS, array(
          COL_ISDELETED=>1,
          COL_DELETEDBY=>$ruser[COL_USERNAME],
          COL_DELETEDON=>date('Y-m-d H:i:s')
        ));
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }
      }
      $this->db->trans_commit();
      ShowJsonSuccess('OK');
      return;
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($e->getMessage());
      return;
    }
    if($deleted){
        ShowJsonSuccess($deleted." data dihapus");
    }else{
        ShowJsonError("Tidak ada dihapus");
    }
  }

  public function factory_index() {
    $data['title'] = "Pabrik";
    $this->db->select('*,uc.Nm_FullName as Nm_CreatedBy, uu.Nm_FullName as Nm_UpdatedBy');
    $this->db->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_MPABRIK.".".COL_CREATEDBY,"left");
    $this->db->join(TBL__USERINFORMATION.' uu','uu.'.COL_USERNAME." = ".TBL_MPABRIK.".".COL_UPDATEDBY,"left");
    $this->db->where(COL_ISDELETED." != ", 1);
    $this->db->order_by(COL_NMPABRIK, 'asc');
    $data['res'] = $this->db->get(TBL_MPABRIK)->result_array();
    //$this->load->view('user/index', $data);
    $this->template->load('main', 'admin/master/index-factory', $data);
  }

  public function factory_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_NMPABRIK => $this->input->post(COL_NMPABRIK),
        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_MPABRIK, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function factory_edit($id) {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_NMPABRIK => $this->input->post(COL_NMPABRIK),
        COL_UPDATEDBY => $ruser[COL_USERNAME],
        COL_UPDATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_IDPABRIK, $id)->update(TBL_MPABRIK, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function factory_delete() {
    $ruser = GetLoggedUser();
    $data = $this->input->post('cekbox');
    $deleted = 0;
    $this->db->trans_begin();
    try {
      foreach ($data as $datum) {
        $res = $this->db
        ->where(COL_IDPABRIK, $datum)
        ->update(TBL_MPABRIK, array(
          COL_ISDELETED=>1,
          COL_DELETEDBY=>$ruser[COL_USERNAME],
          COL_DELETEDON=>date('Y-m-d H:i:s')
        ));
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }
      }
      $this->db->trans_commit();
      ShowJsonSuccess('OK');
      return;
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($e->getMessage());
      return;
    }
    if($deleted){
        ShowJsonSuccess($deleted." data dihapus");
    }else{
        ShowJsonError("Tidak ada dihapus");
    }
  }

  public function supplier_index() {
    $data['title'] = "Pemasok";
    $this->db->select('*,uc.Nm_FullName as Nm_CreatedBy, uu.Nm_FullName as Nm_UpdatedBy');
    $this->db->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_MSUPPLIER.".".COL_CREATEDBY,"left");
    $this->db->join(TBL__USERINFORMATION.' uu','uu.'.COL_USERNAME." = ".TBL_MSUPPLIER.".".COL_UPDATEDBY,"left");
    $this->db->where(COL_ISDELETED." != ", 1);
    $this->db->order_by(COL_NMSUPPLIER, 'asc');
    $data['res'] = $this->db->get(TBL_MSUPPLIER)->result_array();
    //$this->load->view('user/index', $data);
    $this->template->load('main', 'admin/master/index-supplier', $data);
  }

  public function supplier_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_NMSUPPLIER => $this->input->post(COL_NMSUPPLIER),
        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_MSUPPLIER, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function supplier_edit($id) {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_NMSUPPLIER => $this->input->post(COL_NMSUPPLIER),
        COL_UPDATEDBY => $ruser[COL_USERNAME],
        COL_UPDATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_IDSUPPLIER, $id)->update(TBL_MSUPPLIER, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function supplier_delete() {
    $ruser = GetLoggedUser();
    $data = $this->input->post('cekbox');
    $deleted = 0;
    $this->db->trans_begin();
    try {
      foreach ($data as $datum) {
        $res = $this->db
        ->where(COL_IDSUPPLIER, $datum)
        ->update(TBL_MSUPPLIER, array(
          COL_ISDELETED=>1,
          COL_DELETEDBY=>$ruser[COL_USERNAME],
          COL_DELETEDON=>date('Y-m-d H:i:s')
        ));
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }
      }
      $this->db->trans_commit();
      ShowJsonSuccess('OK');
      return;
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($e->getMessage());
      return;
    }
    if($deleted){
        ShowJsonSuccess($deleted." data dihapus");
    }else{
        ShowJsonError("Tidak ada dihapus");
    }
  }

  public function stock_index() {
    $data['title'] = "Obat";
    $this->db->select('mstock.*, kat.NmKategori, uc.Nm_FullName as Nm_CreatedBy, uu.Nm_FullName as Nm_UpdatedBy');
    $this->db->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_MSTOCK.".".COL_CREATEDBY,"left");
    $this->db->join(TBL__USERINFORMATION.' uu','uu.'.COL_USERNAME." = ".TBL_MSTOCK.".".COL_UPDATEDBY,"left");
    $this->db->join(TBL_MKATEGORI.' kat','kat.'.COL_IDKATEGORI." = ".TBL_MSTOCK.".".COL_IDKATEGORI,"left");
    $this->db->where(TBL_MSTOCK.'.'.COL_ISDELETED." != ", 1);
    $this->db->order_by(COL_NMSTOCK, 'asc');
    $data['res'] = $this->db->get(TBL_MSTOCK)->result_array();
    //$this->load->view('user/index', $data);
    $this->template->load('main', 'admin/master/index-stock', $data);
  }

  public function stock_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_IDKATEGORI => $this->input->post(COL_IDKATEGORI),
        COL_NMSTOCK => $this->input->post(COL_NMSTOCK),
        COL_KDSTOCK => $this->input->post(COL_KDSTOCK),
        COL_NMSATUAN => $this->input->post(COL_NMSATUAN),
        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_MSTOCK, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function stock_edit($id) {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_IDKATEGORI => $this->input->post(COL_IDKATEGORI),
        COL_NMSTOCK => $this->input->post(COL_NMSTOCK),
        COL_KDSTOCK => $this->input->post(COL_KDSTOCK),
        COL_NMSATUAN => $this->input->post(COL_NMSATUAN),
        COL_UPDATEDBY => $ruser[COL_USERNAME],
        COL_UPDATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_IDSTOCK, $id)->update(TBL_MSTOCK, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('OK');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid.');
      return;
    }
  }

  public function stock_delete() {
    $ruser = GetLoggedUser();
    $data = $this->input->post('cekbox');
    $deleted = 0;
    $this->db->trans_begin();
    try {
      foreach ($data as $datum) {
        $res = $this->db
        ->where(COL_IDSTOCK, $datum)
        ->update(TBL_MSTOCK, array(
          COL_ISDELETED=>1,
          COL_DELETEDBY=>$ruser[COL_USERNAME],
          COL_DELETEDON=>date('Y-m-d H:i:s')
        ));
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }
      }
      $this->db->trans_commit();
      ShowJsonSuccess('OK');
      return;
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($e->getMessage());
      return;
    }
    if($deleted){
        ShowJsonSuccess($deleted." data dihapus");
    }else{
        ShowJsonError("Tidak ada dihapus");
    }
  }
}
