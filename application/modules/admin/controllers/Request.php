<?php
class Request extends MY_Controller {
  function __construct() {
    parent::__construct();

    if(!IsLogin()) {
      redirect('site/home/login');
    }

    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEPUSKESMAS) {
      redirect('admin/dashboard');
    }
  }

  public function index() {
    $data['title'] = "Permintaan";
    $this->template->load('main', 'admin/request/index', $data);
  }

  public function index_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $IdPuskesmas = !empty($_POST['idPuskesmas'])?$_POST['idPuskesmas']:null;
    $IsOutstanding = !empty($_POST['isOutstanding'])?$_POST['isOutstanding']:null;
    $dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    $dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');

    $ruser = GetLoggedUser();
    $orderdef = array(COL_DATEREQUEST=>'desc');
    $orderables = array(null,COL_DATEREQUEST,COL_NMPUSKESMAS,COL_NMREQUEST,null,null,COL_CREATEDON);
    $cols = array(COL_DATEREQUEST,COL_NMPUSKESMAS,COL_NMREQUEST,COL_CREATEDBY);

    $queryAll = $this->db
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TREQUEST.".".COL_CREATEDBY,"left")
    ->join(TBL_MPUSKESMAS.' pus','pus.'.COL_IDPUSKESMAS." = ".TBL_TREQUEST.".".COL_IDPUSKESMAS,"left")
    ->get(TBL_TREQUEST);

    $i = 0;
    foreach($cols as $item){
      if($item == COL_NMPUSKESMAS) $item = 'pus.'.COL_NMPUSKESMAS;
      if($item == COL_CREATEDBY) $item = TBL_TREQUEST.'.'.COL_CREATEDBY;
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($dateFrom)) {
      $this->db->where(TBL_TREQUEST.'.'.COL_DATEREQUEST.' >= ', $dateFrom);
    }
    if(!empty($dateTo)) {
      $this->db->where(TBL_TREQUEST.'.'.COL_DATEREQUEST.' <= ', $dateTo);
    }
    if(!empty($IdPuskesmas)) {
      $this->db->where(TBL_TREQUEST.'.'.COL_IDPUSKESMAS, $IdPuskesmas);
    }
    if(!empty($IsOutstanding)) {
      $this->db->where(TBL_TREQUEST.'.'.COL_ISPROCESSED, 0);
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
        $order = $orderdef;
        $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('trequest.*, pus.NmPuskesmas, uc.Nm_FullName as Nm_CreatedBy, (select count(*) from trequest_items it where it.IdRequest = trequest.Uniq) as NumItems')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TREQUEST.".".COL_CREATEDBY,"left")
    ->join(TBL_MPUSKESMAS.' pus','pus.'.COL_IDPUSKESMAS." = ".TBL_TREQUEST.".".COL_IDPUSKESMAS,"left")
    ->order_by(TBL_TREQUEST.".".COL_CREATEDON, 'desc')
    ->get_compiled_select(TBL_TREQUEST, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $data[] = array(
        '<a href="'.site_url('admin/request/delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action"><i class="fas fa-trash"></i>&nbsp;HAPUS</a>&nbsp;'.
        '<a href="'.site_url('admin/request/view/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-info btn-view"><i class="fas fa-info-circle"></i>&nbsp;INFO</a>&nbsp;'.
        ($r[COL_ISPROCESSED]?'':'<a href="'.site_url('admin/request/check/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-primary btn-check"><i class="fas fa-check"></i></a>'),
        date('Y-m-d', strtotime($r[COL_DATEREQUEST])),
        $r[COL_NMPUSKESMAS],
        $r[COL_NMREQUEST],
        number_format($r['NumItems']),
        ($r[COL_ISPROCESSED]?'SUDAH':'BELUM'),
        $r[COL_CREATEDBY],
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $arrDet = array();
      $det = $this->input->post("ReqItems");
      $data = array(
        COL_IDPUSKESMAS => $this->input->post(COL_IDPUSKESMAS),
        COL_NMREQUEST => $this->input->post(COL_NMREQUEST),
        COL_DATEREQUEST => $this->input->post(COL_DATEREQUEST),
        COL_NMREMARKS => $this->input->post(COL_NMREMARKS),

        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TREQUEST, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $idDist = $this->db->insert_id();
        if(!empty($det)) {
          $det = json_decode(urldecode($det));
          foreach($det as $s) {

            $arrDet[] = array(
              COL_IDREQUEST=>$idDist,
              COL_IDSTOCK=>$s->IdStock,
              COL_JLHREQUEST=>toNum($s->Jumlah)
            );
          }
        }

        if(!empty($arrDet)) {
          $res = $this->db->insert_batch(TBL_TREQUEST_ITEMS, $arrDet);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        } else {
          throw new Exception('Item tidak boleh kosong.');
        }

        $this->db->trans_commit();
        ShowJsonSuccess('INPUT DATA BERHASIL');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      /*ShowJsonError('Parameter tidak valid.');
      return;*/
      $data['ReqItems'] = json_encode(array());
      $this->load->view('admin/request/form', $data);
    }
  }

  public function view($id) {
    $rdata = $this->db
    ->select('trequest.*, uc.Nm_FullName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TREQUEST.".".COL_CREATEDBY,"left")
    ->where(COL_UNIQ, $id)
    ->get(TBL_TREQUEST)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak ditemukan';
      return;
    }

    $arrdet = array();
    $rdet = $this->db
    ->select('trequest_items.*, st.NmStock, st.NmSatuan')
    ->join(TBL_MSTOCK.' st','st.'.COL_IDSTOCK." = trequest_items.".COL_IDSTOCK,"left")
    ->where(COL_IDREQUEST, $rdata[COL_UNIQ])
    ->get(TBL_TREQUEST_ITEMS)
    ->result_array();
    foreach($rdet as $det) {
      $arrdet[] = array(
        'IdStock'=> $det[COL_IDSTOCK],
        'NmStock'=> $det[COL_NMSTOCK],
        'NmSatuan'=> $det[COL_NMSATUAN],
        'Jumlah'=>$det[COL_JLHREQUEST]
      );
    }
    $this->load->view('admin/request/form', array('ReqItems'=>json_encode($arrdet),'data'=>$rdata,'disabled'=>true));
  }

  public function delete($id) {
    $rissue = $this->db
    ->where("IdRequestItem in (select i.Uniq from trequest_items i where i.IdRequest = $id)", null, false)
    ->get(TBL_TSTOCKDISTRIBUTION_ITEMS)
    ->row_array();
    if(!empty($rissue)) {
      ShowJsonError('Data tidak dapat dihapus karena sudah memiliki relasi dengan data DISTRIBUSI.');
      return;
    }

    $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_TREQUEST);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      return;
    }

    ShowJsonSuccess('Data berhasil dihapus.');
    return;
  }

  public function check($id) {
    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TREQUEST, array(COL_ISPROCESSED=>1));
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      return;
    }

    ShowJsonSuccess('Data berhasil diubah.');
    return;
  }
}
 ?>
