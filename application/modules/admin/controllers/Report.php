<?php
class Report extends MY_Controller {
  function __construct() {
    parent::__construct();

    if(!IsLogin()) {
      redirect('site/home/login');
    }
  }

  public function rekapitulasi() {
    $data['title'] = "Rekapitulasi Obat";
    $data['cetak'] = $cetak = $this->input->get("cetak");
    $month = $this->input->get("month") ? $this->input->get("month") : date('m');
    $year = $this->input->get("year") ? $this->input->get("year") : date('Y');

    if(!empty($month) && !empty($year)) {
      $dateFrom = $year.'-'.$month.'-01';
      $dateTo = date("Y-m-d", strtotime("+1 month", strtotime($dateFrom)));

      $q = @"
      select
r.IdStock,
r.NmSumber,
st.NmStock,
st.NmSatuan,
r.DateExpired,
(
	select coalesce(sum(r_.Jumlah),0)
	from tstockreceipt r_
	where
		r_.IdStock = r.IdStock
		and r_.DateReceipt < '$dateFrom'
) as PrevReceipt,
/*(
	select coalesce(sum(is_.Jumlah),0)
	from tstockdistribution_items is_
	inner join tstockdistribution dist_ on dist_.Uniq = is_.IdDistribution
	inner join tstockreceipt r_ on r_.Uniq = is_.IdReceipt
	where
		r_.IdStock = r.IdStock
		and dist_.DateDistribution < '$dateFrom'
) as PrevDistribution,*/
(
  select coalesce(sum(i_.Jumlah),0) as JlhIssue
  from tstockissue i_
  inner join tstockdistribution_items is_ on is_.Uniq = i_.IdItem
  inner join tstockdistribution dist_ on dist_.Uniq = is_.IdDistribution
  inner join tstockreceipt r_ on r_.Uniq = is_.IdReceipt
  where
    r_.IdStock = r.IdStock
    and i_.DateIssue < '$dateFrom'
) as PrevIssue,
(
	select coalesce(sum(r_.Jumlah),0)
	from tstockreceipt r_
	where
		r_.IdStock = r.IdStock
		and r_.DateReceipt >= '$dateFrom'
		and r_.DateReceipt < '$dateTo'
) as TotalReceipt
from tstockreceipt r
left join mstock st on st.IdStock = r.IdStock
group by r.IdStock
order by st.NmStock
      ";
      $data['res'] = $this->db->query($q)->result_array();
    }

    if($cetak) $this->load->view('admin/report/rekapitulasi_', $data);
    else $this->template->load('main', 'admin/report/rekapitulasi', $data);
  }

  public function stock_opname() {
    $data['title'] = "Stock Opname";
    $data['cetak'] = $cetak = $this->input->get("cetak");
    $month = $this->input->get("month") ? $this->input->get("month") : date('m');
    $year = $this->input->get("year") ? $this->input->get("year") : date('Y');

    if(!empty($month) && !empty($year)) {
      $dateFrom = $year.'-'.$month.'-01';
      $dateTo = date("Y-m-d", strtotime("+1 month", strtotime($dateFrom)));

      $q = @"
      select
r.IdStock,
r.NmSumber,
st.NmStock,
st.NmSatuan,
r.DateExpired,
(
	select coalesce(sum(r_.Jumlah),0)
	from tstockreceipt r_
	where
		r_.IdStock = r.IdStock
		and r_.DateReceipt < '$dateFrom'
) as PrevReceipt,
(
	select coalesce(sum(is_.Jumlah),0)
	from tstockdistribution_items is_
	inner join tstockdistribution dist_ on dist_.Uniq = is_.IdDistribution
	inner join tstockreceipt r_ on r_.Uniq = is_.IdReceipt
	where
		r_.IdStock = r.IdStock
		and dist_.DateDistribution < '$dateFrom'
) as PrevDistribution,
(
  select coalesce(sum(i_.Jumlah),0) as JlhIssue
  from tstockissue i_
  inner join tstockdistribution_items is_ on is_.Uniq = i_.IdItem
  inner join tstockdistribution dist_ on dist_.Uniq = is_.IdDistribution
  inner join tstockreceipt r_ on r_.Uniq = is_.IdReceipt
  where
    r_.IdStock = r.IdStock
    and i_.DateIssue < '$dateFrom'
) as PrevIssue,
(
	select coalesce(sum(r_.Jumlah),0)
	from tstockreceipt r_
	where
		r_.IdStock = r.IdStock
		and r_.DateReceipt >= '$dateFrom'
		and r_.DateReceipt < '$dateTo'
) as TotalReceipt,
(
	select coalesce(sum(is_.Jumlah),0)
	from tstockdistribution_items is_
	inner join tstockdistribution dist_ on dist_.Uniq = is_.IdDistribution
	inner join tstockreceipt r_ on r_.Uniq = is_.IdReceipt
  inner join mpuskesmas pus on pus.IdPuskesmas = dist_.IdPuskesmas
	where
		r_.IdStock = r.IdStock
    and pus.KdTipe != 2
    and dist_.DateDistribution >= '$dateFrom'
		and dist_.DateDistribution < '$dateTo'
) as TotalDistribution,
(
	select coalesce(sum(is_.Jumlah),0)
	from tstockdistribution_items is_
	inner join tstockdistribution dist_ on dist_.Uniq = is_.IdDistribution
	inner join tstockreceipt r_ on r_.Uniq = is_.IdReceipt
  inner join mpuskesmas pus on pus.IdPuskesmas = dist_.IdPuskesmas
	where
		r_.IdStock = r.IdStock
    and pus.KdTipe = 2
    and dist_.DateDistribution >= '$dateFrom'
		and dist_.DateDistribution < '$dateTo'
) as TotalDistributionKhusus
from tstockreceipt r
left join mstock st on st.IdStock = r.IdStock
group by r.IdStock
order by st.NmStock
      ";
      $data['res'] = $this->db->query($q)->result_array();
    }

    if($cetak) $this->load->view('admin/report/stockopname_', $data);
    else $this->template->load('main', 'admin/report/stockopname', $data);
  }

  public function stock_puskesmas() {
    $data['title'] = "Stock Puskesmas";
    $data['cetak'] = $cetak = $this->input->get("cetak");
    $month = $this->input->get("month") ? $this->input->get("month") : date('m');
    $year = $this->input->get("year") ? $this->input->get("year") : date('Y');
    $puskesmas = $this->input->get("idPuskesmas") ? $this->input->get("idPuskesmas") : null;

    if(!empty($month) && !empty($year) && !empty($puskesmas)) {
      $dateFrom = $year.'-'.$month.'-01';
      $dateTo = date("Y-m-d", strtotime("+1 month", strtotime($dateFrom)));
      $lastmonth = date('Y-m-d', strtotime($dateFrom.' -1 month'));

      $q = @"
      select
r.IdStock,
r.NmSumber,
st.NmStock,
st.NmSatuan,
r.DateExpired,
(
	select coalesce(sum(is_.Jumlah),0)
	from tstockdistribution_items is_
	inner join tstockdistribution dist_ on dist_.Uniq = is_.IdDistribution
	inner join tstockreceipt r_ on r_.Uniq = is_.IdReceipt
	where
		r_.IdStock = r.IdStock
		and dist_.DateDistribution < '$lastmonth'
    and dist_.IdPuskesmas = '$puskesmas'
) as PrevDistribution,
(
  select coalesce(sum(i_.Jumlah),0) as JlhIssue
  from tstockissue i_
  inner join tstockdistribution_items is_ on is_.Uniq = i_.IdItem
  inner join tstockdistribution dist_ on dist_.Uniq = is_.IdDistribution
  inner join tstockreceipt r_ on r_.Uniq = is_.IdReceipt
  where
    r_.IdStock = r.IdStock
    and i_.DateIssue < '$dateFrom'
    and dist_.IdPuskesmas = '$puskesmas'
) as PrevIssue,
(
	select coalesce(sum(is_.Jumlah),0)
	from tstockdistribution_items is_
	inner join tstockdistribution dist_ on dist_.Uniq = is_.IdDistribution
	inner join tstockreceipt r_ on r_.Uniq = is_.IdReceipt
	where
		r_.IdStock = r.IdStock
    and dist_.DateDistribution >= '$lastmonth'
		and dist_.DateDistribution < '$dateFrom'
    and dist_.IdPuskesmas = '$puskesmas'
) as CurrDistribution,
(
  select coalesce(sum(i_.Jumlah),0) as JlhIssue
  from tstockissue i_
  inner join tstockdistribution_items is_ on is_.Uniq = i_.IdItem
  inner join tstockdistribution dist_ on dist_.Uniq = is_.IdDistribution
  inner join tstockreceipt r_ on r_.Uniq = is_.IdReceipt
  where
    r_.IdStock = r.IdStock
    and dist_.DateDistribution >= '$lastmonth'
		and dist_.DateDistribution < '$dateFrom'
    and dist_.IdPuskesmas = '$puskesmas'
) as CurrIssue
from tstockreceipt r
left join mstock st on st.IdStock = r.IdStock
group by r.IdStock
order by st.NmStock
      ";
      $data['res'] = $this->db->query($q)->result_array();
    }

    if($cetak) $this->load->view('admin/report/stockpuskesmas_', $data);
    else $this->template->load('main', 'admin/report/stockpuskesmas', $data);
  }

  public function lplpo() {
    $data['title'] = "LPLPO - Laporan Pemakaian dan Lembar Permintaan Obat";
    $data['cetak'] = $cetak = $this->input->get("cetak");
    $data['month'] = $month = $this->input->get("month") ? $this->input->get("month") : date('m');
    $data['year'] = $year = $this->input->get("year") ? $this->input->get("year") : date('Y');
    $puskesmas = $this->input->get("idPuskesmas") ? $this->input->get("idPuskesmas") : null;

    if(!empty($month) && !empty($year) && !empty($puskesmas)) {
      $dateFrom = $year.'-'.$month.'-01';
      $dateTo = date("Y-m-d", strtotime("+1 month", strtotime($dateFrom)));
      $prevmonth = date('Y-m-01', strtotime($dateFrom.' -1 month'));
      $monthprev = date('m', strtotime($prevmonth));
      $yearprev = date('Y', strtotime($prevmonth));

      $qkategori = @"
      select * from mkategori
      order by Seq
      ";
      $rkategori = $this->db->query($qkategori)->result_array();
      $rdata = array();
      foreach($rkategori as $kat) {
        $idkat = $kat[COL_IDKATEGORI];
        $qdata = @"
        select
        0 as ROWSPAN,
        NmStock,
        NmSatuan,
        Harga,
        NmSumber,
        SUM(CASE
            WHEN TIPE = 'DIST'
            THEN TotalQty
            ELSE 0
        END) AS CurrDist,
        SUM(CASE
            WHEN TIPE = 'ISSUE'
            THEN TotalQty
            ELSE 0
        END) AS CurrIssue,
        SUM(CASE
            WHEN TIPE = 'PREV_DIST'
            THEN TotalQty
            ELSE 0
        END) AS PrevDist,
        SUM(CASE
            WHEN TIPE = 'PREV_ISSUE'
            THEN TotalQty
            ELSE 0
        END) AS PrevIssue,
        (select
        	sum(reqit_.JlhRequest) as TotalReq
        	from trequest req_
        	left join trequest_items reqit_ on reqit_.IdRequest = req_.Uniq
        	left join mstock st_ on st_.IdStock = reqit_.IdStock
        	where
        		req_.IdPuskesmas = $puskesmas
        		and reqit_.IdStock = tbl.IdStock
        		and MONTH(req_.DateRequest) = $month
        		and YEAR(req_.DateRequest) = $year
        ) as CurrRequest
        from (
          select
          'DIST' as TIPE,
          st.NmStock,
          st.NmSatuan,
          st.IdStock,
          r.Harga,
          r.NmSumber,
          sum(dit.Jumlah) as TotalQty
          from mstock st
          left join tstockreceipt r on r.IdStock = st.IdStock
          left join tstockdistribution_items dit on dit.IdReceipt = r.Uniq
          left join tstockdistribution dist on dist.Uniq = dit.IdDistribution
          where
          	dist.IdPuskesmas = $puskesmas
            and st.IdKategori = $idkat
          	and MONTH(dist.DateDistribution) = $monthprev
          	and YEAR(dist.DateDistribution) = $yearprev
          group by NmStock, NmSatuan, Harga, NmSumber

          union all

          select
          'ISSUE' as TIPE,
          st.NmStock,
          st.NmSatuan,
          st.IdStock,
          r.Harga,
          r.NmSumber,
          sum(i.Jumlah) as TotalQty
          from mstock st
          left join tstockreceipt r on r.IdStock = st.IdStock
          left join tstockdistribution_items dit on dit.IdReceipt = r.Uniq
          left join tstockdistribution dist on dist.Uniq = dit.IdDistribution
          left join tstockissue i on i.IdItem = dit.Uniq
          where
          	dist.IdPuskesmas = $puskesmas
            and st.IdKategori = $idkat
          	and MONTH(i.DateIssue) = $month
          	and YEAR(i.DateIssue) = $year
          group by NmStock, NmSatuan, Harga, NmSumber

          union all

          select
          'PREV_DIST' as TIPE,
          st.NmStock,
          st.NmSatuan,
          st.IdStock,
          r.Harga,
          r.NmSumber,
          sum(dit.Jumlah) as TotalQty
          from mstock st
          left join tstockreceipt r on r.IdStock = st.IdStock
          left join tstockdistribution_items dit on dit.IdReceipt = r.Uniq
          left join tstockdistribution dist on dist.Uniq = dit.IdDistribution
          where
          	dist.IdPuskesmas = $puskesmas
            and st.IdKategori = $idkat
          	and dist.DateDistribution < '$prevmonth'
          group by NmStock, NmSatuan, Harga, NmSumber

          union all

          select
          'PREV_ISSUE' as TIPE,
          st.NmStock,
          st.NmSatuan,
          st.IdStock,
          r.Harga,
          r.NmSumber,
          sum(i.Jumlah) as TotalQty
          from mstock st
          left join tstockreceipt r on r.IdStock = st.IdStock
          left join tstockdistribution_items dit on dit.IdReceipt = r.Uniq
          left join tstockdistribution dist on dist.Uniq = dit.IdDistribution
          left join tstockissue i on i.IdItem = dit.Uniq
          where
          	dist.IdPuskesmas = $puskesmas
            and st.IdKategori = $idkat
          	and i.DateIssue < '$dateFrom'
          group by NmStock, NmSatuan, Harga, NmSumber
        ) tbl
        group by NmStock, NmSatuan, Harga, NmSumber
        ";
        $rdata_ = $this->db->query($qdata)->result_array();
        $cdata_ = array();
        foreach($rdata_ as $dat) {
          if(!array_key_exists($dat[COL_NMSTOCK], $cdata_)) $cdata_[$dat[COL_NMSTOCK]] = 1;
          else $cdata_[$dat[COL_NMSTOCK]] += 1;
        }
        foreach($rdata_ as $idx => $dat) {
          $rdata_[$idx]['ROWSPAN'] = $cdata_[$dat[COL_NMSTOCK]];
        }
        $rdata[$kat[COL_IDKATEGORI]] = $rdata_;
      }
      $data['kategori'] = $rkategori;
      $data['data'] = $rdata;
    }

    if($cetak) $this->load->view('admin/report/lplpo_', $data);
    else $this->template->load('main', 'admin/report/lplpo', $data);
  }
}
?>
