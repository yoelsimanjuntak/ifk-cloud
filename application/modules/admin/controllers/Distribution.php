<?php
class Distribution extends MY_Controller {
  function __construct() {
    parent::__construct();

    if(!IsLogin()) {
      redirect('site/home/login');
    }

    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN) {
      redirect('admin/dashboard');
    }
  }

  public function index() {
    $data['title'] = "Distribusi";
    $this->template->load('main', 'admin/distribution/index', $data);
  }
  public function index_khusus() {
    $data['title'] = "Distribusi Khusus";
    $this->template->load('main', 'admin/distribution/index-khusus', $data);
  }

  public function index_load($isKhusus=0) {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $IdPuskesmas = !empty($_POST['idPuskesmas'])?$_POST['idPuskesmas']:null;
    $dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    $dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');

    $ruser = GetLoggedUser();
    $orderdef = array(COL_DATEDISTRIBUTION=>'desc');
    if($isKhusus) {
      $orderables = array(null,COL_DATEDISTRIBUTION,COL_NMREFERENSI,null,COL_NMREMARKS,null,COL_CREATEDON);
      $cols = array(COL_DATEDISTRIBUTION,COL_NMREFERENSI,COL_NMREMARKS,COL_CREATEDBY);
    } else {
      $orderables = array(null,COL_DATEDISTRIBUTION,COL_NMPUSKESMAS,COL_NMREFERENSI,null,null,COL_CREATEDON);
      $cols = array(COL_DATEDISTRIBUTION,COL_NMPUSKESMAS,COL_NMREFERENSI,COL_CREATEDBY);
    }

    if($isKhusus) {
      $this->db->where('pus.KdTipe', 2);
    } else {
      $this->db->where('pus.KdTipe != ', 2);
    }
    $queryAll = $this->db
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TSTOCKDISTRIBUTION.".".COL_CREATEDBY,"left")
    ->join(TBL_MPUSKESMAS.' pus','pus.'.COL_IDPUSKESMAS." = ".TBL_TSTOCKDISTRIBUTION.".".COL_IDPUSKESMAS,"left")
    ->get(TBL_TSTOCKDISTRIBUTION);

    $i = 0;
    foreach($cols as $item){
      if($item == COL_NMPUSKESMAS) $item = 'pus.'.COL_NMPUSKESMAS;
      if($item == COL_CREATEDBY) $item = TBL_TSTOCKDISTRIBUTION.'.'.COL_CREATEDBY;
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($dateFrom)) {
      $this->db->where(TBL_TSTOCKDISTRIBUTION.'.'.COL_DATEDISTRIBUTION.' >= ', $dateFrom);
    }
    if(!empty($dateTo)) {
      $this->db->where(TBL_TSTOCKDISTRIBUTION.'.'.COL_DATEDISTRIBUTION.' <= ', $dateTo);
    }
    if(!empty($IdPuskesmas)) {
      $this->db->where(TBL_TSTOCKDISTRIBUTION.'.'.COL_IDPUSKESMAS, $IdPuskesmas);
    }
    if($isKhusus) {
      $this->db->where('pus.'.COL_KDTIPE, 2);
    } else {
      $this->db->where('pus.'.COL_KDTIPE." != ", 2);
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('tstockdistribution.*, pus.NmPuskesmas, uc.Nm_FullName as Nm_CreatedBy, (select count(*) from tstockdistribution_items it where it.IdDistribution = tstockdistribution.Uniq) as NumItems')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TSTOCKDISTRIBUTION.".".COL_CREATEDBY,"left")
    ->join(TBL_MPUSKESMAS.' pus','pus.'.COL_IDPUSKESMAS." = ".TBL_TSTOCKDISTRIBUTION.".".COL_IDPUSKESMAS,"left")
    ->order_by(TBL_TSTOCKDISTRIBUTION.".".COL_CREATEDON, 'desc')
    ->get_compiled_select(TBL_TSTOCKDISTRIBUTION, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      if($ruser[COL_ROLEID] != ROLEPUSKESMAS) {
        $htmlBtn .= '<a href="'.site_url('admin/distribution/delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action"><i class="fas fa-trash"></i>&nbsp;HAPUS</a>&nbsp;';
      }
      $htmlBtn .= '<a href="'.site_url('admin/distribution/view/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-info btn-view"><i class="fas fa-info-circle"></i>&nbsp;INFO</a>&nbsp;';
      $htmlBtn .= @'<button type="button" class="btn btn-outline-success btn-xs dropdown-toggle" data-toggle="dropdown">
      <i class="far fa-print"></i>&nbsp;CETAK
      </button>
      <div class="dropdown-menu">
        <a class="dropdown-item" target="_blank" href="'.site_url('admin/distribution/cetak-konsep/'.$r[COL_UNIQ]).'">KONSEP LEMBAR PEMBERIAN OBAT</a>
        <a class="dropdown-item" target="_blank" href="'.site_url('admin/distribution/cetak-ba/'.$r[COL_UNIQ]).'">BERITA ACARA</a>
        <a class="dropdown-item" target="_blank" href="'.site_url('admin/distribution/cetak-sbbk/'.$r[COL_UNIQ]).'">SBBK</a>
      </div>';
      if($isKhusus) {
        $data[] = array(
          $htmlBtn,
          date('Y-m-d', strtotime($r[COL_DATEDISTRIBUTION])),
          $r[COL_NMREFERENSI],
          number_format($r['NumItems']),
          $r[COL_NMREMARKS],
          $r[COL_CREATEDBY],
          date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
        );
      } else {
        $data[] = array(
          $htmlBtn,
          date('Y-m-d', strtotime($r[COL_DATEDISTRIBUTION])),
          $r[COL_NMPUSKESMAS],
          $r[COL_NMREFERENSI],
          number_format($r['NumItems']),
          $r[COL_CREATEDBY],
          date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
        );
      }
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function add($isKhusus=0) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] == ROLEPUSKESMAS) {
      ShowJsonError('Anda tidak memiliki otorisasi.');
      return;
    }
    if(!empty($_POST)) {
      $arrDet = array();
      $det = $this->input->post("DistItems");
      $data = array(
        COL_IDPUSKESMAS => $this->input->post(COL_IDPUSKESMAS),
        COL_NMREFERENSI => $this->input->post(COL_NMREFERENSI),
        COL_DATEDISTRIBUTION => $this->input->post(COL_DATEDISTRIBUTION),
        COL_NMREMARKS => $this->input->post(COL_NMREMARKS),

        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      if($isKhusus) {
        $rpuskkhusus = $this->db
        ->where(COL_KDTIPE, 2)
        ->get(TBL_MPUSKESMAS)
        ->row_array();
        if(empty($rpuskkhusus)) {
          ShowJsonError('Maaf, distribusi khusus tidak dapat dilakukan.');
          return;
        }

        $data[COL_IDPUSKESMAS]=$rpuskkhusus[COL_IDPUSKESMAS];
      }

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TSTOCKDISTRIBUTION, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $idDist = $this->db->insert_id();
        if(!empty($det)) {
          $det = json_decode(urldecode($det));
          foreach($det as $s) {
            /* CHECK STOCK */
            $qCheck = @"
            select *, Jumlah - coalesce((select sum(i.Jumlah) from tstockdistribution_items i where i.`IdReceipt` = r.`Uniq`),0) as JlhSisa
          	from tstockreceipt r
          	where
          		r.Uniq = ?
              and r.DateReceipt <= ?
            ";
            $rcheck = $this->db->query($qCheck, array($s->IdReceipt, $data[COL_DATEDISTRIBUTION]))->row_array();
            if(empty($rcheck)) {
              throw new Exception('No. BATCH tidak valid. Silakan periksa kembali.');
            }
            if($rcheck['JlhSisa'] < $s->Jumlah) {
              throw new Exception('Jumlah stok pada item BATCH No. <b>'.$rcheck[COL_NMBATCH].'</b> tidak mencukupi.<br />SISA STOK: <b>'.number_format($rcheck['JlhSisa']).'</b>');
            }
            /* CHECK STOCK */

            $arrDet[] = array(
              COL_IDDISTRIBUTION=>$idDist,
              COL_IDRECEIPT=>$s->IdReceipt,
              COL_JUMLAH=>toNum($s->Jumlah)
            );
          }
        }

        if(!empty($arrDet)) {
          if($isKhusus) {
            $arrIssue = array();
            foreach($arrDet as $d) {
              $res = $this->db->insert(TBL_TSTOCKDISTRIBUTION_ITEMS, $d);
              if(!$res) {
                $err = $this->db->error();
                throw new Exception('Error: '.$err['message']);
              }

              $idItem = $this->db->insert_id();
              $arrIssue[] = array(
                COL_IDITEM=>$idItem,
                COL_DATEISSUE=>$this->input->post(COL_DATEDISTRIBUTION),
                COL_JUMLAH=>$d[COL_JUMLAH],
                COL_NMREMARKS=>$this->input->post(COL_NMREMARKS),
                COL_CREATEDBY => $ruser[COL_USERNAME],
                COL_CREATEDON => date('Y-m-d H:i:s')
              );
            }
            if(!empty($arrIssue)) {
              $res = $this->db->insert_batch(TBL_TSTOCKISSUE, $arrIssue);
              if(!$res) {
                $err = $this->db->error();
                throw new Exception('Error: '.$err['message']);
              }
            }
          } else {
            $res = $this->db->insert_batch(TBL_TSTOCKDISTRIBUTION_ITEMS, $arrDet);
            if(!$res) {
              $err = $this->db->error();
              throw new Exception('Error: '.$err['message']);
            }
          }
        } else {
          throw new Exception('Item tidak boleh kosong.');
        }

        $this->db->trans_commit();
        ShowJsonSuccess('INPUT DATA BERHASIL');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      /*ShowJsonError('Parameter tidak valid.');
      return;*/
      $data['DistItems'] = json_encode(array());
      $data['isKhusus'] = $isKhusus;
      $this->load->view('admin/distribution/form', $data);
    }
  }

  public function view($id) {
    $isKhusus = 0;
    $rdata = $this->db
    ->select('tstockdistribution.*, pus.KdTipe, uc.Nm_FullName as Nm_CreatedBy')
    ->join(TBL__USERINFORMATION.' uc','uc.'.COL_USERNAME." = ".TBL_TSTOCKDISTRIBUTION.".".COL_CREATEDBY,"left")
    ->join(TBL_MPUSKESMAS.' pus','pus.'.COL_IDPUSKESMAS." = ".TBL_TSTOCKDISTRIBUTION.".".COL_IDPUSKESMAS,"left")
    ->where(COL_UNIQ, $id)
    ->get(TBL_TSTOCKDISTRIBUTION)
    ->row_array();
    if(empty($rdata)) {
      echo 'Data tidak ditemukan';
      return;
    }

    if($rdata[COL_KDTIPE]==2) {
      $isKhusus = 1;
    }

    $arrdet = array();
    $rdet = $this->db
    ->select('tstockdistribution_items.*, st.NmStock, st.NmSatuan, r.NmBatch')
    ->join(TBL_TSTOCKRECEIPT.' r','r.'.COL_UNIQ." = ".TBL_TSTOCKDISTRIBUTION_ITEMS.".".COL_IDRECEIPT,"left")
    ->join(TBL_MSTOCK.' st','st.'.COL_IDSTOCK." = r.".COL_IDSTOCK,"left")
    ->where(COL_IDDISTRIBUTION, $rdata[COL_UNIQ])
    ->get(TBL_TSTOCKDISTRIBUTION_ITEMS)
    ->result_array();
    foreach($rdet as $det) {
      $arrdet[] = array(
        'IdReceipt'=> $det[COL_IDRECEIPT],
        'NmBatch'=> $det[COL_NMBATCH],
        'NmStock'=> $det[COL_NMSTOCK],
        'NmSatuan'=> $det[COL_NMSATUAN],
        'Jumlah'=>$det[COL_JUMLAH]
      );
    }
    $this->load->view('admin/distribution/form', array('DistItems'=>json_encode($arrdet),'data'=>$rdata,'disabled'=>true, 'isKhusus'=>$isKhusus));
  }

  public function delete($id) {
    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] == ROLEPUSKESMAS) {
      ShowJsonError('Anda tidak memiliki otorisasi.');
      return;
    }
    $rissue = $this->db
    ->where("IdItem in (select i.Uniq from tstockdistribution_items i where i.IdDistribution = $id)", null, false)
    ->get(TBL_TSTOCKISSUE)
    ->row_array();
    if(!empty($rissue)) {
      ShowJsonError('Data tidak dapat dihapus karena sudah memiliki relasi dengan data PEMAKAIAN.');
      return;
    }

    $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_TSTOCKDISTRIBUTION);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      return;
    }

    ShowJsonSuccess('Data berhasil dihapus.');
    return;
  }

  public function cetak_konsep($uniq) {
    $rdata = $this->db
    ->join(TBL_MPUSKESMAS.' pus','pus.'.COL_IDPUSKESMAS." = ".TBL_TSTOCKDISTRIBUTION.".".COL_IDPUSKESMAS,"left")
    ->where(COL_UNIQ, $uniq)
    ->get(TBL_TSTOCKDISTRIBUTION)
    ->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid');
      return;
    }

    $rdet = $this->db
    ->select('tstockdistribution_items.*, st.NmStock, st.NmSatuan, r.NmBatch, r.DateExpired, r.NmSumber, kat.NmKategori, kat.Seq')
    ->join(TBL_TSTOCKRECEIPT.' r','r.'.COL_UNIQ." = ".TBL_TSTOCKDISTRIBUTION_ITEMS.".".COL_IDRECEIPT,"left")
    ->join(TBL_MSTOCK.' st','st.'.COL_IDSTOCK." = r.".COL_IDSTOCK,"left")
    ->join(TBL_MKATEGORI.' kat','kat.'.COL_IDKATEGORI." = st.".COL_IDKATEGORI,"left")
    ->where(COL_IDDISTRIBUTION, $rdata[COL_UNIQ])
    ->order_by('kat.'.COL_SEQ)
    ->get(TBL_TSTOCKDISTRIBUTION_ITEMS)
    ->result_array();

    $arrkat = array();
    foreach($rdet as $d) {
      if(!in_array($d[COL_NMKATEGORI], $arrkat)) $arrkat[] = $d[COL_NMKATEGORI];
    }

    $this->load->library('Mypdf');
    $mpdf = new Mypdf();

    $html = $this->load->view('distribution/_cetak_konsep', array('data'=>$rdata, 'det'=>$rdet, 'kat'=>$arrkat), TRUE);
    //echo $html;
    //return;
    $mpdf->pdf->SetTitle($this->setting_web_name.' - KONSEP LEMBAR PEMBERIAN OBAT');
    $mpdf->pdf->SetFooter('Hal. {PAGENO} dari {nbpg}');
    $mpdf->pdf->SetHeader('Dicetak melalui aplikasi <strong>'.$this->setting_web_name.'</strong> pada '.date('d-m-Y H:i'));
    $mpdf->pdf->WriteHTML($html);
    $mpdf->pdf->Output($this->setting_web_name.' - Konsep Lembar Pemberian Obat '.date('YmdHis').'.pdf', 'I');
  }

  public function cetak_ba($uniq) {
    $rdata = $this->db
    ->join(TBL_MPUSKESMAS.' pus','pus.'.COL_IDPUSKESMAS." = ".TBL_TSTOCKDISTRIBUTION.".".COL_IDPUSKESMAS,"left")
    ->where(COL_UNIQ, $uniq)
    ->get(TBL_TSTOCKDISTRIBUTION)
    ->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid');
      return;
    }

    $qdet = @"
    select
    st.NmStock,
    st.NmSatuan,
    st.IdStock,
    r.Harga,
    r.NmSumber,
    sum(dit.Jumlah) as TotalQty,
    r.DateExpired,
    r.NmBatch,
    pab.NmPabrik
    from tstockdistribution dist
    left join tstockdistribution_items dit on dit.IdDistribution = dist.Uniq
    left join tstockreceipt r on r.Uniq = dit.IdReceipt
    left join mstock st on st.IdStock = r.IdStock
    left join mpabrik pab on pab.IdPabrik = r.IdPabrik
    where
      dist.Uniq = $uniq
    group by NmStock, NmSatuan, Harga, NmSumber
    ";
    $rdet = $this->db->query($qdet)->result_array();
    $cdet_ = array();
    foreach($rdet as $dat) {
      if(!array_key_exists($dat[COL_NMSTOCK], $cdet_)) $cdet_[$dat[COL_NMSTOCK]] = 1;
      else $cdet_[$dat[COL_NMSTOCK]] += 1;
    }
    foreach($rdet as $idx => $dat) {
      $rdet[$idx]['ROWSPAN'] = $cdet_[$dat[COL_NMSTOCK]];
    }

    $this->load->library('Mypdf');
    $mpdf = new Mypdf("", "A4-L");

    $html = $this->load->view('distribution/_cetak_ba', array('data'=>$rdata, 'det'=>$rdet), TRUE);
    //echo $html;
    //return;
    $mpdf->pdf->SetTitle($this->setting_web_name.' - BERITA ACARA SERAH TERIMA');
    $mpdf->pdf->SetFooter('Hal. {PAGENO} dari {nbpg}');
    $mpdf->pdf->SetHeader('Dicetak melalui aplikasi <strong>'.$this->setting_web_name.'</strong> pada '.date('d-m-Y H:i'));
    $mpdf->pdf->WriteHTML($html);
    $mpdf->pdf->Output($this->setting_web_name.' - BA Serah Terima Obat '.date('YmdHis').'.pdf', 'I');
  }

  public function cetak_sbbk($uniq) {
    $rdata = $this->db
    ->join(TBL_MPUSKESMAS.' pus','pus.'.COL_IDPUSKESMAS." = ".TBL_TSTOCKDISTRIBUTION.".".COL_IDPUSKESMAS,"left")
    ->where(COL_UNIQ, $uniq)
    ->get(TBL_TSTOCKDISTRIBUTION)
    ->row_array();
    if(empty($rdata)) {
      show_error('Parameter tidak valid');
      return;
    }

    $qdet = @"
    select
    st.NmStock,
    st.NmSatuan,
    st.IdStock,
    r.Harga,
    r.NmSumber,
    sum(dit.Jumlah) as TotalQty,
    r.DateExpired,
    r.NmBatch,
    pab.NmPabrik
    from tstockdistribution dist
    left join tstockdistribution_items dit on dit.IdDistribution = dist.Uniq
    left join tstockreceipt r on r.Uniq = dit.IdReceipt
    left join mstock st on st.IdStock = r.IdStock
    left join mpabrik pab on pab.IdPabrik = r.IdPabrik
    where
      dist.Uniq = $uniq
    group by NmStock, NmSatuan, Harga, NmSumber
    ";
    $rdet = $this->db->query($qdet)->result_array();
    $cdet_ = array();
    foreach($rdet as $dat) {
      if(!array_key_exists($dat[COL_NMSTOCK], $cdet_)) $cdet_[$dat[COL_NMSTOCK]] = 1;
      else $cdet_[$dat[COL_NMSTOCK]] += 1;
    }
    foreach($rdet as $idx => $dat) {
      $rdet[$idx]['ROWSPAN'] = $cdet_[$dat[COL_NMSTOCK]];
    }

    $this->load->library('Mypdf');
    $mpdf = new Mypdf("", "A4-L");

    $html = $this->load->view('distribution/_cetak_sbbk', array('data'=>$rdata, 'det'=>$rdet), TRUE);
    //echo $html;
    //return;
    $mpdf->pdf->SetTitle($this->setting_web_name.' - SURAT BUKTI BARANG KELUAR');
    $mpdf->pdf->SetFooter('Hal. {PAGENO} dari {nbpg}');
    $mpdf->pdf->SetHeader('Dicetak melalui aplikasi <strong>'.$this->setting_web_name.'</strong> pada '.date('d-m-Y H:i'));
    $mpdf->pdf->WriteHTML($html);
    $mpdf->pdf->Output($this->setting_web_name.' - SBBK '.date('YmdHis').'.pdf', 'I');
  }
}
?>
