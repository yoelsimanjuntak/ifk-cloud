<?php
class Ajax extends MY_Controller {
  public function get_available_receipt() {
    $IdStock = $this->input->post('IdStock');
    $Date = $this->input->post('Tanggal');

    $q = @"
select
st.NmStock,
st.KdStock,
st.NmSatuan,
tbl.*,
tbl.Jumlah - tbl.JlhDist as JlhSisa
from (
	select *, coalesce((select sum(i.Jumlah) from tstockdistribution_items i where i.`IdReceipt` = r.`Uniq`),0) as JlhDist
	from tstockreceipt r
	where
		r.IdStock = ?
		and r.DateReceipt <= ?
) tbl
left join mstock st on st.IdStock = tbl.`IdStock`
where
	tbl.Jumlah - tbl.JlhDist > 0
order by
  tbl.DateReceipt desc
    ";
    $rreceipt = $this->db->query($q, array($IdStock, $Date))->result_array();
    $this->load->view('admin/ajax/get-available-receipt', array('res'=>$rreceipt));
  }

  public function get_stock_uom() {
    $IdStock = $this->input->post('IdStock');
    $rstock = $this->db
    ->where(COL_IDSTOCK, $IdStock)
    ->get(TBL_MSTOCK)
    ->row_array();

    if(!empty($rstock)) {
      echo $rstock[COL_NMSATUAN];
    }
  }

  public function get_stock_name_by_id() {
    $IdStock = $this->input->post('IdStock');
    $rstock = $this->db
    ->where(COL_IDSTOCK, $IdStock)
    ->get(TBL_MSTOCK)
    ->row_array();

    if(!empty($rstock)) {
      echo $rstock[COL_NMSTOCK];
    }
  }

  public function get_available_dist() {
    $IdPuskesmas = $this->input->post('IdPuskesmas');
    $Date = $this->input->post('Tanggal');

    $q = @"
select
tbl.Uniq,
pus.NmPuskesmas,
r.NmBatch,
st.NmStock,
tbl.DateDistribution,
r.DateExpired,
st.NmSatuan,
tbl.Jumlah as JlhDist,
tbl.JlhPakai,
tbl.Jumlah - tbl.JlhPakai as JlhSisa
from (
	select
  i.Uniq,
  dist.DateDistribution,
	dist.IdPuskesmas,
	i.IdReceipt,
	i.Jumlah,
	coalesce((select sum(is_.Jumlah) from tstockissue is_ where is_.IdItem = i.Uniq),0) as JlhPakai
	from tstockdistribution_items i
	inner join tstockdistribution dist on dist.Uniq = i.IdDistribution
	where
    dist.DateDistribution <= ?
    and dist.IdPuskesmas = ?
) tbl
inner join tstockreceipt r on r.Uniq = tbl.IdReceipt
left join mstock st on st.IdStock = r.IdStock
left join mpuskesmas pus on pus.IdPuskesmas = tbl.IdPuskesmas
where
	tbl.Jumlah - tbl.JlhPakai > 0
order by
  tbl.DateDistribution,
  st.NmStock,
  r.NmBatch,
  r.DateExpired
    ";
    $rdist = $this->db->query($q, array($Date, $IdPuskesmas))->result_array();
    $this->load->view('admin/ajax/get-available-dist', array('res'=>$rdist));
  }

  public function get_available_request() {
    $IdPuskesmas = $this->input->post('IdPuskesmas');

    $q = @"
select
req.*,
(select count(*) from trequest_items i_ where i_.IdRequest = req.Uniq) as Jlh
from trequest req
where
  IdPuskesmas = ?
    ";
    $rrequest = $this->db->query($q, array($IdPuskesmas))->result_array();
    $this->load->view('admin/ajax/get-available-request', array('res'=>$rrequest));
  }

  public function get_request_item() {
    $IdRequest = $this->input->post('IdRequest');

    $q = @"
select
i.*
from trequest_items i
left join mstock st on st.IdStock = i.IdStock
where
  i.IdRequest = ?
    ";
    $rrequest = $this->db->query($q, array($IdRequest))->result_array();
    echo json_encode($rrequest);
  }

  public function changepassword() {
    $data['data'] = array();
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $this->form_validation->set_rules(array(
        array(
          'field' => 'NewPassword',
          'label' => 'NewPassword',
          'rules' => 'required|min_length[5]',
          'errors' => array('min_length' => 'Password minimal terdiri dari 5 karakter.')
        ),
        array(
          'field' => 'ConfirmPassword',
          'label' => 'Repeat ConfirmPassword',
          'rules' => 'required|matches[NewPassword]',
          'errors' => array('matches' => 'Kolom Konfirmasi Password wajib sama dengan Password Baru.')
        )
      ));

      if($ruser[COL_PASSWORD] != md5($this->input->post('OldPassword'))) {
        ShowJsonError('Password Lama tidak valid.');
        return false;
      }

      if($this->form_validation->run()) {
        $res = $this->db
        ->where(COL_USERNAME, $ruser[COL_USERNAME])
        ->update(TBL__USERS, array(COL_PASSWORD=>md5($this->input->post('NewPassword'))));
        if(!$res) {
          $err = $this->db->error();
          ShowJsonError($err['message']);
          return false;
        }

        ShowJsonSuccess('Password berhasil diperbarui.');
        return false;
      } else {
        ShowJsonError(validation_errors());
        return false;
      }
    } else {
      $this->load->view('admin/ajax/changepassword', $data);
    }
  }

  public function changesetting() {
    if(!empty($_POST['SETTING_ORG_KAUPTNAMA'])) {
      SetSetting('SETTING_ORG_KAUPTNAMA', $_POST['SETTING_ORG_KAUPTNAMA']);
    }

    if(!empty($_POST['SETTING_ORG_KAUPTNIP'])) {
      SetSetting('SETTING_ORG_KAUPTNIP', $_POST['SETTING_ORG_KAUPTNIP']);
    }

    if(!empty($_POST['SETTING_ORG_KTUNAMA'])) {
      SetSetting('SETTING_ORG_KTUNAMA', $_POST['SETTING_ORG_KTUNAMA']);
    }

    if(!empty($_POST['SETTING_ORG_KTUNIP'])) {
      SetSetting('SETTING_ORG_KTUNIP', $_POST['SETTING_ORG_KTUNIP']);
    }

    ShowJsonSuccess('Pengaturan berhasil diubah.');
    return;
  }

  public function get_receipt_by_batch() {
    $NmBatch = $this->input->post('NmBatch');
    $Date = $this->input->post('Tanggal');

    $q = @"
select
st.NmStock,
st.KdStock,
st.NmSatuan,
tbl.*,
tbl.Jumlah - tbl.JlhDist as JlhSisa
from (
	select *, coalesce((select sum(i.Jumlah) from tstockdistribution_items i where i.`IdReceipt` = r.`Uniq`),0) as JlhDist
	from tstockreceipt r
	where
		r.NmBatch = ?
		and r.DateReceipt <= ?
) tbl
left join mstock st on st.IdStock = tbl.`IdStock`
where
	tbl.Jumlah - tbl.JlhDist > 0
order by
  tbl.DateExpired asc,
  (tbl.Jumlah - tbl.JlhDist) asc
    ";
    $rreceipt = $this->db->query($q, array($NmBatch, $Date))->row_array();

    echo json_encode($rreceipt);
  }

  public function get_dist_by_batch() {
    $NmBatch = $this->input->post('NmBatch');
    $IdPuskesmas = $this->input->post('IdPuskesmas');
    $Date = $this->input->post('Tanggal');

    $q = @"
select
tbl.Uniq,
pus.NmPuskesmas,
r.NmBatch,
st.NmStock,
tbl.DateDistribution,
r.DateExpired,
st.NmSatuan,
tbl.Jumlah as JlhDist,
tbl.JlhPakai,
tbl.Jumlah - tbl.JlhPakai as JlhSisa
from (
	select
  i.Uniq,
  dist.DateDistribution,
	dist.IdPuskesmas,
	i.IdReceipt,
	i.Jumlah,
	coalesce((select sum(is_.Jumlah) from tstockissue is_ where is_.IdItem = i.Uniq),0) as JlhPakai
	from tstockdistribution_items i
	inner join tstockdistribution dist on dist.Uniq = i.IdDistribution
	where
    dist.DateDistribution <= ?
    and dist.IdPuskesmas = ?
) tbl
inner join tstockreceipt r on r.Uniq = tbl.IdReceipt
left join mstock st on st.IdStock = r.IdStock
left join mpuskesmas pus on pus.IdPuskesmas = tbl.IdPuskesmas
where
	tbl.Jumlah - tbl.JlhPakai > 0
  and r.NmBatch = ?
order by
  r.DateExpired asc,
  (tbl.Jumlah - tbl.JlhPakai) asc
    ";
    $rdist = $this->db->query($q, array($Date, $IdPuskesmas, $NmBatch))->row_array();

    echo json_encode($rdist);
  }
}
