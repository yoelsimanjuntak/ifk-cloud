<form id="form-editor" method="post" action="#">
<div class="modal-header">
  <h5 class="modal-title">Form Penerimaan</h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true"><i class="fa fa-close"></i></span>
  </button>
</div>
<div class="modal-body">
  <p class="text-danger error-message"></p>
    <div class="row">
      <div class="col-sm-6 pr-3 pl-2">
        <div class="form-group">
          <label>OBAT</label>
          <select class="form-control" name="<?=COL_IDSTOCK?>" style="width: 100%" required>
            <?=GetCombobox("SELECT * FROM mstock left join mkategori on mkategori.IdKategori = mstock.IdKategori WHERE mstock.IsDeleted != 1 ORDER BY NmStock", COL_IDSTOCK, array(COL_NMKATEGORI, COL_NMSTOCK), (!empty($data)?$data[COL_IDSTOCK]:null))?>
          </select>
        </div>
        <div class="form-group row">
          <div class="col-sm-4">
            <label>TANGGAL</label>
            <input type="text" class="form-control datepicker text-right" name="<?=COL_DATERECEIPT?>" value="<?=!empty($data)?$data[COL_DATERECEIPT]:""?>" required />
          </div>
          <div class="col-sm-8">
            <label>BATCH</label>
            <input type="text" class="form-control" name="<?=COL_NMBATCH?>" value="<?=!empty($data)?$data[COL_NMBATCH]:""?>" />
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-4">
            <label>JUMLAH</label>
            <input type="text" class="form-control text-right uang" name="<?=COL_JUMLAH?>" value="<?=!empty($data)?$data[COL_JUMLAH]:""?>" required />
          </div>
          <div class="col-sm-8">
            <label>HARGA SATUAN</label>
            <input type="text" class="form-control text-right uang" name="<?=COL_HARGA?>" value="<?=!empty($data)?$data[COL_HARGA]:""?>" required />
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-4">
            <label>T.A</label>
            <input type="text" class="form-control text-right" name="<?=COL_TAHUN?>" value="<?=!empty($data)?$data[COL_TAHUN]:date('Y')?>" required />
          </div>
          <div class="col-sm-8">
            <label>SUMBER DANA</label>
            <select class="form-control" name="<?=COL_NMSUMBER?>" style="width: 100%" required>
              <option value="DAK" <?=!empty($data)&&$data[COL_NMSUMBER]=='DAK'?'selected':''?>>DAK</option>
              <option value="DAU" <?=!empty($data)&&$data[COL_NMSUMBER]=='DAU'?'selected':''?>>DAU</option>
              <option value="PROGRAM" <?=!empty($data)&&$data[COL_NMSUMBER]=='PROGRAM'?'selected':''?>>PROGRAM</option>
              <option value="BUFFER STOCK" <?=!empty($data)&&$data[COL_NMSUMBER]=='BUFFER STOCK'?'selected':''?>>BUFFER STOCK</option>
            </select>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-4">
            <label>KADALUARSA</label>
            <input type="text" class="form-control datepicker-exp text-right" name="<?=COL_DATEEXPIRED?>" value="<?=!empty($data)?$data[COL_DATEEXPIRED]:date('Y-m-d', strtotime('+5 years'))?>" required />
          </div>
        </div>
      </div>
      <div class="col-sm-6 pl-3 pr-2">
        <div class="form-group">
          <label>PABRIKAN</label>
          <select class="form-control" name="<?=COL_IDPABRIK?>" style="width: 100%" required>
            <?=GetCombobox("SELECT * FROM mpabrik WHERE IsDeleted != 1 ORDER BY NmPabrik", COL_IDPABRIK, COL_NMPABRIK, (!empty($data)?$data[COL_IDPABRIK]:null))?>
          </select>
        </div>
        <div class="form-group">
          <label>PEMASOK</label>
          <select class="form-control" name="<?=COL_IDSUPPLIER?>" style="width: 100%" required>
            <?=GetCombobox("SELECT * FROM msupplier WHERE IsDeleted != 1 ORDER BY NmSupplier", COL_IDSUPPLIER, COL_NMSUPPLIER, (!empty($data)?$data[COL_IDSUPPLIER]:null))?>
          </select>
        </div>
        <div class="form-group">
          <label>CATATAN</label>
          <textarea rows="4" class="form-control" name="<?=COL_NMREMARKS?>"><?=!empty($data)?$data[COL_NMREMARKS]:''?></textarea>
        </div>
      </div>
    </div>
    <?php
    if(!empty($data)) {
      ?>
      <p class="text-muted text-sm font-italic label-info mb-0 mt-3 pt-2">
        Diinput oleh: <b><?=$data['Nm_CreatedBy']?></b><br />
        Diinput pada: <b><?=date('Y-m-d H:i:s', strtotime($data[COL_CREATEDON]))?></b>
      </p>
      <?php
    }
    ?>
</div>
<?php
if(!isset($disabled) || empty($disabled)) {
  ?>
  <div class="modal-footer">
    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">BATAL</button>
    <button type="submit" class="btn btn-outline-primary btn-ok">SIMPAN</button>
  </div>
  <?php
}
?>

</form>
<script>
$(document).ready(function() {
  var form = $('#form-editor');
  $(".money", form).number(true, 2, '.', ',');
  $(".uang", form).number(true, 0, '.', ',');
  $("select", form).select2({ width: 'resolve', theme: 'bootstrap4' });
  $('.datepicker', form).daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: parseInt(moment().format('YYYY'),10)-2,
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
        format: 'YYYY-MM-DD'
    }
  });
  $('.datepicker-exp', form).daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    locale: {
        format: 'YYYY-MM-DD'
    }
  });
  <?php
  if(!empty($disabled)) {
    ?>
    $('input,select,textarea,button', form).not('[data-dismiss=modal]').attr('disabled', true);
    <?php
  }
  ?>
});
</script>
