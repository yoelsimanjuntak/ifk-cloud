<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 30/01/2020
 * Time: 22:01
 */
$user = GetLoggedUser();
?>
<style>
div.dataTables_info {
    padding-top: 0 !important;
}
div.dataTables_length label {
    margin-bottom: 0 !important;
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"><?= $title ?> <small class="text-sm font-weight-light"> Data</small></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-tachometer-alt"></i> Dashboard</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
          <div class="card card-default">
            <div class="card-header">
              <div class="card-tools">
                <a href="<?=site_url('admin/receipt/add')?>" type="button" class="btn btn-tool btn-add-data text-info"><i class="fas fa-plus"></i>&nbsp;TAMBAH</a>
                <button type="button" class="btn btn-tool btn-refresh-data"><i class="fas fa-sync-alt"></i>&nbsp;REFRESH</button>
              </div>
            </div>
            <div class="card-body">
              <form id="dataform" method="post" action="#">
                <table id="datalist" class="table table-bordered table-hover table-condensed">
                  <thead>
                    <tr>
                      <th class="text-center" style="width: 10px">#</th>
                      <th>TANGGAL</th>
                      <th>OBAT</th>
                      <th>BATCH</th>
                      <th>JUMLAH</th>
                      <th>SATUAN</th>
                      <th>Diinput Oleh</th>
                      <th>Diinput Pada</th>
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>
              </form>
            </div>
          </div>
          <div class="modal fade" id="modal-editor" role="dialog">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
              </div>
            </div>
          </div>
          <div class="modal fade" id="modal-barcode" role="dialog">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">BARCODE</h5>
                </div>
                <div class="modal-body">
                  <div class="text-center" id="modal-body-barcode" style="line-height:1 !important">
                    <img src="" class="mb-2" style="border: 1px solid #000; padding: 10px" alt="Loading..." /><br  />
                    <small class="font-weight-bold mt-2">...</small>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">TUTUP</button>
                  <a href="#" class="btn btn-outline-primary btn-download-barcode"><i class="far fa-download"></i>&nbsp;DOWNLOAD</a>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</section>
<div id="dom-filter" class="d-none">
  <div class="form-group row">
    <label class="col-sm-2 mb-0">OBAT</label>
    <div class="col-sm-5">
      <select class="form-control" name="filterIdStock" style="width: 100%" required>
        <?=GetCombobox("SELECT * FROM mstock WHERE IsDeleted != 1 ORDER BY NmStock", COL_IDSTOCK, COL_NMSTOCK, null, true, false, '-- SEMUA --')?>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 mb-0">PERIODE</label>
    <div class="col-sm-2">
      <input type="text" class="form-control form-control-sm datepicker text-right" name="filterDateFrom" value="<?=date('Y-m-1')?>" />
    </div>
    <label class="col-sm-1 mb-0 text-center">s.d</label>
    <div class="col-sm-2">
      <input type="text" class="form-control form-control-sm datepicker text-right" name="filterDateTo" value="<?=date('Y-m-d')?>" />
    </div>
  </div>
</div>
<div class="text-center d-none" id="divtemp-barcode" style="line-height: 1 !important; padding: 5px; display: inline-block;">
  <img src="" class="mb-2" style="width: 100%; border: 1px solid #000; padding: 10px" alt="Loading..." /><br  />
  <small class="font-weight-bold mt-2">...</small>
</div>
<script type="text/javascript">
$(document).ready(function() {
  var modalData = $('#modal-editor');
  var modalBarcode = $('#modal-barcode');

  var dt = $('#datalist').dataTable({
    "autoWidth" : false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?=site_url('admin/receipt/index-load')?>",
      "type": 'POST',
      "data": function(data){
          var dateFrom = $('[name=filterDateFrom]', $('.datefilter')).val();
          var dateTo = $('[name=filterDateTo]', $('.datefilter')).val();
          var idStock = $('[name=filterIdStock]', $('.datefilter')).val();

          data.dateFrom = dateFrom;
          data.dateTo = dateTo;
          data.idStock = idStock;
       }
    },
    "scrollY" : '40vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    //"aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    //"dom":"R<'row'<'col-sm-8'l><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    "dom":"R<'row'<'col-sm-8 datefilter'><'col-sm-4 text-right'f<'clear'>B>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
    "order": [[ 1, "desc" ]],
    "columnDefs": [
      {"targets":[0], "className":'nowrap text-center'},
      {"targets":[1,4,7], "className":'nowrap dt-body-right'}
    ],
    "columns": [
      {"orderable": false},
      {"orderable": true,"width": "50px"},
      {"orderable": true},
      {"orderable": true},
      {"orderable": false},
      {"orderable": false},
      {"orderable": false},
      {"orderable": true}
    ],
    "createdRow": function(row, data, dataIndex) {
      $('.btn-action', $(row)).click(function() {
        var url = $(this).attr('href');
        if(confirm('Apakah anda yakin?')) {
          $.get(url, function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success(res.success);
            }
          }, "json").done(function() {
            dt.DataTable().ajax.reload();
          }).fail(function() {
            toastr.error('SERVER ERROR');
          });
        }
        return false;
      });
      $('.btn-view', $(row)).click(function() {
        var href = $(this).attr('href');
        $('.modal-content', modalData).load(href, function() {
          modalData.modal('show');
        });
        return false;
      });
      $('.btn-barcode', $(row)).click(function() {
        $('img', modalBarcode).attr('src', '');
        $('img', modalBarcode).attr('alt', 'Loading...');
        $('small', modalBarcode).html('');
        $('img', $('#divtemp-barcode')).attr('src', '');
        $('small', $('#divtemp-barcode')).attr('alt', 'Loading...');
        $('small', $('#divtemp-barcode')).text('');
        $('.btn-download-barcode').addClass("d-none");

        var src = $(this).data('url');
        var nmstock = $(this).data('nmstock');
        var expdate = $(this).data('expdate');

        $('img', modalBarcode).attr('src', src);
        $('img', modalBarcode).attr('alt', src);
        $('small', modalBarcode).html(nmstock+'<br />EXP. '+expdate);

        $('img', $('#divtemp-barcode')).on('load', function(){
          html2canvas(document.querySelector("#divtemp-barcode"), {scrollY: -window.scrollY, useCORS: true}).then(function(canvas) {
            var img = canvas.toDataURL("image/png");

            $('#divtemp-barcode').addClass('d-none');
            /*$('.btn-download-barcode').removeClass("d-none").unbind('click').click(function() {
              var tabBarcode = window.open();
              tabBarcode.document.body.innerHTML = '<div style="text-align: center"><img src="'+img+'" /></div>';
              return false;
            });*/
            $('.btn-download-barcode').removeClass("d-none").attr('download', 'download.png').attr('href', img);
          });
        });
        $('img', $('#divtemp-barcode')).attr('src', src);
        $('small', $('#divtemp-barcode')).attr('alt', src);
        $('small', $('#divtemp-barcode')).html(nmstock+'<br />EXP. '+expdate);

        $('#divtemp-barcode').removeClass('d-none');
        modalBarcode.modal('show');
        return false;
      });
    }
  });
  $("div.datefilter").html($('#dom-filter').html());

  $('.btn-refresh-data').click(function() {
    dt.DataTable().ajax.reload();
  });
  $('input,select', $("div.datefilter")).change(function() {
    dt.DataTable().ajax.reload();
  });

  $('.btn-add-data').click(function() {
    var href = $(this).attr('href');
    $('.modal-content', modalData).load(href, function() {
      modalData.modal('show');

      $('form', modalData).validate({
        submitHandler: function(form) {
          var btnSubmit = $('button[type=submit]', $(form));
          var txtSubmit = btnSubmit[0].innerHTML;
          btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
          $(form).ajaxSubmit({
            url: href,
            dataType: 'json',
            type : 'post',
            success: function(res) {
              if(res.error != 0) {
                toastr.error(res.error);
              } else {
                toastr.success(res.success);
                dt.DataTable().ajax.reload();
                modalData.modal('hide');
              }
            },
            error: function() {
              toastr.error('SERVER ERROR');
            },
            complete: function() {
              btnSubmit.html(txtSubmit);
            }
          });
          return false;
        }
      });
    });
    return false;
  });
});
</script>
