<style>
#tbl-items td, #tbl-items th {
  padding: .5rem !important;
  font-size: 11pt !important;
  vertical-align: middle !important;
}
#tbl-items .select2-container {
  margin-right: 0 !important;
}
</style>
<?php
$user = GetLoggedUser();
?>
<form id="form-editor" method="post" action="#">
<div class="modal-header">
  <h5 class="modal-title">Form Permintaan Obat</h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true"><i class="fa fa-close"></i></span>
  </button>
</div>
<div class="modal-body">
  <p class="text-danger error-message"></p>
    <div class="row">
      <div class="col-sm-2">
        <div class="form-group">
          <label>TANGGAL</label>
          <input type="text" class="form-control datepicker text-right" name="<?=COL_DATEREQUEST?>" value="<?=!empty($data)?$data[COL_DATEREQUEST]:""?>" required />
        </div>
      </div>
      <div class="col-sm-5">
        <div class="form-group">
          <label>PUSKESMAS</label>
          <?php
          if($user[COL_ROLEID] != ROLEPUSKESMAS) {
            ?>
            <select class="form-control" name="<?=COL_IDPUSKESMAS?>" style="width: 100%" required>
              <?=GetCombobox("SELECT * FROM mpuskesmas WHERE IsDeleted != 1 and KdTipe = 1 ORDER BY NmPuskesmas", COL_IDPUSKESMAS, COL_NMPUSKESMAS, (!empty($data)?$data[COL_IDPUSKESMAS]:null))?>
            </select>
            <?php
          } else {
            $rpuskesmas = $this->db
            ->where(COL_IDPUSKESMAS, (!empty($data)?$data[COL_IDPUSKESMAS]:$user[COL_IDUNIT]))
            ->get(TBL_MPUSKESMAS)
            ->row_array();
            ?>
            <input type="text" class="form-control" value="<?=!empty($rpuskesmas)?$rpuskesmas[COL_NMPUSKESMAS]:'--'?>" disabled />
            <input type="hidden" name="<?=COL_IDPUSKESMAS?>" value="<?=$user[COL_IDUNIT]?>" />
            <?php
          }
          ?>
        </div>
      </div>
      <div class="col-sm-5">
        <div class="form-group">
          <label>NO. REFERENSI</label>
          <input type="text" class="form-control" name="<?=COL_NMREQUEST?>" value="<?=!empty($data)?$data[COL_NMREQUEST]:""?>" />
        </div>
      </div>
      <div class="col-sm-12">
        <div class="form-group">
          <div class="form-group">
            <label>CATATAN</label>
            <textarea rows="2" class="form-control" name="<?=COL_NMREMARKS?>"><?=!empty($data)?$data[COL_NMREMARKS]:''?></textarea>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="form-group mb-0">
          <label>ITEM :</label>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          <input type="hidden" name="ReqItems" />
          <div class="row">
            <div class="col-sm-12">
              <table id="tbl-items" class="table table-bordered">
                <thead class="bg-info">
                  <tr>
                    <th>Nama Obat</th>
                    <th>Jumlah</th>
                    <th>Satuan</th>
                    <th class="text-center">#</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
                <?php
                if(!isset($disabled) || empty($disabled)) {
                  ?>
                  <tfoot>
                    <tr>
                      <th style="white-space: nowrap; min-width: 250px">
                        <select name="<?=COL_IDSTOCK?>" class="form-control form-control-sm" style="width: 100%;">
                          <?=GetCombobox("SELECT * from mstock where IsDeleted != 1 order by NmStock", COL_IDSTOCK, COL_NMSTOCK, null, true, false, '-- Pilih Obat --')?>
                        </select>
                      </th>
                      <th style="white-space: nowrap; max-width: 100px">
                        <input type="text" class="form-control form-control-sm uang text-right" name="<?=COL_JUMLAH?>" />
                      </th>
                      <th style="white-space: nowrap;">
                        <input type="hidden" name="<?=COL_NMSTOCK?>" />
                        <input type="text" class="form-control form-control-sm" name="<?=COL_NMSATUAN?>" readonly />
                      </th>
                      <th style="white-space: nowrap;" class="text-center">
                        <button type="button" id="btn-add-item" class="btn btn-sm btn-default"><i class="fa fa-plus"></i></button>
                      </th>
                    </tr>
                  </tfoot>
                  <?php
                }
                ?>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php
    if(!empty($data)) {
      ?>
      <p class="text-muted text-sm font-italic label-info mb-0 mt-3 pt-2">
        Diinput oleh: <b><?=$data['Nm_CreatedBy']?></b><br />
        Diinput pada: <b><?=date('Y-m-d H:i:s', strtotime($data[COL_CREATEDON]))?></b>
      </p>
      <?php
    }
    ?>

</div>
<?php
if(!isset($disabled) || empty($disabled)) {
  ?>
  <div class="modal-footer">
    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">BATAL</button>
    <button type="submit" class="btn btn-outline-primary btn-ok">SIMPAN</button>
  </div>
  <?php
}
?>
</form>
<script>
$(document).ready(function() {
  var form = $('#form-editor');
  $(".money", form).number(true, 2, '.', ',');
  $(".uang", form).number(true, 0, '.', ',');
  $("select", form).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
  $('.datepicker', form).daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: parseInt(moment().format('YYYY'),10)-2,
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
        format: 'YYYY-MM-DD'
    }
  });

  $('#modal-browse-item').on('show.bs.modal', function () {
    form.closest('.modal').modal('hide');
  });
  $('#modal-browse-item').on('hide.bs.modal', function () {
    form.closest('.modal').modal('show');
  });

  $('[name=ReqItems]', form).change(function() {
    writeItem('tbl-items', 'ReqItems');
  }).val(encodeURIComponent('<?=$ReqItems?>')).trigger('change');

  $('[name=IdStock]', form).change(function() {
    var val = $(this).val();
    $.ajax({
      url: "<?=site_url('admin/ajax/get-stock-uom')?>",
      method: "POST",
      data: {IdStock: val}
    }).done(function(data) {
      $('[name=NmSatuan]', form).val(data);
    });
    $.ajax({
      url: "<?=site_url('admin/ajax/get-stock-name-by-id')?>",
      method: "POST",
      data: {IdStock: val}
    }).done(function(data) {
      $('[name=NmStock]', form).val(data);
    });
  });

  $('#btn-add-item', $('#tbl-items')).click(function() {
    var dis = $(this);
    var arr = $('[name=ReqItems]').val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var row = dis.closest('tr');
    var IdStock = $('[name=IdStock]', row).val();
    var NmStock = $('[name=NmStock]', row).val();
    var Jumlah = $('[name=Jumlah]', row).val();
    var NmSatuan = $('[name=NmSatuan]', row).val();
    if(IdStock && Jumlah) {
      var exist = jQuery.grep(arr, function(a) {
        return a.IdStock == IdStock;
      });
      if(exist.length == 0) {
        arr.push({'IdStock': IdStock, 'NmStock':NmStock, 'Jumlah':Jumlah, 'NmSatuan':NmSatuan});
        $('[name=ReqItems]').val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
        $('input', row).val('');
        $('select', row).val('').trigger('change');
      } else {
        alert('Item yang sama tidak bisa diinput berulang.');
        return false;
      }
    } else {
      alert('Harap isi kolom isian dengan benar.');
      return false;
    }
  });
  <?php
  if(!empty($disabled)) {
    ?>
    $('input,select,textarea,button', form).not('[data-dismiss=modal]').attr('disabled', true);
    <?php
  }
  ?>
});

function writeItem(tbl, input) {
  var tbl = $('#'+tbl+'>tbody');
  var arr = $('[name='+input+']').val();
  if(arr) {
    arr = JSON.parse(decodeURIComponent(arr));
    if(arr.length > 0) {
      var html = '';
      for (var i=0; i<arr.length; i++) {
        html += '<tr>';
        html += '<td>'+arr[i].NmStock+'</td>';
        html += '<td class="text-right">'+$.number(arr[i].Jumlah, 0)+'</td>';
        html += '<td>'+arr[i].NmSatuan+'</td>';
        <?php
        if(!isset($disabled) || empty($disabled)) {
          ?>
          html += '<td class="text-center"><button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="'+arr[i].IdStock+'" /></td>';
          <?php
        } else {
          ?>
          html += '<td class="text-center">-</td>';
          <?php
        }
        ?>
        html += '</tr>';
      }
      tbl.html(html);

      $('.btn-del', tbl).click(function() {
        var row = $(this).closest('tr');
        var idx = $('[name=idx]', row).val();
        if(idx) {
          var arr = $('[name='+input+']').val();
          arr = JSON.parse(decodeURIComponent(arr));

          var arrNew = $.grep(arr, function(e){ return e.IdStock != idx; });
          $('[name='+input+']').val(encodeURIComponent(JSON.stringify(arrNew))).trigger('change');
        }
      });
    } else {
      tbl.html('<tr><td colspan="4"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
    }
  } else {
    tbl.html('<tr><td colspan="4"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
  }
}
</script>
