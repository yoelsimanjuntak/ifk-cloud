<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 font-weight-light"><?= $title ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-tachometer-alt"></i> DASHBOARD</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
          <div class="card card-default">
            <div class="card-header">
              <?=form_open(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal'))?>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group row mb-0">
                    <label class="control-label col-sm-4">PUSKESMAS</label>
                    <div class="col-sm-8">
                      <select class="form-control" name="<?=COL_IDPUSKESMAS?>" style="width: 100%" required>
                        <?=GetCombobox("SELECT * FROM mpuskesmas WHERE IsDeleted != 1 ORDER BY NmPuskesmas", COL_IDPUSKESMAS, COL_NMPUSKESMAS)?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group row mb-0">
                    <label class="control-label col-sm-3">TANGGAL</label>
                    <div class="col-sm-4">
                      <input type="text" class="form-control datepicker text-right" name="<?=COL_DATEDISTRIBUTION?>" value="2020-08-31" required />
                    </div>
                    <div class="col-sm-5">
                      <button type="submit" class="btn btn-outline-primary"><i class="fa fa-arrow-circle-right"></i> SIMPAN STOK AWAL</button>
                    </div>
                  </div>
                </div>
              </div>
              <input type="hidden" name="DistItems" />
              <?=form_close()?>
            </div>
            <div class="card-body p-0">
              <div class="table-responsive">
                <table id="tbl-items" class="table table-bordered">
                  <thead class="bg-success">
                    <tr>
                      <th>OBAT</th>
                      <th>PABRIK</th>
                      <th>SUPPLIER</th>
                      <th>BATCH</th>
                      <th>SUMBER</th>
                      <th>HARGA</th>
                      <th>JUMLAH</th>
                      <th>T.A</th>
                      <th>TGL. EXP</th>
                      <th class="text-center">#</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                  <tfoot>
                    <tr>
                      <th style="white-space: nowrap; min-width: 250px">
                        <select name="<?=COL_IDSTOCK?>" class="form-control" style="width: 100%;" required>
                          <?=GetCombobox("SELECT * from mstock where IsDeleted != 1 order by NmStock", COL_IDSTOCK, COL_NMSTOCK, null, true, false, '-- OBAT --')?>
                        </select>
                      </th>
                      <th style="white-space: nowrap; min-width: 250px">
                        <select class="form-control" name="<?=COL_IDPABRIK?>" style="width: 100%" required>
                          <?=GetCombobox("SELECT * FROM mpabrik WHERE IsDeleted != 1 ORDER BY NmPabrik", COL_IDPABRIK, COL_NMPABRIK, null, true, false, '-- PABRIK --')?>
                        </select>
                      </th>
                      <th style="white-space: nowrap; min-width: 250px">
                        <select class="form-control" name="<?=COL_IDSUPPLIER?>" style="width: 100%" required>
                          <?=GetCombobox("SELECT * FROM msupplier WHERE IsDeleted != 1 ORDER BY NmSupplier", COL_IDSUPPLIER, COL_NMSUPPLIER, null, true, false, '-- SUPPLIER --')?>
                        </select>
                      </th>
                      <th style="white-space: nowrap; min-width: 150px">
                        <input type="text" class="form-control" name="<?=COL_NMBATCH?>" required />
                      </th>
                      <th style="white-space: nowrap; min-width: 150px">
                        <select class="form-control" name="<?=COL_NMSUMBER?>" style="width: 100%" required>
                          <option value="DAK">DAK</option>
                          <option value="DAU">DAU</option>
                          <option value="PROGRAM">PROGRAM</option>
                          <option value="BUFFER STOCK">BUFFER STOCK</option>
                        </select>
                      </th>
                      <th style="white-space: nowrap; min-width: 150px">
                        <input type="text" class="form-control uang text-right" name="<?=COL_HARGA?>" required />
                      </th>
                      <th style="white-space: nowrap; min-width: 150px">
                        <input type="text" class="form-control uang text-right" name="<?=COL_JUMLAH?>" required />
                      </th>
                      <th style="white-space: nowrap; min-width: 100px">
                        <input type="text" class="form-control text-right" name="<?=COL_TAHUN?>" required />
                      </th>
                      <th style="white-space: nowrap; min-width: 150px">
                        <input type="text" class="form-control text-right" name="<?=COL_DATEEXPIRED?>" required />
                      </th>
                      <th style="white-space: nowrap;" class="text-center">
                        <button type="button" id="btn-add-item" class="btn btn-success"><i class="fa fa-plus"></i></button>
                      </th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
var form = $('#main-form');
function writeItem(tbl, input) {
  var tbl = $('#'+tbl+'>tbody');
  var arr = $('[name='+input+']').val();
  if(arr) {
    arr = JSON.parse(decodeURIComponent(arr));
    if(arr.length > 0) {
      var html = '';
      for (var i=0; i<arr.length; i++) {
        html += '<tr>';
        html += '<td>'+arr[i].NmStock+'</td>';
        html += '<td>'+arr[i].NmPabrik+'</td>';
        html += '<td>'+arr[i].NmSupplier+'</td>';
        html += '<td>'+arr[i].NmBatch+'</td>';
        html += '<td>'+arr[i].NmSumber+'</td>';
        html += '<td class="text-right">'+$.number(arr[i].Harga, 0)+'</td>';
        html += '<td class="text-right">'+$.number(arr[i].Jumlah, 0)+'</td>';
        html += '<td class="text-right">'+arr[i].Tahun+'</td>';
        html += '<td>'+arr[i].DateExpired+'</td>';
        <?php
        if(!isset($disabled) || empty($disabled)) {
          ?>
          html += '<td class="text-center"><button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="'+arr[i].Idx+'" /></td>';
          <?php
        } else {
          ?>
          html += '<td class="text-center">-</td>';
          <?php
        }
        ?>
        html += '</tr>';
      }
      tbl.html(html);

      $('.btn-del', tbl).click(function() {
        var row = $(this).closest('tr');
        var idx = $('[name=idx]', row).val();
        if(idx) {
          var arr = $('[name='+input+']').val();
          arr = JSON.parse(decodeURIComponent(arr));

          var arrNew = $.grep(arr, function(e){ return e.Idx != idx; });
          $('[name='+input+']').val(encodeURIComponent(JSON.stringify(arrNew))).trigger('change');
        }
      });
    } else {
      tbl.html('<tr><td colspan="9"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
    }
  } else {
    tbl.html('<tr><td colspan="9"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
  }
}

$(document).ready(function() {
  $('[name=DateExpired]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: parseInt(moment().format('YYYY'),10),
    locale: {
      format: 'YYYY-MM-DD'
    }
  });

  $('[name=DistItems]', form).change(function() {
    writeItem('tbl-items', 'DistItems');
  }).trigger('change');

  $('#btn-add-item', $('#tbl-items')).click(function() {
    var dis = $(this);
    var arr = $('[name=DistItems]').val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var row = dis.closest('tr');
    var IdStock = $('[name=IdStock]', row).val();
    var NmStock = $('[name=IdStock] option:selected', row).text();
    var IdPabrik = $('[name=IdPabrik]', row).val();
    var NmPabrik = $('[name=IdPabrik] option:selected', row).text();
    var IdSupplier = $('[name=IdSupplier]', row).val();
    var NmSupplier = $('[name=IdSupplier] option:selected', row).text();
    var NmBatch = $('[name=NmBatch]', row).val();
    var NmSumber = $('[name=NmSumber]', row).val();
    var Harga = $('[name=Harga]', row).val();
    var Jumlah = $('[name=Jumlah]', row).val();
    var Tahun = $('[name=Tahun]', row).val();
    var DateExpired = $('[name=DateExpired]', row).val();

    $('input,textarea,select', $('#tbl-items')).each(function(i, requiredField) {
      if($(requiredField).val()=='') {
        //alert($(requiredField).attr('name'));
        toastr.error('HARAP ISI DATA DENGAN LENGKAP.');
        return false;
      }
    });

    arr.push({
      'Idx': moment().format('YYYYMMDDHHmmss'),
      'IdStock': IdStock,
      'NmStock': NmStock,
      'IdPabrik':IdPabrik,
      'NmPabrik': NmPabrik,
      'IdSupplier':IdSupplier,
      'NmSupplier': NmSupplier,
      'NmBatch':NmBatch,
      'NmSumber':NmSumber,
      'Harga':Harga,
      'Jumlah':Jumlah,
      'Tahun':Tahun,
      'DateExpired':DateExpired
    });
    $('[name=DistItems]').val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
    $('input', row).val('');
    $('select', row).val('').trigger('change');
  });

  form.validate({
    submitHandler: function(form_) {
      var btnSubmit = $('button[type=submit]', $(form_));
      var txtSubmit = btnSubmit[0].innerHTML;
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i> MEMPROSES...');
      $(form_).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            location.reload();
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
        }
      });
      return false;
    }
  });
});
</script>
