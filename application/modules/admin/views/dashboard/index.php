<?php
$dateFrom = date('Y-m-01');
$dateTo = date('Y-m-t', strtotime($dateFrom));

$ruser = GetLoggedUser();
$rpuskesmas = $this->db
->where(COL_ISDELETED, 0)
->get(TBL_MPUSKESMAS)
->result_array();

$this->db
->where(TBL_TSTOCKRECEIPT.'.'.COL_DATERECEIPT.' >= ', $dateFrom)
->where(TBL_TSTOCKRECEIPT.'.'.COL_DATERECEIPT.' <= ', $dateTo);
$rreceipt = $this->db->get(TBL_TSTOCKRECEIPT)->result_array();

$this->db
->where(TBL_TSTOCKDISTRIBUTION.'.'.COL_DATEDISTRIBUTION.' >= ', $dateFrom)
->where(TBL_TSTOCKDISTRIBUTION.'.'.COL_DATEDISTRIBUTION.' <= ', $dateTo);
if($ruser[COL_ROLEID] == ROLEPUSKESMAS) {
  $this->db->where(TBL_TSTOCKDISTRIBUTION.'.'.COL_IDPUSKESMAS, $ruser[COL_IDUNIT]);
}
$rdist = $this->db->get(TBL_TSTOCKDISTRIBUTION)->result_array();


$this->db
->join(TBL_TSTOCKDISTRIBUTION_ITEMS.' i','i.'.COL_UNIQ." = ".TBL_TSTOCKISSUE.".".COL_IDITEM,"left")
->join(TBL_TSTOCKDISTRIBUTION.' dist','dist.'.COL_UNIQ." = i.".COL_IDDISTRIBUTION,"left")
->where(TBL_TSTOCKISSUE.'.'.COL_DATEISSUE.' >= ', $dateFrom)
->where(TBL_TSTOCKISSUE.'.'.COL_DATEISSUE.' <= ', $dateTo);
if($ruser[COL_ROLEID] == ROLEPUSKESMAS) {
  $this->db->where('dist.'.COL_IDPUSKESMAS, $ruser[COL_IDUNIT]);
}
$rissue = $this->db->get(TBL_TSTOCKISSUE)->result_array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?= $title ?></h3>
      </div><!-- /.col -->
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-3 col-6">
        <div class="small-box bg-info">
          <div class="inner">
            <h3><?=number_format(count($rpuskesmas))?></h3>

            <p>Puskesmas<br /><small class="font-weight-bold">Kota Tebing Tinggi</small></p>
          </div>
          <div class="icon">
            <i class="fa fa-hospital"></i>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-6">
        <div class="small-box bg-success">
          <div class="inner">
            <h3><?=number_format(count($rreceipt))?></h3>

            <p>Penerimaan Obat<br /><small class="font-weight-bold"><?=date('M Y')?></small></p>
          </div>
          <div class="icon">
            <i class="fa fa-inbox-in"></i>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-6">
        <div class="small-box bg-primary">
          <div class="inner">
            <h3><?=number_format(count($rdist))?></h3>

            <p>Distribusi Obat<br /><small class="font-weight-bold"><?=date('M Y')?></small></p>
          </div>
          <div class="icon">
            <i class="fa fa-inbox-out"></i>
          </div>
        </div>
      </div>
      <div class="col-lg-3 col-6">
        <div class="small-box bg-purple">
          <div class="inner">
            <h3><?=number_format(count($rissue))?></h3>

            <p>Pemakaian Obat<br /><small class="font-weight-bold"><?=date('M Y')?></small></p>
          </div>
          <div class="icon">
            <i class="fa fa-notes-medical"></i>
          </div>
        </div>
      </div>
      <?php
      if($ruser[COL_ROLEID]==ROLEADMIN) {
        ?>
        <div class="col-lg-6 col-6">
          <div class="card card-outline card-secondary">
            <div class="card-header">
              <h5 class="card-title">PENGATURAN IFK</h5>
            </div>
            <div class="card-body">
              <form action="<?=site_url('admin/ajax/changesetting')?>" method="POST" id="form-setting" class="form-horizontal">
                <h6>KEPALA UPT</h6>
                <div class="form-group row">
                  <label class="col-sm-2 control-label">Nama</label>
                  <div class="col-sm-8">
                    <input type="text" name="SETTING_ORG_KAUPTNAMA" value="<?=GetSetting('SETTING_ORG_KAUPTNAMA')?>" class="form-control" />
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 control-label">NIP</label>
                  <div class="col-sm-8">
                    <input type="text" name="SETTING_ORG_KAUPTNIP" value="<?=GetSetting('SETTING_ORG_KAUPTNIP')?>" class="form-control" />
                  </div>
                </div>

                <h6>KEPALA SUBBAG TATA USAHA</h6>
                <div class="form-group row">
                  <label class="col-sm-2 control-label">Nama</label>
                  <div class="col-sm-8">
                    <input type="text" name="SETTING_ORG_KTUNAMA" value="<?=GetSetting('SETTING_ORG_KTUNAMA')?>" class="form-control" />
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-2 control-label">NIP</label>
                  <div class="col-sm-8">
                    <input type="text" name="SETTING_ORG_KTUNIP" value="<?=GetSetting('SETTING_ORG_KTUNIP')?>" class="form-control" />
                  </div>
                </div>
                <div class="form-group row mb-0 mt-2 pt-3" style="margin: 0 -20px; border-top: 1px solid #dedede">
                  <div class="col-sm-12 text-center">
                    <button type="submit" class="btn btn-outline-info"><i class="far fa-check"></i>&nbsp;SIMPAN PERUBAHAN</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php
      }
      ?>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-data" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title font-weight-light">Form</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">

      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
  var modalData = $('#modal-data');

  var dt = $('#list-data').dataTable({
    "autoWidth" : false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?=site_url('admin/dashboard/data-load')?>",
      "type": 'POST',
    },
    "scrollY" : '30vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    //"aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    //"dom":"R<'row'<'col-sm-8'l><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    "dom":"R<'row'<'col-sm-6'l><'col-sm-6'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
    "order": [],
    "columnDefs": [{"targets":[0], "className":'text-center'}, {"targets":[1], "className":'dt-body-right'}],
    "columns": [
      {"orderable": false,"width": "10px"},
      {"orderable": true,"width": "100px"},
      {"orderable": true},
      {"orderable": false},
      {"orderable": true},
      {"orderable": false,"width": "200px"}
    ],
    "createdRow": function(row, data, dataIndex) {
      $('.btn-action', $(row)).click(function() {
        var url = $(this).attr('href');
        if(confirm('Apakah anda yakin?')) {
          $.get(url, function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success('Berhasil');
            }
          }, "json").done(function() {
            dt.DataTable().ajax.reload();
          }).fail(function() {
            toastr.error('SERVER ERROR');
          });
        }
        return false;
      });
      $('.btn-popup-action', $(row)).click(function() {
        var url = $(this).attr('href');
        $('.modal-body', modalData).load(url, function() {
          modalData.modal('show');
          $('form', modalData).validate({
            submitHandler: function(form) {
              var btnSubmit = $('button[type=submit]', $(form));
              var txtSubmit = btnSubmit[0].innerHTML;
              btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
              $(form).ajaxSubmit({
                dataType: 'json',
                type : 'post',
                success: function(res) {
                  if(res.error != 0) {
                    toastr.error(res.error);
                  } else {
                    toastr.success('Berhasil');
                    dt.DataTable().ajax.reload();
                    modalData.modal('hide');
                  }
                },
                error: function() {
                  toastr.error('SERVER ERROR');
                },
                complete: function() {
                  btnSubmit.html(txtSubmit);
                }
              });
              return false;
            }
          });
        });
        return false;
      });
    }
  });

  $('.modal').on('hidden.bs.modal', function (event) {
      $('form', $(this)).val('').trigger('change');
  });

  $('.btn-refresh-data').click(function() {
    dt.DataTable().ajax.reload();
  });

  $('.btn-add-data').click(function() {
    var href = $(this).attr('href');
    $('.modal-body', modalData).load(href, function() {
      modalData.modal('show');
      $('.datepicker', modalData).daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 1990,
        maxYear: parseInt(moment().format('YYYY'),10),
        locale: {
          format: 'Y-MM-DD'
        }
      });
      $("select.tag-input", modalData).select2({ width: 'resolve', theme: 'bootstrap4' });

      $('form', modalData).validate({
        submitHandler: function(form) {
          var btnSubmit = $('button[type=submit]', $(form));
          var txtSubmit = btnSubmit[0].innerHTML;
          btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
          $(form).ajaxSubmit({
            dataType: 'json',
            type : 'post',
            success: function(res) {
              if(res.error != 0) {
                toastr.error(res.error);
              } else {
                toastr.success(res.success);
                dt.DataTable().ajax.reload();
                modalData.modal('hide');
              }
            },
            error: function() {
              toastr.error('SERVER ERROR');
            },
            complete: function() {
              btnSubmit.html(txtSubmit);
            }
          });
          return false;
        }
      });
    });
    return false;
  });

  $('#form-setting').validate({
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', $(form));
      var txtSubmit = btnSubmit[0].innerHTML;
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            setTimeout(function(){
              location.reload();
            }, 1000);
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
        }
      });
      return false;
    }
  });
});
</script>
