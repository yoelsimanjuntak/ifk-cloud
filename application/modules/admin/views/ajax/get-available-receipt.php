<style>
#res-available-receipt td, #res-available-receipt th {
  padding: .5rem !important;
  font-size: 11pt !important;
  vertical-align: middle !important;
}
</style>
<?php
$i = 0;
foreach ($res as $r) {
  $res[$i] = array(
    $r[COL_DATERECEIPT],
    $r[COL_NMBATCH],
    '<strong>'.number_format($r['JlhSisa']).'</strong>'.' '.$r[COL_NMSATUAN],
    $r[COL_DATEEXPIRED],
    $r[COL_NMSUMBER],
    '<button type="button" class="btn btn-xs btn-info btn-select-receipt" data-idreceipt="'.$r[COL_UNIQ].'" data-batchno="'.$r[COL_NMBATCH].'" data-nmstock="'.$r[COL_NMSTOCK].'">
      <i class="far fa-mouse-pointer"></i>&nbsp;PILIH
    </button>'
  );
  $i++;
}
$data = json_encode($res);
?>
<table id="res-available-receipt" class="table table-striped table-bordered table-condensed" width="100%">
  <!--<thead class="bg-secondary">
    <tr>
      <th>TGL. DITERIMA</th>
      <th>BATCH</th>
      <th>JUMLAH TERSEDIA</th>
      <th>TGL. KADALUARSA</th>
      <th>SUMBER DANA</th>
      <th class="text-center">#</th>
    </tr>
  </thead>-->
  <tbody>
    <!--<?php
    if(!empty($res) && count($res) > 0) {
      foreach($res as $r) {
        ?>
        <tr>
          <td class="text-right"><?=$r[COL_DATERECEIPT]?></td>
          <td><?=$r[COL_NMBATCH]?></td>
          <td class="text-right"><?='<strong>'.number_format($r['JlhSisa']).'</strong>'.' '.$r[COL_NMSATUAN]?></td>
          <td class="text-right"><?=$r[COL_DATEEXPIRED]?></td>
          <td><?=$r[COL_NMSUMBER]?></td>
          <td class="nowrap text-center">
            <button type="button" class="btn btn-xs btn-info btn-select-receipt" data-idreceipt="<?=$r[COL_UNIQ]?>" data-batchno="<?=$r[COL_NMBATCH]?>" data-nmstock="<?=$r[COL_NMSTOCK]?>">
              <i class="far fa-mouse-pointer"></i>&nbsp;PILIH
            </button>
          </td>
        </tr>
        <?php
      }
    } else {
      ?>
      <tr>
        <td colspan="6">
          <small class="font-italic">Tidak ada data tersedia.</small>
        </td>
      </tr>
      <?php
    }
    ?>-->
  </tbody>
</table>
<script type="text/javascript">
$(document).ready(function() {
  var dtDist = $('#res-available-receipt').dataTable({
    "autoWidth" : false,
    "aaData": <?=$data?>,
    "iDisplayLength": 10,
    "sDom": "Rfrtp",
    "ordering": false,
    "columnDefs": [
      {"targets":[0,2,3], "className":'dt-body-right nowrap'},
      {"targets":[5], "className":'text-center'}
    ],
    "aoColumns": [
      {"sTitle": "TGL. DITERIMA","bSortable":false},
      {"sTitle": "BATCH"},
      {"sTitle": "JLH."},
      {"sTitle": "TGL. EXPIRED"},
      {"sTitle": "SUMBER DANA"},
      {"sTitle": "#"},
    ],
    "createdRow": function(row, data, dataIndex) {
      $('.btn-select-receipt', row).click(function() {
        var form_ = $('#form-editor');
        var idreceipt = $(this).data('idreceipt');
        var batchno = $(this).data('batchno');
        var nmstock = $(this).data('nmstock');

        $('#modal-browse-item').modal('hide');
        $('[name=IdReceipt]', form_).val(idreceipt);
        $('[name=NmBatch]', form_).val(batchno);
        $('[name=NmStock]', form_).val(nmstock);
      });
    }
  });
});
</script>
