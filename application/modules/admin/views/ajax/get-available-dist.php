<style>
#res-available-dist td, #res-available-dist th {
  padding: .5rem !important;
  font-size: 10pt !important;
  vertical-align: middle !important;
}
</style>
<?php
$i = 0;
foreach ($res as $r) {
  $res[$i] = array(
    $r[COL_DATEDISTRIBUTION],
    $r[COL_NMSTOCK],
    $r[COL_NMBATCH],
    '<strong>'.number_format($r['JlhSisa']).'</strong>',
    $r[COL_NMSATUAN],
    $r[COL_DATEEXPIRED],
    '<button type="button" class="btn btn-xs btn-info btn-select-dist" data-iditem="'.$r[COL_UNIQ].'" data-batchno="'.$r[COL_NMBATCH].'" data-nmstock="'.$r[COL_NMSTOCK].'" data-nmsatuan="'.$r[COL_NMSATUAN].'" data-qty="'.$r['JlhSisa'].'">
      <i class="far fa-mouse-pointer"></i>&nbsp;PILIH
    </button>'
  );
  $i++;
}
$data = json_encode($res);
?>
<table id="res-available-dist" class="table table-striped table-bordered table-condensed" width="100%">
  <!--<thead class="bg-secondary">
    <tr>
      <th>TGL. DISTRIBUSI</th>
      <th>OBAT</th>
      <th>BATCH</th>
      <th>JLH.</th>
      <th>SATUAN</th>
      <th>TGL. EXPIRED</th>
      <th class="text-center">#</th>
    </tr>
  </thead>-->
  <tbody></tbody>
</table>
<script type="text/javascript">
$(document).ready(function() {
  var dtDist = $('#res-available-dist').dataTable({
    "autoWidth" : false,
    "aaData": <?=$data?>,
    "iDisplayLength": 10,
    "sDom": "Rfrtp",
    "ordering": false,
    "columnDefs": [
      {"targets":[0,5], "className":'dt-body-right nowrap'},
      {"targets":[3], "className":'dt-body-right'},
      {"targets":[6], "className":'text-center'}
    ],
    "aoColumns": [
      {"sTitle": "TANGGAL","bSortable":false},
      {"sTitle": "OBAT"},
      {"sTitle": "BATCH"},
      {"sTitle": "JLH."},
      {"sTitle": "SATUAN"},
      {"sTitle": "TGL. EXPIRED"},
      {"sTitle": "#"},
    ],
    "createdRow": function(row, data, dataIndex) {
      $('.btn-select-dist', row).click(function() {
        var form_ = $('#form-editor');
        var iditem = $(this).data('iditem');
        var batchno = $(this).data('batchno');
        var nmstock = $(this).data('nmstock');
        var nmsatuan = $(this).data('nmsatuan');
        var qty = $(this).data('qty');

        $('#modal-browse-item').modal('hide');
        $('[name=IdItem]', form_).val(iditem);
        $('[name=NmBatch]', form_).val(batchno);
        $('[name=NmStock]', form_).val(nmstock);
        $('[name=NmSatuan]', form_).val(nmsatuan);
        $('[name=Qty]', form_).val(qty);
        $('[name=Jumlah]', form_).val(qty);
      });
    }
  });
});
</script>
