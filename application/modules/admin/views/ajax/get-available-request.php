<style>
#res-available-request td, #res-available-request th {
  padding: .5rem !important;
  font-size: 11pt !important;
  vertical-align: middle !important;
}
</style>
<?php
$i = 0;
foreach ($res as $r) {
  $htmlTooltip = "";
  $ritems = $this->db
  ->join(TBL_MSTOCK.' st','st.'.COL_IDSTOCK." = trequest_items.".COL_IDSTOCK,"left")
  ->where(COL_IDREQUEST, $r[COL_UNIQ])
  ->get(TBL_TREQUEST_ITEMS)
  ->result_array();
  if(count($ritems)>0) {
    $htmlTooltip .= "<ul>";
    foreach ($ritems as $it) {
      $htmlTooltip .= "<li>".$it[COL_NMSTOCK]." x ".number_format($it[COL_JLHREQUEST])." ".$it[COL_NMSATUAN]."</li>";
    }
    $htmlTooltip .= "</ul>";
  } else {
    $htmlTooltip = "(kosong)";
  }
  $res[$i] = array(
    $r[COL_DATEREQUEST],
    $r[COL_NMREQUEST],
    '<a href="javascript:void(0)" data-toggle="tooltip" data-html="true" data-placement="bottom" title="'.$htmlTooltip.'"><strong>'.number_format($r['Jlh']).'</strong></a>',
    '<button type="button" class="btn btn-xs btn-info btn-select-request" data-idrequest="'.$r[COL_UNIQ].'">
      <i class="far fa-mouse-pointer"></i>&nbsp;PILIH
    </button>'
  );
  $i++;
}
$data = json_encode($res);
?>
<table id="res-available-request" class="table table-striped table-bordered table-condensed" width="100%">
  </tbody>
</table>
<script type="text/javascript">
$(document).ready(function() {
  var dtDist = $('#res-available-request').dataTable({
    "autoWidth" : false,
    "aaData": <?=$data?>,
    "iDisplayLength": 10,
    "sDom": "Rfrtp",
    "ordering": false,
    "columnDefs": [
      {"targets":[0], "className":'dt-body-right nowrap'},
      {"targets":[2,3], "className":'text-center'}
    ],
    "aoColumns": [
      {"sTitle": "TGL. PERMINTAAN","bSortable":false},
      {"sTitle": "NO. PERMINTAAN","bSortable":false},
      {"sTitle": "JLH. ITEM"},
      {"sTitle": "#"},
    ]
  });
  $('[data-toggle="tooltip"]').tooltip();
});
</script>
