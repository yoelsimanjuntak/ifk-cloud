<?php
$rpuskesmas = $this->db
->where(COL_ISDELETED, 0)
->order_by(COL_NMPUSKESMAS)
->get(TBL_MPUSKESMAS)
->result_array();
if(!empty($cetak)) {
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=LPLPO $month-$year ".date('YmdHi').".xls");
}
?>
<!--<div class="card card-default">
  <div class="card-header">
    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
    </div>
  </div>
  <div class="card-body">

  </div>
</div>-->
<div class="table-responsive">
  <table id="tbl-rekapitulasi2" class="table table-bordered" style="font-size: 10pt" border="1">
    <thead class="text-center">
      <tr>
        <th rowspan="2">No.</th>
        <th rowspan="2">Nama Obat</th>
        <th rowspan="2">Satuan</th>
        <th rowspan="2">Harga</th>
        <th colspan="5">STOK AWAL</th>
        <th colspan="5">PENERIMAAN</th>
        <th colspan="5">PERSEDIAAN</th>
        <th colspan="5">PEMAKAIAN</th>
        <th colspan="5">STOK AKHIR</th>
        <th rowspan="2">PERMINTAAN</th>
      </tr>
      <tr>
        <th>DAK</th>
        <th>PROGRAM</th>
        <th>DAU</th>
        <th>BUFFER</th>
        <th>JKN</th>
        <th>DAK</th>
        <th>PROGRAM</th>
        <th>DAU</th>
        <th>BUFFER</th>
        <th>JKN</th>
        <th>DAK</th>
        <th>PROGRAM</th>
        <th>DAU</th>
        <th>BUFFER</th>
        <th>JKN</th>
        <th>DAK</th>
        <th>PROGRAM</th>
        <th>DAU</th>
        <th>BUFFER</th>
        <th>JKN</th>
        <th>DAK</th>
        <th>PROGRAM</th>
        <th>DAU</th>
        <th>BUFFER</th>
        <th>JKN</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $no = 1;
      $prevstock = '';
      foreach($kategori as $kat) {
        ?>
        <tr>
          <td colspan="30"><strong><?=$kat[COL_NMKATEGORI]?></strong></td>
        </tr>
        <?php
        foreach($data[$kat[COL_IDKATEGORI]] as $dat) {
          ?>
          <tr>
            <?php
            if($dat[COL_NMSTOCK] != $prevstock) {
              ?>
              <td style="text-align: right" <?=$dat['ROWSPAN']>1?'rowspan='.$dat['ROWSPAN']:''?>><?=$no?></td>
              <td <?=$dat['ROWSPAN']>1?'rowspan='.$dat['ROWSPAN']:''?>><?=$dat[COL_NMSTOCK]?></td>
              <td <?=$dat['ROWSPAN']>1?'rowspan='.$dat['ROWSPAN']:''?>><?=$dat[COL_NMSATUAN]?></td>
              <?php
            }
            ?>
            <td style="text-align: right"><?=$dat[COL_HARGA]?></td>
            <td style="text-align: right"><?=$dat['NmSumber']=='DAK'?$dat['PrevDist']-$dat['PrevIssue']:0?></td>
            <td style="text-align: right"><?=$dat['NmSumber']=='PROGRAM'?$dat['PrevDist']-$dat['PrevIssue']:0?></td>
            <td style="text-align: right"><?=$dat['NmSumber']=='DAU'?$dat['PrevDist']-$dat['PrevIssue']:0?></td>
            <td style="text-align: right"><?=$dat['NmSumber']=='BUFFER STOCK'?$dat['PrevDist']-$dat['PrevIssue']:0?></td>
            <td style="text-align: right"><?=$dat['NmSumber']=='JKN'?$dat['PrevDist']-$dat['PrevIssue']:0?></td>

            <td style="text-align: right"><?=$dat['NmSumber']=='DAK'?$dat['CurrDist']:0?></td>
            <td style="text-align: right"><?=$dat['NmSumber']=='PROGRAM'?$dat['CurrDist']:0?></td>
            <td style="text-align: right"><?=$dat['NmSumber']=='DAU'?$dat['CurrDist']:0?></td>
            <td style="text-align: right"><?=$dat['NmSumber']=='BUFFER STOCK'?$dat['CurrDist']:0?></td>
            <td style="text-align: right"><?=$dat['NmSumber']=='JKN'?$dat['CurrDist']:0?></td>

            <td style="text-align: right"><?=$dat['NmSumber']=='DAK'?$dat['PrevDist']-$dat['PrevIssue']+$dat['CurrDist']:0?></td>
            <td style="text-align: right"><?=$dat['NmSumber']=='PROGRAM'?$dat['PrevDist']-$dat['PrevIssue']+$dat['CurrDist']:0?></td>
            <td style="text-align: right"><?=$dat['NmSumber']=='DAU'?$dat['PrevDist']-$dat['PrevIssue']+$dat['CurrDist']:0?></td>
            <td style="text-align: right"><?=$dat['NmSumber']=='BUFFER STOCK'?$dat['PrevDist']-$dat['PrevIssue']+$dat['CurrDist']:0?></td>
            <td style="text-align: right"><?=$dat['NmSumber']=='JKN'?$dat['PrevDist']-$dat['PrevIssue']+$dat['CurrDist']:0?></td>

            <td style="text-align: right"><?=$dat['NmSumber']=='DAK'?$dat['CurrIssue']:0?></td>
            <td style="text-align: right"><?=$dat['NmSumber']=='PROGRAM'?$dat['CurrIssue']:0?></td>
            <td style="text-align: right"><?=$dat['NmSumber']=='DAU'?$dat['CurrIssue']:0?></td>
            <td style="text-align: right"><?=$dat['NmSumber']=='BUFFER STOCK'?$dat['CurrIssue']:0?></td>
            <td style="text-align: right"><?=$dat['NmSumber']=='JKN'?$dat['CurrIssue']:0?></td>

            <td style="text-align: right"><?=$dat['NmSumber']=='DAK'?$dat['PrevDist']-$dat['PrevIssue']+$dat['CurrDist']-$dat['CurrIssue']:0?></td>
            <td style="text-align: right"><?=$dat['NmSumber']=='PROGRAM'?$dat['PrevDist']-$dat['PrevIssue']+$dat['CurrDist']-$dat['CurrIssue']:0?></td>
            <td style="text-align: right"><?=$dat['NmSumber']=='DAU'?$dat['PrevDist']-$dat['PrevIssue']+$dat['CurrDist']-$dat['CurrIssue']:0?></td>
            <td style="text-align: right"><?=$dat['NmSumber']=='BUFFER STOCK'?$dat['PrevDist']-$dat['PrevIssue']+$dat['CurrDist']-$dat['CurrIssue']:0?></td>
            <td style="text-align: right"><?=$dat['NmSumber']=='JKN'?$dat['PrevDist']-$dat['PrevIssue']+$dat['CurrDist']-$dat['CurrIssue']:0?></td>

            <?php
            if($dat[COL_NMSTOCK] != $prevstock) {
              ?>
              <td style="text-align: right" <?=$dat['ROWSPAN']>1?'rowspan='.$dat['ROWSPAN']:''?>><?=!empty($dat['CurrRequest'])?$dat['CurrRequest']:0?></td>
              <?php
            }
            ?>
          </tr>
          <?php
          $no++;
          $prevstock = $dat[COL_NMSTOCK];
        }
      }
      ?>
    </tbody>
  </table>
</div>
<?php
if(empty($cetak)) {
  ?>
  <script type="text/javascript">
  $(document).ready(function() {
    var dt = $('#tbl-rekapitulasi').dataTable({
      "autoWidth" : false,
      "scrollY" : '40vh',
      "fixedColumns": true,
      "fixedHeader": true,
      "scrollX": true,
      "ordering": false,
      "iDisplayLength": 50,
    });
  });
  </script>
  <?php
}
?>
