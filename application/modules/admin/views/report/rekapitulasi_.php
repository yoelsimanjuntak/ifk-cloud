<?php
$rpuskesmas = $this->db
->where(COL_ISDELETED." != ", 1)
->order_by(COL_KDTIPE)
->order_by(COL_NMPUSKESMAS)
->get(TBL_MPUSKESMAS)
->result_array();
if(!empty($cetak)) {
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Rekapitulasi Obat ".date('Ymd-Hi').".xls");
}
?>
<!--<div class="card card-default">
  <div class="card-header">
    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
    </div>
  </div>
  <div class="card-body">

  </div>
</div>-->
<div class="table-responsive">
  <table id="tbl-rekapitulasi" class="table table-bordered" style="font-size: 10pt" border="1">
    <thead class="text-center">
      <tr>
        <th rowspan="2" style="vertical-align: middle">No.</th>
        <th rowspan="2" style="vertical-align: middle">Nama Obat</th>
        <th rowspan="2" style="vertical-align: middle">Satuan</th>
        <th colspan="2">Persediaan</th>
        <th rowspan="2" style="vertical-align: middle">Tgl. Kadaluarsa</th>
        <th <?=!empty($rpuskesmas)&&count($rpuskesmas)>0?'colspan="'.count($rpuskesmas).'"':''?> style="border-right-width: 0">Distribusi</th>
        <th rowspan="2" style="vertical-align: middle; border-left-width: 1px">Jlh. Distribusi</th>
        <th rowspan="2" style="vertical-align: middle">Jlh. Pemakaian</th>
        <th rowspan="2" style="vertical-align: middle; white-space: nowrap">Jlh. Akhir<br />(Persediaan - Pemakaian)</th>
      </tr>
      <tr>
        <th style="border-top:0 !important">Jlh.<br />Stok Awal</th>
        <th style="border-top:0 !important">Jlh.<br />Penerimaan</th>
        <?php
        foreach ($rpuskesmas as $p) {
          ?>
          <th style="vertical-align: middle; font-size: 10pt; width: 150px; border-top:0 !important"><?=$p[COL_NMPUSKESMAS]?></th>
          <?php
        }
        ?>
      </tr>
    </thead>
    <tbody>
      <?php
      $no = 1;
      foreach($res as $r) {
        ?>
        <tr>
          <td><?=$no?></td>
          <td style="white-space: nowrap"><?=$r[COL_NMSTOCK]?></td>
          <td><?=$r[COL_NMSATUAN]?></td>
          <td class="text-right" style="min-width: 100px;"><?=number_format($r['PrevReceipt']-$r['PrevIssue'])?></td>
          <td class="text-right" style="min-width: 100px"><?=number_format($r['TotalReceipt'])?></td>
          <td class="text-right" style="white-space: nowrap"><?=$r[COL_DATEEXPIRED]?></td>
          <?php
          $month = $this->input->get("month");
          $year = $this->input->get("year");
          $dateFrom = $year.'-'.$month.'-01';
          $dateTo = date("Y-m-d", strtotime("+1 month", strtotime($dateFrom)));
          $idStock = $r[COL_IDSTOCK];

          $sumDist = 0;
          foreach ($rpuskesmas as $p) {
            $idPuskesmas = $p[COL_IDPUSKESMAS];
            $qDist = @"
            select coalesce(sum(is_.Jumlah),0) as JlhDistribusi
            from tstockdistribution_items is_
            inner join tstockdistribution dist_ on dist_.Uniq = is_.IdDistribution
            inner join tstockreceipt r_ on r_.Uniq = is_.IdReceipt
            where
              r_.IdStock = $idStock
              and dist_.IdPuskesmas = $idPuskesmas
              and dist_.DateDistribution >= '$dateFrom'
              and dist_.DateDistribution < '$dateTo'
            ";
            $rdist = $this->db->query($qDist)->row_array();
            $sumDist += $rdist['JlhDistribusi'];
            ?>
            <td class="text-right"><?=number_format($rdist['JlhDistribusi'])?></td>
            <?php
          }
          $qIssue = @"
          select coalesce(sum(i.Jumlah),0) as JlhIssue
          from tstockissue i
          inner join tstockdistribution_items is_ on is_.Uniq = i.IdItem
          inner join tstockdistribution dist_ on dist_.Uniq = is_.IdDistribution
          inner join tstockreceipt r_ on r_.Uniq = is_.IdReceipt
          where
            r_.IdStock = $idStock
            and i.DateIssue >= '$dateFrom'
            and i.DateIssue < '$dateTo'
          ";
          $rIssue = $this->db->query($qIssue)->row_array();
          ?>
          <td class="text-right font-weight-bold" style="min-width: 100px"><?=number_format($sumDist)?></td>
          <td class="text-right font-weight-bold" style="min-width: 100px"><?=number_format($rIssue['JlhIssue'])?></td>
          <td class="text-right font-weight-bold" style="min-width: 100px"><?=number_format($r['PrevReceipt']-$r['PrevIssue']+$r['TotalReceipt']-$rIssue['JlhIssue'])?></td>
        </tr>
        <?php
        $no++;
      }
      ?>
    </tbody>
  </table>
</div>
<?php
if(empty($cetak)) {
  ?>
  <script type="text/javascript">
  $(document).ready(function() {
    var dt = $('#tbl-rekapitulasi').dataTable({
      "autoWidth" : false,
      "scrollY" : '40vh',
      "fixedColumns": true,
      "fixedHeader": true,
      "scrollX": true,
      "ordering": false,
      "iDisplayLength": 50,
    });
  });
  </script>
  <?php
}
?>
