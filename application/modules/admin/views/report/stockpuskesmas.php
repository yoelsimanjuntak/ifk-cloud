<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 font-weight-light"><?= $title ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-tachometer-alt"></i> DASHBOARD</a></li>
          <li class="breadcrumb-item active">LAPORAN</li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
          <div class="card card-default">
            <div class="card-header">
              <?=form_open(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal','method'=>'get'))?>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group row mb-0">
                    <label class="control-label col-sm-2">PUSKESMAS</label>
                    <div class="col-sm-3">
                      <select class="form-control form-control-sm no-select2" name="idPuskesmas" style="width: 100%" required>
                        <?=GetCombobox("SELECT * FROM mpuskesmas WHERE IsDeleted != 1 ORDER BY NmPuskesmas", COL_IDPUSKESMAS, COL_NMPUSKESMAS, (!empty($_GET['idPuskesmas'])?$_GET['idPuskesmas']:null))?>
                      </select>
                    </div>

                    <label class="control-label col-sm-1">PERIODE</label>
                    <?php
                    $selMonth = date('m');
                    if(!empty($_GET['month'])) {
                      $selMonth = $_GET['month'];
                    }
                    $selMonth = str_pad($selMonth, 2, "0", STR_PAD_LEFT);
                    ?>
                    <div class="col-sm-2">
                      <select class="form-control form-control-sm no-select2" name="month" style="width: 100%">
                        <option value="01" <?=$selMonth=="01"?'selected':''?>>Januari</option>
                        <option value="02" <?=$selMonth=="02"?'selected':''?>>Febuari</option>
                        <option value="03" <?=$selMonth=="03"?'selected':''?>>Maret</option>
                        <option value="04" <?=$selMonth=="04"?'selected':''?>>April</option>
                        <option value="05" <?=$selMonth=="05"?'selected':''?>>Mei</option>
                        <option value="06" <?=$selMonth=="06"?'selected':''?>>Juni</option>
                        <option value="07" <?=$selMonth=="07"?'selected':''?>>Juli</option>
                        <option value="08" <?=$selMonth=="08"?'selected':''?>>Agustus</option>
                        <option value="09" <?=$selMonth=="09"?'selected':''?>>September</option>
                        <option value="10" <?=$selMonth=="10"?'selected':''?>>Oktober</option>
                        <option value="11" <?=$selMonth=="11"?'selected':''?>>November</option>
                        <option value="12" <?=$selMonth=="12"?'selected':''?>>Desember</option>
                      </select>
                    </div>
                    <div class="col-sm-1">
                      <input type="number" class="form-control form-control-sm text-right" name="year" value="<?=!empty($_GET['year'])?$_GET['year']:date('Y')?>" />
                    </div>
                    <div class="col-sm-3">
                      <button type="submit" class="btn btn-sm btn-outline-primary" title="Lihat"><i class="fa fa-arrow-circle-right"></i> TAMPILKAN</button>
                      <button type="submit" class="btn btn-sm btn-outline-success" title="Cetak" name="cetak" value="1"><i class="fa fa-print"></i> CETAK</button>
                    </div>
                  </div>
                </div>
              </div>
              <?=form_close()?>
            </div>
            <div class="card-body">
              <?php
              if(!empty($res)) {
                $this->load->view('admin/report/stockpuskesmas_');
              }
              ?>
            </div>
          </div>
      </div>
    </div>
  </div>
</section>
