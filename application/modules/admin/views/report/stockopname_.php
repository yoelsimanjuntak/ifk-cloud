<!--<div class="card card-default">
  <div class="card-header">
    <div class="card-tools">
      <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
    </div>
  </div>
  <div class="card-body">

  </div>
</div>-->
<div class="table-responsive">
  <table id="tbl-rekapitulasi" class="table table-bordered" style="font-size: 10pt" border="1">
    <thead class="text-center">
      <tr>
        <th>No.</th>
        <th>Nama Obat</th>
        <th>Satuan</th>
        <th>Tgl. Kadaluarsa</th>
        <th>Jlh. Stok Awal</th>
        <th>Jlh. Penerimaan</th>
        <th>Jlh. Distribusi</th>
        <th>Jlh. Distribusi Khusus</th>
        <th>Jlh. Stock Opname</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $no = 1;
      foreach($res as $r) {
        ?>
        <tr>
          <td><?=$no?></td>
          <td style="white-space: nowrap"><?=$r[COL_NMSTOCK]?></td>
          <td><?=$r[COL_NMSATUAN]?></td>
          <td class="text-right" style="white-space: nowrap"><?=$r[COL_DATEEXPIRED]?></td>
          <td class="text-right" style="min-width: 100px;"><?=number_format($r['PrevReceipt']-$r['PrevDistribution'])?></td>
          <td class="text-right" style="min-width: 100px"><?=number_format($r['TotalReceipt'])?></td>
          <td class="text-right" style="min-width: 100px"><?=number_format($r['TotalDistribution'])?></td>
          <td class="text-right" style="min-width: 100px"><?=number_format($r['TotalDistributionKhusus'])?></td>
          <td class="text-right font-weight-bold" style="min-width: 100px"><?=number_format($r['PrevReceipt']-$r['PrevDistribution']+$r['TotalReceipt']-$r['TotalDistribution']-$r['TotalDistributionKhusus'])?></td>
        </tr>
        <?php
        $no++;
      }
      ?>
    </tbody>
  </table>
</div>
<?php
if(empty($cetak)) {
  ?>
  <script type="text/javascript">
  $(document).ready(function() {
    var dt = $('#tbl-rekapitulasi').dataTable({
      "autoWidth" : false,
      "scrollY" : '40vh',
      "fixedColumns": true,
      "fixedHeader": true,
      "scrollX": true,
      "ordering": false,
      "iDisplayLength": 50,
    });
  });
  </script>
  <?php
}
?>
