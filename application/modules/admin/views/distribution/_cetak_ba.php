<html>
<style>
body {
  /*font-family: "Source Sans Pro",-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol"*/
}
table.table-bordered, .table-bordered td, .table-bordered th {
  border: 1px solid #000;
  border-collapse: collapse;
}
.table-bordered td, .table-bordered th {
  padding: .25rem;
  font-size: 8.5pt !important;
}
</style>
<body>
  <h4 style="text-align: center; margin-bottom: 0">BERITA ACARA<br />SERAH TERIMA OBAT-OBATAN & PERBEKKES<br />Nomor:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/BA/UPTD-IFK/&nbsp;&nbsp;&nbsp;&nbsp;/<?=date('Y', strtotime($data[COL_DATEDISTRIBUTION]))?></h4><hr />
  <br />
  <p>
    Pada tanggal <strong><?=date('d-m-Y', strtotime($data[COL_DATEDISTRIBUTION]))?></strong>, yang bertandatangan dibawah ini:
  </p>
  <table>
    <tr>
      <td>Nama</td><td>:</td><td style="font-weight: bold"><?=GetSetting('SETTING_ORG_KAUPTNAMA')?></td>
    </tr>
    <tr>
      <td>NIP</td><td>:</td><td style="font-weight: bold"><?=GetSetting('SETTING_ORG_KAUPTNIP')?></td>
    </tr>
    <tr>
      <td>Jabatan</td><td>:</td><td style="font-weight: bold">Kepala <?=GetSetting('SETTING_ORG_NAME')?></td>
    </tr>
  </table>
  <br />
  <p>
    Selanjutnya sebagai Pihak I
  </p>
  <table>
    <tr>
      <td>Nama</td><td>:</td><td style="font-weight: bold"><?=!empty($data[COL_NMKAPUSKESMAS])?$data[COL_NMKAPUSKESMAS]:'<span style="font-style:italic">(kosong)</span>'?></td>
    </tr>
    <tr>
      <td>NIP</td><td>:</td><td style="font-weight: bold"><?=!empty($data[COL_NMKAPUSKESMASNIP])?$data[COL_NMKAPUSKESMASNIP]:'<span style="font-style:italic">(kosong)</span>'?></td>
    </tr>
    <tr>
      <td>Unit Kerja</td><td>:</td><td style="font-weight: bold"><?=$data[COL_NMPUSKESMAS]?></td>
    </tr>
  </table>
  <br />
  <p>
    Selanjutnya sebagai Pihak I
  </p>
  <p style="text-align: justify">
    Pihak I dalam hal ini bertindak selaku Pengelola Obat dan Perbekkes Dinas Kesehatan Kota Tebing Tinggi menyerahkan sejumlah obat-obatan dan
    perbekalan kesehatan kepada Pihak II selaku Bidang / Unit Pelayanan Kesehatan, sebagai berikut:
  </p>
  <br />
  <table width="100%" class="table-bordered" cellmargin="0">
    <thead>
      <tr style="background-color: #dedede">
        <th rowspan="2">NO</th>
        <th rowspan="2">NAMA OBAT / PERBEKKES</th>
        <th rowspan="2">SATUAN</th>
        <th rowspan="2">HARGA</th>
        <th colspan="4">PEMBERIAN</th>
        <th rowspan="2">TOTAL</th>
        <th rowspan="2">EXP.</th>
        <th rowspan="2">BATCH</th>
        <th rowspan="2">PABRIK</th>
      </tr>
      <tr style="background-color: #dedede">
        <th>DAK</th>
        <th>PROGRAM</th>
        <th>DAU</th>
        <th>BUFFER</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $no=1;
      $prevstock = '';
      $sumHarga = 0;
      foreach($det as $d) {
        ?>
        <tr>
          <?php
          if($d[COL_NMSTOCK] != $prevstock) {
            ?>
            <td style="text-align: right"><?=$no?></td>
            <td><?=$d[COL_NMSTOCK]?></td>
            <td><?=$d[COL_NMSATUAN]?></td>
            <?php
          }
          ?>
          <td style="text-align: right"><?=number_format($d[COL_HARGA])?></td>
          <td style="text-align: right; width: 50px"><?=$d[COL_NMSUMBER]=='DAK'?number_format($d['TotalQty']):'-'?></td>
          <td style="text-align: right; width: 50px"><?=$d[COL_NMSUMBER]=='PROGRAM'?number_format($d['TotalQty']):'-'?></td>
          <td style="text-align: right; width: 50px"><?=$d[COL_NMSUMBER]=='DAU'?number_format($d['TotalQty']):'-'?></td>
          <td style="text-align: right; width: 50px"><?=$d[COL_NMSUMBER]=='BUFFER STOCK'?number_format($d['TotalQty']):'-'?></td>
          <td style="text-align: right"><?=number_format($d[COL_HARGA]*$d['TotalQty'])?></td>
          <td style="text-align: right"><?=date('d-m-Y', strtotime($d[COL_DATEEXPIRED]))?></td>
          <td><?=$d[COL_NMBATCH]?></td>
          <td style="white-space: nowrap"><?=$d[COL_NMPABRIK]?></td>
        </tr>
        <?php
        $no++;
        $prevstock = $d[COL_NMSTOCK];
        $sumHarga += $d[COL_HARGA]*$d['TotalQty'];
      }
      ?>
      <tr>
        <td colspan="8" style="font-weight: bold; text-align: right">TOTAL</td>
        <td style="font-weight: bold; text-align: right"><?=number_format($sumHarga)?></td>
      </tr>
    </tbody>
  </table>
  <br />
  <table width="100%" cellpadding="2">
    <tr>
      <td style="width: 33%; white-space: nowrap">Yang menerima</td>
      <td style="width: 33%; white-space: nowrap"></td>
      <td style="width: 33%; white-space: nowrap">Yang menyerahkan</td>
    </tr>
    <tr>
      <td style="width: 33%; white-space: nowrap">Pihak II</td>
      <td style="width: 33%; white-space: nowrap"></td>
      <td style="width: 33%; white-space: nowrap">Pihak I</td>
    </tr>
    <tr>
      <td style="width: 33%; white-space: nowrap; font-weight: bold">
        <br /><br /><br />
        <?=!empty($data[COL_NMKAPUSKESMAS])?$data[COL_NMKAPUSKESMAS]:'<span style="font-style: italic">(kosong)</span>'?><br />
        NIP. <?=!empty($data[COL_NMKAPUSKESMASNIP])?$data[COL_NMKAPUSKESMASNIP]:'<span style="font-style: italic">(kosong)</span>'?>
      </td>
      <td></td>
      <td style="width: 33%; white-space: nowrap; font-weight: bold">
        <br /><br /><br />
        <?=GetSetting('SETTING_ORG_KAUPTNAMA')?><br />
        NIP. <?=GetSetting('SETTING_ORG_KAUPTNIP')?>
      </td>
    </tr>
  </table>
</body>
</html>
