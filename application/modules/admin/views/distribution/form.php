<style>
#tbl-items td, #tbl-items th {
  padding: .5rem !important;
  font-size: 11pt !important;
  vertical-align: middle !important;
}
#tbl-items .select2-container {
  margin-right: 0 !important;
}
</style>
<div class="modal-header">
  <h5 class="modal-title">Form Distribusi</h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true"><i class="fa fa-close"></i></span>
  </button>
</div>
<div class="modal-body">
  <p class="text-danger error-message"></p>
  <form id="form-editor" method="post" action="#">
    <div class="row">
      <div class="col-sm-2">
        <div class="form-group">
          <label>TANGGAL</label>
          <input type="text" class="form-control datepicker text-right" name="<?=COL_DATEDISTRIBUTION?>" value="<?=!empty($data)?$data[COL_DATEDISTRIBUTION]:""?>" required />
        </div>
      </div>
      <?php
      if(!isset($isKhusus) || !$isKhusus) {
        ?>
        <div class="col-sm-5">
          <div class="form-group">
            <label>PUSKESMAS</label>
            <select class="form-control" name="<?=COL_IDPUSKESMAS?>" style="width: 100%" required>
              <?=GetCombobox("SELECT * FROM mpuskesmas WHERE IsDeleted != 1 and KdTipe = 1 ORDER BY NmPuskesmas", COL_IDPUSKESMAS, COL_NMPUSKESMAS, (!empty($data)?$data[COL_IDPUSKESMAS]:null))?>
            </select>
          </div>
        </div>
        <?php
      }
      ?>
      <div class="col-sm-5">
        <div class="form-group">
          <label>NO. REFERENSI</label>
          <input type="text" class="form-control" name="<?=COL_NMREFERENSI?>" value="<?=!empty($data)?$data[COL_NMREFERENSI]:""?>" />
        </div>
      </div>
      <div class="col-sm-12">
        <div class="form-group">
          <div class="form-group">
            <label><?=isset($isKhusus) && $isKhusus?'TUJUAN':'CATATAN'?></label>
            <textarea rows="2" class="form-control" name="<?=COL_NMREMARKS?>"><?=!empty($data)?$data[COL_NMREMARKS]:''?></textarea>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="form-group row mb-0">
          <label class="col-sm-2">ITEM :</label>
          <div class="col-sm-10 text-right">
            <!--<button type="button" id="btn-load-request" class="btn btn-xs btn-outline-secondary"><i class="fa fa-search"></i> ISI BERDASARKAN PERMINTAAN</button>-->
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          <input type="hidden" name="DistItems" />
          <div class="row">
            <div class="col-sm-12">
              <table id="tbl-items" class="table table-bordered">
                <thead class="bg-info">
                  <tr>
                    <th>Nama Obat</th>
                    <th>Batch</th>
                    <th>Jumlah</th>
                    <th>Satuan</th>
                    <th class="text-center">#</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
                <?php
                if(!isset($disabled) || empty($disabled)) {
                  ?>
                  <tfoot>
                    <tr>
                      <th style="white-space: nowrap; min-width: 250px">
                        <select name="<?=COL_IDSTOCK?>" class="form-control form-control-sm" style="width: 100%;">
                          <?=GetCombobox("SELECT * from mstock where IsDeleted != 1 order by NmStock", COL_IDSTOCK, COL_NMSTOCK, null, true, false, '-- Pilih Obat --')?>
                        </select>
                      </th>
                      <th style="white-space: nowrap">
                        <div class="input-group input-group-sm">
                          <input type="hidden" name="<?=COL_IDRECEIPT?>" readonly />
                          <input type="hidden" name="<?=COL_NMSTOCK?>" readonly />
                          <input type="text" class="form-control" name="<?=COL_NMBATCH?>" readonly />
                          <span class="input-group-append">
                            <button type="button" id="btn-browse-item" class="btn btn-info"><i class="far fa-search"></i><!--&nbsp;BROWSE..--></button>
                          </span>
                        </div>
                      </th>
                      <th style="white-space: nowrap; max-width: 100px">
                        <input type="text" class="form-control form-control-sm uang text-right" name="<?=COL_JUMLAH?>" />
                      </th>
                      <th style="white-space: nowrap;">
                        <input type="text" class="form-control form-control-sm" name="<?=COL_NMSATUAN?>" readonly />
                      </th>
                      <th style="white-space: nowrap;" class="text-center">
                        <button type="button" id="btn-add-item" class="btn btn-sm btn-default"><i class="fa fa-plus"></i></button>
                      </th>
                    </tr>
                  </tfoot>
                  <?php
                }
                ?>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php
    if(!empty($data)) {
      ?>
      <p class="text-muted text-sm font-italic label-info mb-0 mt-3 pt-2">
        Diinput oleh: <b><?=$data['Nm_CreatedBy']?></b><br />
        Diinput pada: <b><?=date('Y-m-d H:i:s', strtotime($data[COL_CREATEDON]))?></b>
      </p>
      <?php
    }
    ?>
    <button type="submit" class="d-none">SUBMIT</button>
  </form>
</div>
<?php
if(!isset($disabled) || empty($disabled)) {
  ?>
  <div class="modal-footer">
    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">BATAL</button>
    <button type="button" class="btn btn-outline-primary btn-ok">SIMPAN</button>
  </div>
  <?php
}
?>
<script>
var form = $('#form-editor');
$(document).ready(function() {
  $(".money", form).number(true, 2, '.', ',');
  $(".uang", form).number(true, 0, '.', ',');
  $("select", form).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
  $('.datepicker', form).daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: parseInt(moment().format('YYYY'),10)-2,
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
        format: 'YYYY-MM-DD'
    }
  });
  $('.btn-ok').click(function() {
    $('#form-editor').submit();
  });

  $('#modal-browse-item, #modal-browse-request').on('show.bs.modal', function () {
    form.closest('.modal').modal('hide');
  });
  $('#modal-browse-item, #modal-browse-request').on('hide.bs.modal', function () {
    form.closest('.modal').modal('show');
  });

  $('#btn-browse-item', form).click(function() {
    var idstock = $('[name=IdStock]', form).val();
    var tanggal = $('[name=DateDistribution]', form).val();
    $('.modal-body', $('#modal-browse-item')).load('<?=site_url('admin/ajax/get-available-receipt')?>', {IdStock: idstock, Tanggal: tanggal}, function(){
      $('#modal-browse-item').modal('show');

      $('.btn-select-receipt', $('table#res-available-receipt')).click(function() {
        var idreceipt = $(this).data('idreceipt');
        var batchno = $(this).data('batchno');
        var nmstock = $(this).data('nmstock');

        $('#modal-browse-item').modal('hide');
        $('[name=IdReceipt]', form).val(idreceipt);
        $('[name=NmBatch]', form).val(batchno);
        $('[name=NmStock]', form).val(nmstock);
      });
    });

  });

  /*$('#btn-load-request', form).click(function() {
    var IdPuskesmas = $('[name=IdPuskesmas]', form).val();
    $('.modal-body', $('#modal-browse-request')).load('<?=site_url('admin/ajax/get-available-request')?>', {IdPuskesmas: IdPuskesmas}, function(){
      $('#modal-browse-request').modal('show');
      $('.btn-select-request', $('table#res-available-request')).click(function() {
        var idrequest = $(this).data('idrequest');
        $.ajax({
          url: "<?=site_url('admin/ajax/get-request-item')?>",
          method: "POST",
          data: {IdRequest: idrequest}
        }).success(function(res) {
          var items = JSON.parse(res);
          if(items && items.length > 0) {
            var arrIx = [];
            for(var ix=0; ix<items.length; ix++) {
              arr.push({'IdReceipt': IdReceipt, 'NmBatch':NmBatch, 'NmStock':NmStock, 'Jumlah':Jumlah, 'NmSatuan':NmSatuan});
            }
          }
        }).done(function() {
          $('#modal-browse-request').modal('hide');
        });
      });
    });

  });*/

  $('[name=DistItems]', form).change(function() {
    writeItem('tbl-items', 'DistItems');
  }).val(encodeURIComponent('<?=$DistItems?>')).trigger('change');

  $('[name=IdStock]', form).change(function() {
    var val = $(this).val();
    $.ajax({
      url: "<?=site_url('admin/ajax/get-stock-uom')?>",
      method: "POST",
      data: {IdStock: val}
    }).done(function(data) {
      $('[name=NmSatuan]', form).val(data);
    });
  });

  $('#btn-add-item', $('#tbl-items')).click(function() {
    var dis = $(this);
    var arr = $('[name=DistItems]').val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var row = dis.closest('tr');
    var IdReceipt = $('[name=IdReceipt]', row).val();
    var NmBatch = $('[name=NmBatch]', row).val();
    var NmStock = $('[name=NmStock]', row).val();
    var Jumlah = $('[name=Jumlah]', row).val();
    var NmSatuan = $('[name=NmSatuan]', row).val();
    if(IdReceipt && Jumlah) {
      var exist = jQuery.grep(arr, function(a) {
        return a.IdReceipt == IdReceipt;
      });
      if(exist.length == 0) {
        arr.push({'IdReceipt': IdReceipt, 'NmBatch':NmBatch, 'NmStock':NmStock, 'Jumlah':Jumlah, 'NmSatuan':NmSatuan});
        $('[name=DistItems]').val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
        $('input', row).val('');
        $('select', row).val('').trigger('change');
      } else {
        alert('Item dengan No. BATCH yang sama tidak bisa diinput berulang.');
        return false;
      }
    } else {
      alert('Harap isi kolom isian dengan benar.');
      return false;
    }
  });
  <?php
  if(!empty($disabled)) {
    ?>
    $('input,select,textarea,button', form).not('[data-dismiss=modal]').attr('disabled', true);
    <?php
  }
  ?>
});

function writeItem(tbl, input) {
  var tbl = $('#'+tbl+'>tbody');
  var arr = $('[name='+input+']').val();
  if(arr) {
    arr = JSON.parse(decodeURIComponent(arr));
    if(arr.length > 0) {
      var html = '';
      for (var i=0; i<arr.length; i++) {
        html += '<tr>';
        html += '<td>'+arr[i].NmStock+'</td>';
        html += '<td>'+arr[i].NmBatch+'</td>';
        html += '<td class="text-right">'+$.number(arr[i].Jumlah, 0)+'</td>';
        html += '<td>'+arr[i].NmSatuan+'</td>';
        <?php
        if(!isset($disabled) || empty($disabled)) {
          ?>
          html += '<td class="text-center"><button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="'+arr[i].IdReceipt+'" /></td>';
          <?php
        } else {
          ?>
          html += '<td class="text-center">-</td>';
          <?php
        }
        ?>
        html += '</tr>';
      }
      tbl.html(html);

      $('.btn-del', tbl).click(function() {
        var row = $(this).closest('tr');
        var idx = $('[name=idx]', row).val();
        if(idx) {
          var arr = $('[name='+input+']').val();
          arr = JSON.parse(decodeURIComponent(arr));

          var arrNew = $.grep(arr, function(e){ return e.IdReceipt != idx; });
          $('[name='+input+']').val(encodeURIComponent(JSON.stringify(arrNew))).trigger('change');
        }
      });
    } else {
      tbl.html('<tr><td colspan="5"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
    }
  } else {
    tbl.html('<tr><td colspan="5"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
  }
}

/*onScan.attachTo(document, {
  onScan: function(sCode, iQty) {
    var batch_ = sCode;
    var tanggal = $('[name=DateDistribution]', form).val();
    $.ajax({
      url: "<?=site_url('admin/ajax/get-receipt-by-batch')?>",
      method: "POST",
      data: {NmBatch: batch_, Tanggal: tanggal}
    }).done(function(res) {
      res = JSON.parse(res);
      if(!res) {
        toastr.error('Maaf, obat dengan BATCH <strong>'+batch_+'</strong> tidak ditemukan.');
      }

      $('[name=IdStock]', form).val(res.IdStock).trigger('change');
      $('[name=IdReceipt]', form).val(res.Uniq);
      $('[name=NmBatch]', form).val(res.NmBatch);
      $('[name=NmStock]', form).val(res.NmStock);
    });
  }
});*/
/*document.addEventListener('scan', function(sCode, iQty) {
});*/
document.addEventListener('scan', function(sScancode, iQuantity) {
  var tanggal = $('[name=DateDistribution]', form).val();
  $.ajax({
    url: "<?=site_url('admin/ajax/get-receipt-by-batch')?>",
    method: "POST",
    data: {NmBatch: sScancode.detail.scanCode, Tanggal: tanggal}
  }).done(function(res) {
    res = JSON.parse(res);
    if(!res) {
      toastr.error('Maaf, obat dengan BATCH <strong>'+sScancode.detail.scanCode+'</strong> tidak ditemukan.');
      return false;
    } else {
      $('[name=IdStock]', form).val(res.IdStock).trigger('change');
      $('[name=IdReceipt]', form).val(res.Uniq);
      $('[name=NmBatch]', form).val(res.NmBatch);
      $('[name=NmStock]', form).val(res.NmStock);
    }
  });
});
</script>
