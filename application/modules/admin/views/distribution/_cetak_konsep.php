<html>
<style>
body {
  /*font-family: "Source Sans Pro",-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol"*/
}
table.table-bordered, .table-bordered td, .table-bordered th {
  border: 1px solid #000;
  border-collapse: collapse;
}
.table-bordered td, .table-bordered th {
  padding: .25rem;
  font-size: 8.5pt !important;
}
</style>
<body>
  <h4 style="text-align: center; margin-bottom: 0">KONSEP LEMBAR PEMBERIAN OBAT-OBATAN<br />KE PUSKESMAS DAN SUB UNITNYA</h4><hr />
  <table>
    <tr>
      <td style="width: 50px; white-space: nowrap">PUSKESMAS</td><td style="width: 10px; white-space: nowrap">:</td>
      <td style="font-weight: bold"><?=$data[COL_NMPUSKESMAS]?></td>
    </tr>
    <tr>
      <td style="width: 50px; white-space: nowrap">SUB UNIT</td><td style="width: 10px; white-space: nowrap">:</td>
      <td style="font-weight: bold"></td>
    </tr>
    <tr>
      <td style="width: 50px; white-space: nowrap">TANGGAL</td><td style="width: 10px; white-space: nowrap">:</td>
      <td style="font-weight: bold"><?=date('d-m-Y', strtotime($data[COL_DATEDISTRIBUTION]))?></td>
    </tr>
  </table>
  <br />
  <table width="100%" class="table-bordered" cellmargin="0">
    <thead>
      <tr style="background-color: #dedede">
        <th rowspan="2">NO</th>
        <th rowspan="2">NAMA OBAT</th>
        <th rowspan="2">SATUAN</th>
        <th rowspan="2">EXP. DATE</th>
        <th colspan="4">PEMBERIAN</th>
      </tr>
      <tr style="background-color: #dedede">
        <th>DAK</th>
        <th>PROGRAM</th>
        <th>DAU</th>
        <th>BUFFER</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $no=1;
      foreach($kat as $k) {
        ?>
        <tr>
          <td colspan="8" style="font-weight: bold"><?=$k?></td>
        </tr>
        <?php
        foreach($det as $d) {
          if($d[COL_NMKATEGORI] != $k) continue;
          ?>
          <tr>
            <td style="text-align: right"><?=$no?></td>
            <td><?=$d[COL_NMSTOCK]?></td>
            <td><?=$d[COL_NMSATUAN]?></td>
            <td style="text-align: right"><?=date('d-m-Y', strtotime($d[COL_DATEEXPIRED]))?></td>
            <td style="text-align: right; width: 50px"><?=$d[COL_NMSUMBER]=='DAK'?number_format($d[COL_JUMLAH]):'-'?></td>
            <td style="text-align: right; width: 50px"><?=$d[COL_NMSUMBER]=='PROGRAM'?number_format($d[COL_JUMLAH]):'-'?></td>
            <td style="text-align: right; width: 50px"><?=$d[COL_NMSUMBER]=='DAU'?number_format($d[COL_JUMLAH]):'-'?></td>
            <td style="text-align: right; width: 50px"><?=$d[COL_NMSUMBER]=='BUFFER STOCK'?number_format($d[COL_JUMLAH]):'-'?></td>
          </tr>
          <?php
          $no++;
        }
      }
      ?>
    </tbody>
  </table>
  <br />
  <table width="100%" cellpadding="2">
    <tr>
      <td style="width: 33%; white-space: nowrap">Disetujui untuk dikeluarkan,</td>
      <td style="width: 33%; white-space: nowrap"></td>
      <td style="width: 33%; white-space: nowrap">TEBING TINGGI,    -<?=date('m-Y', strtotime($data[COL_DATEDISTRIBUTION]))?></td>
    </tr>
    <tr>
      <td style="width: 33%; white-space: nowrap">Ka. Subbag TU UPTD. Instalasi Farmasi<br />Kota Tebing Tinggi</td>
      <td style="width: 33%; white-space: nowrap"></td>
      <td style="width: 33%; white-space: nowrap">Pengawas</td>
    </tr>
    <tr>
      <td style="width: 33%; white-space: nowrap; font-weight: bold">
        <br /><br /><br />
        <?=GetSetting('SETTING_ORG_KTUNAMA')?><br />
        NIP. <?=GetSetting('SETTING_ORG_KTUNIP')?>
      </td>
      <td style="width: 33%; white-space: nowrap">

      </td>
      <td style="width: 33%; white-space: nowrap; font-weight: bold">
        <br /><br /><br />
        <?=!empty($data[COL_NMKAPUSKESMAS])?$data[COL_NMKAPUSKESMAS]:'<span style="font-style: italic">(kosong)</span>'?><br />
        NIP. <?=!empty($data[COL_NMKAPUSKESMASNIP])?$data[COL_NMKAPUSKESMASNIP]:'<span style="font-style: italic">(kosong)</span>'?>
      </td>
    </tr>
    <tr>
      <td style="width: 33%; white-space: nowrap">
        <br /><br />
        Yang membuat daftar,
        <br /><br /><br /><br />
        <?='<strong>'.GetLoggedUser()[COL_NM_FULLNAME].'</strong>'?>
      </td>
      <td style="width: 33%; white-space: nowrap">

      </td>
      <td style="width: 33%; white-space: nowrap">

      </td>
    </tr>
  </table>
</body>
</html>
