<html>
<style>
body {
  /*font-family: "Source Sans Pro",-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol"*/
}
table.table-bordered, .table-bordered td, .table-bordered th {
  border: 1px solid #000;
  border-collapse: collapse;
}
.table-bordered td, .table-bordered th {
  padding: .25rem;
  font-size: 8.5pt !important;
}
</style>
<body>
  <h4 style="text-align: center; margin-bottom: 0">SURAT BUKTI BARANG KELUAR<br />Nomor:440.442/&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;/UPTD-IFK/TT/&nbsp;&nbsp;&nbsp;&nbsp;/<?=date('Y', strtotime($data[COL_DATEDISTRIBUTION]))?></h4><hr />
  <br />
  <table width="100%">
    <tr>
      <td style="width: 80%"></td>
      <td style="white-space: nowrap">
        <p>
          Kepada Yth.<br />
          <strong>Kepala Puskesmas <?=$data[COL_NMPUSKESMAS]?></strong><br /><br /><br />
          di TEBING TINGGI
        </p>
      </td>
    </tr>
  </table>
  <br />
  <p style="text-align: justify">
    Sesuai dengan surat permintaan No.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tanggal &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.
    <br />Dengan ini kami kirimkan obat-obatan dan perbekalan kesehatan sebagai berikut:
  </p>
  <table width="100%" class="table-bordered" cellmargin="0">
    <thead>
      <tr style="background-color: #dedede">
        <th rowspan="2">NO</th>
        <th rowspan="2">NAMA OBAT / PERBEKKES</th>
        <th rowspan="2">SATUAN</th>
        <th rowspan="2">HARGA</th>
        <th colspan="4">PEMBERIAN</th>
        <th rowspan="2">TOTAL</th>
        <th rowspan="2">EXP.</th>
        <th rowspan="2">BATCH</th>
        <th rowspan="2">PABRIK</th>
      </tr>
      <tr style="background-color: #dedede">
        <th>DAK</th>
        <th>PROGRAM</th>
        <th>DAU</th>
        <th>BUFFER</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $no=1;
      $prevstock = '';
      $sumHarga = 0;
      foreach($det as $d) {
        ?>
        <tr>
          <?php
          if($d[COL_NMSTOCK] != $prevstock) {
            ?>
            <td style="text-align: right"><?=$no?></td>
            <td><?=$d[COL_NMSTOCK]?></td>
            <td><?=$d[COL_NMSATUAN]?></td>
            <?php
          }
          ?>
          <td style="text-align: right"><?=number_format($d[COL_HARGA])?></td>
          <td style="text-align: right; width: 50px"><?=$d[COL_NMSUMBER]=='DAK'?number_format($d['TotalQty']):'-'?></td>
          <td style="text-align: right; width: 50px"><?=$d[COL_NMSUMBER]=='PROGRAM'?number_format($d['TotalQty']):'-'?></td>
          <td style="text-align: right; width: 50px"><?=$d[COL_NMSUMBER]=='DAU'?number_format($d['TotalQty']):'-'?></td>
          <td style="text-align: right; width: 50px"><?=$d[COL_NMSUMBER]=='BUFFER STOCK'?number_format($d['TotalQty']):'-'?></td>
          <td style="text-align: right"><?=number_format($d[COL_HARGA]*$d['TotalQty'])?></td>
          <td style="text-align: right"><?=date('d-m-Y', strtotime($d[COL_DATEEXPIRED]))?></td>
          <td><?=$d[COL_NMBATCH]?></td>
          <td style="white-space: nowrap"><?=$d[COL_NMPABRIK]?></td>
        </tr>
        <?php
        $no++;
        $prevstock = $d[COL_NMSTOCK];
        $sumHarga += $d[COL_HARGA]*$d['TotalQty'];
      }
      ?>
      <tr>
        <td colspan="8" style="font-weight: bold; text-align: right">TOTAL</td>
        <td style="font-weight: bold; text-align: right"><?=number_format($sumHarga)?></td>
      </tr>
    </tbody>
  </table>
  <br />
  <p>
    Demikian untuk dapat diterima, atas perhatiannya kami ucapkan terima kasih.
  </p>
  <table width="100%" cellpadding="2">
    <tr>
      <td colspan="5">Obat-Obatan dan Perbekalan Kesehatan tersebut diatas telah diterima dengan baik dan cukup.</td>
    </tr>
    <tr>
      <td colspan="4"></td>
      <td>Tebing Tinggi, <?=date('d-m-Y', strtotime($data[COL_DATEDISTRIBUTION]))?></td>
    </tr>
    <tr>
      <td style="width: 30%; white-space: nowrap">Yang menerima</td>
      <td style="width: 5%; white-space: nowrap"></td>
      <td style="width: 30%; white-space: nowrap">Diketahui Kepala UPTD Puskesmas</td>
      <td style="width: 5%; white-space: nowrap"></td>
      <td style="width: 30%; white-space: nowrap">KTU. UPTD. Instalasi Farmasi Kota</td>
    </tr>
    <tr>
      <td style="width: 30%; white-space: nowrap; font-weight: bold">
        <br /><br /><br />
        <br />
        NIP.
      </td>
      <td style="width: 5%; white-space: nowrap"></td>
      <td style="width: 30%; white-space: nowrap; font-weight: bold">
        <br /><br /><br />
        <?=!empty($data[COL_NMKAPUSKESMAS])?$data[COL_NMKAPUSKESMAS]:'<span style="font-style: italic">(kosong)</span>'?><br />
        NIP. <?=!empty($data[COL_NMKAPUSKESMASNIP])?$data[COL_NMKAPUSKESMASNIP]:'<span style="font-style: italic">(kosong)</span>'?>
      </td>
      <td style="width: 5%; white-space: nowrap"></td>
      <td style="width: 30%; white-space: nowrap; font-weight: bold">
        <br /><br /><br />
        <?=GetSetting('SETTING_ORG_KTUNAMA')?><br />
        NIP. <?=GetSetting('SETTING_ORG_KTUNIP')?>
      </td>
    </tr>
  </table>
</body>
</html>
