<div class='modal-header'>
<h5 class='modal-title'>DETAIL PEMAKAIAN</h5>
</div>
<div class='modal-body p-0'>
  <table class='table table-striped mb-0'>
  <tr>
    <td width='200px'>Tanggal</td><td width='10px'>:</td>
    <td class="font-weight-bold"><?=$rdata[0][COL_DATEISSUE]?></td>
  </tr>
  <tr>
    <td width='200px'>Puskesmas / Lokasi</td><td width='10px'>:</td>
    <td class="font-weight-bold"><?=$rdata[0][COL_NMPUSKESMAS]?></td>
  </tr>
  </table>
  <div class="row">
    <div class="col-sm-12 p-3">
      <table id="tbl-item" class="table table-bordered" width="100%">
        <thead>
          <tr>
            <th>#</th>
            <th>OBAT</th>
            <th>BATCH</th>
            <th>JLH.</th>
            <th>SATUAN</th>
            <th>WAKTU</th>
            <th>OLEH</th>
          </tr>
        </thead>
        <tbody>
          <?php
          foreach($rdata as $d) {
            ?>
            <tr>
              <td><a href="<?=site_url('admin/issue/delete/'.$d[COL_UNIQ])?>" class="btn btn-xs btn-outline-danger btn-action"><i class="fas fa-trash"></i></a></td>
              <td><?=$d[COL_NMSTOCK]?></td>
              <td><?=$d[COL_NMBATCH]?></td>
              <td><?=number_format($d[COL_JUMLAH])?></td>
              <td><?=$d[COL_NMSATUAN]?></td>
              <td><?=$d[COL_CREATEDON]?></td>
              <td><?=$d['Nm_CreatedBy']?></td>
            </tr>
            <?php
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  var dtItem = $('#tbl-item').dataTable({
    "autoWidth" : false,
    "scrollY" : '40vh',
    "scrollX": "200%",
    "iDisplayLength": 50,
    //"aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    //"dom":"R<'row'<'col-sm-8'l><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    //"dom":"R<'row'<'col-sm-8 datefilter'><'col-sm-4 text-right'f<'clear'>B>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    //"buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
    "order": [[ 5, "desc" ]],
    "columnDefs": [
      {"targets":[0], "className":'nowrap text-center'},
      {"targets":[5,6], "className":'nowrap'},
      {"targets":[3,5], "className":'nowrap dt-body-right'}],
    "columns": [
      {"orderable": false,"width": "10px"},
      {"orderable": true,"width": "240px"},
      {"orderable": false},
      {"orderable": false},
      {"orderable": false},
      {"orderable": true},
      {"orderable": false}
    ],
    "createdRow": function(row, data, dataIndex) {
      $('.btn-action', $(row)).click(function() {
        var url = $(this).attr('href');
        if(confirm('Apakah anda yakin?')) {
          $.get(url, function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success(res.success);
            }
          }, "json").done(function() {
            $('#modal-editor').modal('hide');
          }).fail(function() {
            toastr.error('SERVER ERROR');
          });
        }
        return false;
      });
    },
    "initComplete": function(settings, json) {
    }
  });

});
</script>
