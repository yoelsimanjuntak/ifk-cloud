<style>
#tbl-items td, #tbl-items th {
  padding: .5rem !important;
  font-size: 11pt !important;
  vertical-align: middle !important;
}
#tbl-items .select2-container {
  margin-right: 0 !important;
}
</style>
<?php
$user = GetLoggedUser();
?>
<div class="modal-header">
  <h5 class="modal-title">Form Pemakaian</h5>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true"><i class="fa fa-close"></i></span>
  </button>
</div>
<div class="modal-body">
  <p class="text-danger error-message"></p>
  <form id="form-editor" method="post" action="#">
    <div class="row">
      <div class="col-sm-9">
        <div class="form-group">
          <label>PUSKESMAS</label>
          <?php
          if($user[COL_ROLEID] != ROLEPUSKESMAS) {
            ?>
            <select class="form-control" name="<?=COL_IDPUSKESMAS?>" style="width: 100%" required>
              <?=GetCombobox("SELECT * FROM mpuskesmas WHERE IsDeleted != 1 and KdTipe = 1 ORDER BY NmPuskesmas", COL_IDPUSKESMAS, COL_NMPUSKESMAS, (!empty($data)?$data[COL_IDPUSKESMAS]:null))?>
            </select>
            <?php
          } else {
            $rpuskesmas = $this->db
            ->where(COL_IDPUSKESMAS, (!empty($data)?$data[COL_IDPUSKESMAS]:$user[COL_IDUNIT]))
            ->get(TBL_MPUSKESMAS)
            ->row_array();
            ?>
            <input type="text" class="form-control" value="<?=!empty($rpuskesmas)?$rpuskesmas[COL_NMPUSKESMAS]:'--'?>" disabled />
            <input type="hidden" name="<?=COL_IDPUSKESMAS?>" value="<?=$user[COL_IDUNIT]?>" />
            <?php
          }
          ?>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="form-group">
          <label>TANGGAL</label>
          <input type="text" class="form-control datepicker text-right" name="<?=COL_DATEISSUE?>" value="<?=!empty($data)?$data[COL_DATEISSUE]:""?>" required />
        </div>
      </div>
      <div class="col-sm-12">
        <div class="form-group">
          <label>CATATAN</label>
          <textarea rows="3" class="form-control" name="<?=COL_NMREMARKS?>"><?=!empty($data)?$data[COL_NMREMARKS]:''?></textarea>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          <input type="hidden" name="IssueItems" />
          <div class="row">
            <div class="col-sm-12">
              <table id="tbl-items" class="table table-bordered">
                <thead class="bg-info">
                  <tr>
                    <th>Batch</th>
                    <th>Obat</th>
                    <th>Jumlah</th>
                    <th>Satuan</th>
                    <th class="text-center">#</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
                <tfoot>
                  <tr>
                    <th style="white-space: nowrap; min-width: 200px">
                      <div class="input-group input-group-sm">
                        <input type="hidden" name="<?=COL_IDITEM?>" readonly />
                        <input type="hidden" name="Qty" readonly />
                        <input type="text" class="form-control" name="<?=COL_NMBATCH?>" readonly />
                        <span class="input-group-append">
                          <button type="button" id="btn-browse-item" class="btn btn-info"><i class="far fa-search"></i><!--&nbsp;BROWSE..--></button>
                        </span>
                      </div>
                    </th>
                    <th style="white-space: nowrap; min-width: 100px">
                      <input type="text" class="form-control form-control-sm" name="<?=COL_NMSTOCK?>" readonly />
                    </th>
                    <th style="white-space: nowrap; max-width: 100px">
                      <input type="text" class="form-control form-control-sm uang text-right" name="<?=COL_JUMLAH?>" />
                    </th>
                    <th style="white-space: nowrap; max-width: 100px">
                      <input type="text" class="form-control form-control-sm" name="<?=COL_NMSATUAN?>" readonly />
                    </th>
                    <th style="white-space: nowrap;" class="text-center">
                      <button type="button" id="btn-add-item" class="btn btn-sm btn-default"><i class="fa fa-plus"></i></button>
                    </th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
      <button type="submit" class="d-none">SUBMIT</button>
    </div>
  </form>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-outline-primary btn-ok">SIMPAN</button>
  <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">BATAL</button>
</div>
<script>
var form = $('#form-editor');
$(document).ready(function() {
  $(".money", form).number(true, 2, '.', ',');
  $(".uang", form).number(true, 0, '.', ',');
  $("select", form).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
  $('.datepicker', form).daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: parseInt(moment().format('YYYY'),10)-2,
    maxYear: parseInt(moment().format('YYYY'),10),
    locale: {
        format: 'YYYY-MM-DD'
    }
  });
  $('.btn-ok').click(function() {
    $('#form-editor').submit();
  });

  $('#modal-browse-item').on('show.bs.modal', function () {
    form.closest('.modal').modal('hide');
  });
  $('#modal-browse-item').on('hide.bs.modal', function () {
    form.closest('.modal').modal('show');
  });

  $('#btn-browse-item', form).click(function() {
    var idPuskesmas = $('[name=IdPuskesmas]', form).val();
    var tanggal = $('[name=DateIssue]', form).val();
    $('.modal-body', $('#modal-browse-item')).load('<?=site_url('admin/ajax/get-available-dist')?>', {IdPuskesmas: idPuskesmas, Tanggal: tanggal}, function(){
      $('#modal-browse-item').modal('show');
    });
  });

  $('[name=IssueItems]', form).change(function() {
    writeItem('tbl-items', 'IssueItems');
  }).val(encodeURIComponent('<?=$IssueItems?>')).trigger('change');

  $('#btn-add-item', $('#tbl-items')).click(function() {
    var dis = $(this);
    var arr = $('[name=IssueItems]').val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var row = dis.closest('tr');
    var IdItem = $('[name=IdItem]', row).val();
    var NmBatch = $('[name=NmBatch]', row).val();
    var NmStock = $('[name=NmStock]', row).val();
    var Jumlah = $('[name=Jumlah]', row).val();
    var Qty = $('[name=Qty]', row).val();
    var NmSatuan = $('[name=NmSatuan]', row).val();
    if(IdItem && Jumlah) {
      var exist = jQuery.grep(arr, function(a) {
        return a.IdItem == IdItem;
      });
      if(exist.length == 0) {
        if(parseInt(Jumlah) > parseInt(Qty)) {
          alert('Jumlah tidak boleh melebihi sisa persediaan.');
          return false;
        }

        arr.push({'IdItem': IdItem, 'NmBatch':NmBatch, 'NmStock':NmStock, 'Jumlah':Jumlah, 'NmSatuan':NmSatuan});
        $('[name=IssueItems]').val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
        $('input', row).val('');
        $('select', row).val('').trigger('change');
      } else {
        alert('Item yang sama tidak bisa diinput berulang.');
        return false;
      }
    } else {
      alert('Harap isi kolom isian dengan benar.');
      return false;
    }
  });

  /*$('[name=IdPuskesmas]', form).change(function(){
    var idPuskesmas = $(this).val();
    var tanggal = $('[name=DateIssue]', form).val();
    $('[name=IdStock]', form).load('<?=site_url('admin/ajax/get-opt-available-dist')?>', {IdPuskesmas: idPuskesmas, Tanggal: tanggal}, function(){
      $('[name=IdStock]', form).select2({ width: 'resolve', theme: 'bootstrap4' });
    });
  }).trigger('change');*/
});

function writeItem(tbl, input) {
  var tbl = $('#'+tbl+'>tbody');
  var arr = $('[name='+input+']').val();
  if(arr) {
    arr = JSON.parse(decodeURIComponent(arr));
    if(arr.length > 0) {
      var html = '';
      for (var i=0; i<arr.length; i++) {
        html += '<tr>';
        html += '<td>'+arr[i].NmBatch+'</td>';
        html += '<td>'+arr[i].NmStock+'</td>';
        html += '<td class="text-right">'+$.number(arr[i].Jumlah, 0)+'</td>';
        html += '<td>'+arr[i].NmSatuan+'</td>';
        <?php
        if(!isset($disabled) || empty($disabled)) {
          ?>
          html += '<td class="text-center"><button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="'+arr[i].IdItem+'" /></td>';
          <?php
        } else {
          ?>
          html += '<td class="text-center">-</td>';
          <?php
        }
        ?>
        html += '</tr>';
      }
      tbl.html(html);

      $('.btn-del', tbl).click(function() {
        var row = $(this).closest('tr');
        var idx = $('[name=idx]', row).val();
        if(idx) {
          var arr = $('[name='+input+']').val();
          arr = JSON.parse(decodeURIComponent(arr));

          var arrNew = $.grep(arr, function(e){ return e.IdItem != idx; });
          $('[name='+input+']').val(encodeURIComponent(JSON.stringify(arrNew))).trigger('change');
        }
      });
    } else {
      tbl.html('<tr><td colspan="5"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
    }
  } else {
    tbl.html('<tr><td colspan="5"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
  }
}

/*onScan.attachTo(document, {
  onScan: function(sCode, iQty) {
    var batch_ = sCode;
    var tanggal = $('[name=DateIssue]', form).val();
    var puskesmas = $('[name=IdPuskesmas]', form).val();
    $.ajax({
      url: "<?=site_url('admin/ajax/get-dist-by-batch')?>",
      method: "POST",
      data: {NmBatch: batch_, Tanggal: tanggal, IdPuskesmas: puskesmas}
    }).done(function(res) {
      res = JSON.parse(res);
      if(!res) {
        toastr.error('Maaf, obat dengan BATCH <strong>'+batch_+'</strong> tidak ditemukan.');
      }

      $('[name=IdItem]', form).val(res.Uniq);
      $('[name=NmBatch]', form).val(res.NmBatch);
      $('[name=NmStock]', form).val(res.NmStock);
      $('[name=NmSatuan]', form).val(res.NmSatuan);
      $('[name=Jumlah]', form).val(res.JlhSisa);
      $('[name=Qty]', form).val(res.JlhSisa);
    });
  }
});*/
document.addEventListener('scan', function(sCode, iQty) {
  //var batch_ = sCode;
  var tanggal = $('[name=DateIssue]', form).val();
  var puskesmas = $('[name=IdPuskesmas]', form).val();
  $.ajax({
    url: "<?=site_url('admin/ajax/get-dist-by-batch')?>",
    method: "POST",
    data: {NmBatch: sCode.detail.scanCode, Tanggal: tanggal, IdPuskesmas: puskesmas}
  }).done(function(res) {
    res = JSON.parse(res);
    if(!res) {
      toastr.error('Maaf, obat dengan BATCH <strong>'+sCode.detail.scanCode+'</strong> tidak ditemukan.');
      return false
    } else {
      $('[name=IdItem]', form).val(res.Uniq);
      $('[name=NmBatch]', form).val(res.NmBatch);
      $('[name=NmStock]', form).val(res.NmStock);
      $('[name=NmSatuan]', form).val(res.NmSatuan);
      $('[name=Jumlah]', form).val(res.JlhSisa);
      $('[name=Qty]', form).val(res.JlhSisa);
    }
  });
});
</script>
