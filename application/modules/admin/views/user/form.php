<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?= $title ?></h3>
      </div><!-- /.col -->
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'form-main','class'=>'form-horizontal'))?>
    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          <a href="<?=site_url('admin/user/index')?>" class="btn btn-sm btn-secondary"><i class="far fa-arrow-left"></i>&nbsp;KEMBALI</a>
          <button type="submit" class="btn btn-sm btn-primary"><i class="far fa-save"></i>&nbsp;SIMPAN</button>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card card-outline card-olive">
          <div class="card-header">
            <h5 class="card-title font-weight-light">Akun & Data Diri</h5>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12">
                <?php
                if(empty($role) || true) {
                  ?>
                  <div class="form-group">
                    <label>Tipe User</label>
                    <select class="form-control" name="<?=COL_ROLEID?>" required>
                      <?=GetCombobox("SELECT * FROM _roles ORDER BY RoleName", COL_ROLEID, COL_ROLENAME, (!empty($data)?$data[COL_ROLEID]:null))?>
                    </select>
                  </div>
                  <?php
                }
                ?>
                <div class="form-group div-unit">
                  <label>Unit Kerja</label>
                  <select class="form-control" name="<?=COL_IDUNIT?>" style="width: 100%" required>
                    <?=GetCombobox("SELECT * FROM mpuskesmas  where IsDeleted != 1 and KdTipe = 1 ORDER BY NmPuskesmas", COL_IDPUSKESMAS, COL_NMPUSKESMAS, (!empty($data)?$data[COL_IDUNIT]:null))?>
                  </select>
                </div>
                <div class="form-group">
                    <div class="row">
                      <div class="col-sm-6">
                        <label>Username / NIP</label>
                          <input type="text" class="form-control" name="<?=COL_EMAIL?>" placeholder="Username / NIP" value="<?=!empty($data)?$data[COL_EMAIL]:''?>" <?=!empty($data)?'readonly':''?> />
                      </div>
                      <div class="col-sm-6">
                        <label>Nama</label>
                        <input type="text" class="form-control" name="<?=COL_NM_FULLNAME?>" placeholder="Nama Lengkap" value="<?=!empty($data)?$data[COL_NM_FULLNAME]:''?>" required />
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <?php
        if(empty($data)) {
          ?>
          <div class="card card-outline card-info">
            <div class="card-header">
              <h5 class="card-title font-weight-light">Password</h5>
            </div>
            <div class="card-body">
              <div class="form-group row">
                <label class="control-label col-sm-4">Password</label>
                <div class="col-sm-8">
                  <input type="password" class="form-control" name="<?=COL_PASSWORD?>" placeholder="Password" />
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-sm-4">Konfirmasi Password</label>
                <div class="col-sm-8">
                  <input type="password" class="form-control" name="ConfirmPassword" placeholder="Konfirmasi Password" />
                </div>
              </div>
            </div>
          </div>
          <?php
        }
        ?>
      </div>
    </div>
    <?=form_close()?>
  </div>
</section>
<script>
$(document).ready(function() {
  $('[name=RoleID]').change(function(){
    val = $(this).val();
    if(val == '<?=ROLEPUSKESMAS?>') {
      $('.div-unit').show();
      $('.div-unit').find('input,select').attr('disabled', false);
    } else {
      $('.div-unit').hide();
      $('.div-unit').find('input,select').attr('disabled', true);
    }
  }).trigger('change');

  $('button[data-verify=true]').click(function() {
    $('[name=Verify]').val(1);
    $('#form-main').submit();
  });
  $('#form-main').validate({
    ignore: "[type=file]",
    submitHandler: function(form) {
      if(!confirm('Apakah anda yakin?')) {
        return false;
      }

      var btnSubmit = $('button[type=submit], button[data-verify=true]', $(form));
      btnSubmit.attr('disabled', true);
      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success('Berhasil');
            if(res.data && res.data.redirect) {
              setTimeout(function(){
                location.href = res.data.redirect;
              }, 1000);
            }
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});
</script>
