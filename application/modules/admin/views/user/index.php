<?php $data = array();
$i = 0;
foreach ($res as $d) {
  $res[$i] = array(
    '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d[COL_USERNAME] . '" />',
    anchor('admin/user/edit/'.GetEncryption($d[COL_USERNAME]),$d[COL_USERNAME]),
    $d[COL_ISSUSPEND] ? '<small class="badge pull-left badge-danger">SUSPENDED</small>' : '<small class="badge pull-left badge-success">ACTIVE</small>',
    $d[COL_ROLENAME],
    //$d[COL_ROLENAME],
    $d[COL_NM_FULLNAME],
    $d[COL_NMPUSKESMAS],
    //(!empty($d[COL_DATE_REGISTERED])?date('d/m/Y', strtotime($d[COL_DATE_REGISTERED])):"-"),
    (!empty($d[COL_LASTLOGIN])?date('d/m/Y H:i:s', strtotime($d[COL_LASTLOGIN])):"-")
  );
  $i++;
}
$data = json_encode($res);
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark font-weight-light"><?= $title ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=site_url('admin/dashboard')?>">Dashboard</a></li>
            <li class="breadcrumb-item active"><?= $title ?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <p>
          <?=anchor('admin/user/delete','<i class="fas fa-trash"></i> Hapus',array('class'=>'cekboxaction btn btn-danger btn-sm','data-confirm'=>'Apakah anda yakin?'))?>
          <?=anchor('admin/user/activate/0','<i class="fas fa-check"></i> Aktifkan',array('class'=>'cekboxaction btn btn-success btn-sm','data-confirm'=>'Apakah anda yakin?'))?>
          <?=anchor('admin/user/activate/1','<i class="fas fa-warning"></i> Suspend',array('class'=>'cekboxaction btn btn-secondary btn-sm','data-confirm'=>'Apakah anda yakin?'))?>
          <?=anchor('admin/user/activate/999','<i class="fas fa-refresh"></i> Reset Password',array('class'=>'cekboxaction btn btn-info btn-sm','data-confirm'=>'Apakah anda yakin?'))?>
          <?=anchor('admin/user/add'.(!empty($role)?'/'.$role:''),'<i class="fas fa-plus"></i> Data Baru',array('class'=>'btn btn-primary btn-sm'))?>
        </p>
        <div class="card card-default">
          <div class="card-body">
            <form id="dataform" method="post" action="#">
              <table id="datalist" class="table table-bordered table-hover" style="white-space: nowrap;">

              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function() {
  var dataTable = $('#datalist').dataTable({
    "autoWidth" : false,
    //"sDom": "Rlfrtip",
    "aaData": <?=$data?>,
    //"bJQueryUI": true,
    //"aaSorting" : [[5,'desc']],
    "scrollY" : '40vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    "dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
    "order": [[ 1, "asc" ]],
    "columnDefs":[
      {targets: [6], className:'text-right'}
    ],
    "aoColumns": [
      {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" class=\"\" />","width":"10px","bSortable":false},
      {"sTitle": "Username"},
      {"sTitle": "Status","bSortable":false},
      {"sTitle": "Tipe"},
      //{"sTitle": "Role"},
      {"sTitle": "Nama Lengkap"},
      {"sTitle": "Unit"},
      //{"sTitle": "Tanggal Registrasi"},
      {"sTitle": "Login Terakhir"},
      //{"sTitle": "Address"},
      //{"sTitle": "Last Login", "width": "15%"}
    ]
  });
  $('#cekbox').click(function() {
    if($(this).is(':checked')) {
      $('.cekbox').prop('checked',true);
    } else {
      $('.cekbox').prop('checked',false);
    }
  });
});
</script>
